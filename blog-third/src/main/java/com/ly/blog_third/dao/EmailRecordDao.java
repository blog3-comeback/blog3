package com.ly.blog_third.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_third.entity.EmailRecordEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 邮箱记录
 * @author ly create at 2021/8/5 - 17:35
 **/
@Mapper
public interface EmailRecordDao extends BaseMapper<EmailRecordEntity> {

}
