package com.ly.blog_third.dao;

import com.ly.blog_third.entity.EmailTemplateEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-09-15 09:34:04
 */
@Mapper
public interface EmailTemplateDao extends BaseMapper<EmailTemplateEntity> {
	
}
