package com.ly.blog_third.controller;

import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_third.entity.EmailTemplateEntity;
import com.ly.blog_third.service.EmailTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 
 *
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-09-15 09:34:04
 */
@RestController
@RequestMapping("third/emailtemplate")
public class EmailTemplateController {
    @Autowired
    private EmailTemplateService emailTemplateService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@PreAuthorize("hasAuthority('blog_third:emailtemplate:list')")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = emailTemplateService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@PreAuthorize("hasAuthority('blog_third:emailtemplate:info')")
    public R info(@PathVariable("id") Integer id){
		EmailTemplateEntity emailTemplate = emailTemplateService.getById(id);

        return R.ok().put("emailTemplate", emailTemplate);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@PreAuthorize("hasAuthority('blog_third:emailtemplate:save')")
    public R save(@RequestBody EmailTemplateEntity emailTemplate){
        emailTemplate.setCreateBy(SecurityUtils.getUserId());
        emailTemplate.setCreateTime(new Date());
        emailTemplate.setLastUpdateBy(SecurityUtils.getUserId());
        emailTemplate.setLastUpdateTime(new Date());
		emailTemplateService.save(emailTemplate);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@PreAuthorize("hasAuthority('blog_third:emailtemplate:update')")
    public R update(@RequestBody EmailTemplateEntity emailTemplate){
        emailTemplate.setLastUpdateBy(SecurityUtils.getUserId());
        emailTemplate.setLastUpdateTime(new Date());
		emailTemplateService.updateById(emailTemplate);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@PreAuthorize("hasAuthority('blog_third:emailtemplate:delete')")
    public R delete(@RequestBody Integer[] ids){
		emailTemplateService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
