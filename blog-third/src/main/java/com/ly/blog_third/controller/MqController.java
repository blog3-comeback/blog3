package com.ly.blog_third.controller;

import com.ly.blog_common.entity.mq.MqMsgEntity;
import com.ly.blog_common.response.BlogResponseStatus;
import com.ly.blog_common.utils.R;
import com.ly.blog_third.service.MqSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 消息中间消息转发 - rabbitMQ
 * @author ly create at 2021/6/29 - 11:00
 **/

@RestController
@RequestMapping("/third/mq")
public class MqController {

    @Autowired
    private MqSendService mqService;

    /**
     * 发送信息到MQ
     * @param mqMsgEntity mq消息体封装
     * @return
     */
    @PostMapping("/send")
    public R send(@RequestBody MqMsgEntity mqMsgEntity){
        boolean res = mqService.send(mqMsgEntity);
        return res?R.ok():R.error(BlogResponseStatus.MQ_SEND_ERROR);
    }

}
