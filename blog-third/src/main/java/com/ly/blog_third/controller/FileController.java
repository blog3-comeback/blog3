package com.ly.blog_third.controller;

import com.ly.blog_third.entity.OssEntity;
import com.ly.blog_common.utils.R;
import com.ly.blog_third.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 个人文件对接
 * @author ly create at 2021/6/21 - 17:43
 **/

@RestController
@RequestMapping("/third/file")
public class FileController {

    @Autowired
    private OssController ossController;

    @Autowired
    private FileService fileService;

    @PostMapping("")
    public R upload(OssEntity ossEntity, MultipartFile file, HttpServletRequest request) {

        String filePath = fileService.upload(ossEntity,file);
        return R.ok().put("filePath",filePath);

    }

    @GetMapping("/{directory}/{fileName}")
    public void downLoad(@PathVariable(value = "directory") String directory,
                        @PathVariable(value = "fileName") String fileName,
                        HttpServletResponse response) {
        fileService.downLoad(directory,fileName,response);
    }


    @GetMapping("/netFileUpload")
    public R netFileUpload(@RequestParam(value = "netUrl") String netUrl) throws Exception {
        return R.ok().put("filePath",fileService.netFileUpload(netUrl,(Map<String, Object>)ossController.policy().get("data")));
    }

}
