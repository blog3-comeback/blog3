package com.ly.blog_third.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 中间件基础配置，配置交换机-队列-通道 - 邮件 采用直连 - 一对一
 * @author ly create at 2021/6/29 - 11:02
 **/

@Configuration
public class MQEmailConfig {


    public final static String QUEUE_EMAIL = "email.topic";
    public final static String EXCHANGE_EMAIL = "email.topic";
    public final static String ROUTE_KEY_EMAIL = "email.topic";


    @Bean
    public Exchange emailExchange(){
        return new TopicExchange(EXCHANGE_EMAIL,true,false);
    }

    @Bean
    public Queue emailRegisterQueue(){
        // durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息代理重启时仍然存在，暂存队列：当前连接有效
        // exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
        // autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除。
        //   return new Queue("TestDirectQueue",true,true,false);

        //一般设置一下队列的持久化就好,其余两个就是默认false

        return new Queue(QUEUE_EMAIL,true);
    }


    /**
     *
     * 匹配规则:
     * 一、路由键必须是一串字符，用句号（.） 隔开，比如说 aaa.bbb，或者 aaa.bbb.ccc 等。
     * 二、路由模式必须包含一个 星号（*），主要用于匹配路由键指定位置的一个单词，比如说，一个路由模式是这样子：aaa..b.*，那么就只能匹配路由键是这样子的：第一个单词是 aaa，第四个单词是 b。
     *    #号（#）就表示相当于一个或者多个单词，例如一个匹配模式是aaa.bbb.ccc.#，那么，以aaa.bbb.ccc开头的路由键都是可以的。
     *
     * @param emailQueue
     * @param emailExchange
     * @return
     */
    /**
     * 绑定注册路由
     * @param emailRegisterQueue
     * @param emailExchange
     * @return
     */
    @Bean
    Binding bindingExchangeRegisterQueue(Queue emailRegisterQueue , TopicExchange emailExchange) {
        return BindingBuilder.bind(emailRegisterQueue).to(emailExchange).with(ROUTE_KEY_EMAIL);
    }


    /**
     * 邮件发送消息配置 - e
     */





}
