package com.ly.blog_third.entity;

import lombok.Data;

/**
 * Oss配置参数信息
 * @author ly create at 2021/6/21 - 18:02
 **/

@Data
public class OssEntity {

    //策略
    private String policy;
    //标记
    private String signature;
    //文件夹+文件名 2021-06-21/b08b022b-22b7-433a-8d75-50d904697aea.png
    private String key;
    //通行id
    private String ossaccessKeyId;
    //通行id
    private String accessKeyId;
    //文件夹 2021-06-21/
    private String dir;
    //服务ip
    private String host;

}
