package com.ly.blog_third.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-09-15 09:34:04
 */
@Data
@TableName("email_template")
public class EmailTemplateEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 类型
	 */
	private Integer type;
	/**
	 * 主题
	 */
	private String title;
	/**
	 * 邮箱模板
	 */
	private String template;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;

}
