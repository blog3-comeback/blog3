package com.ly.blog_third.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 邮箱记录
 * @author ly create at 2021/8/5 - 17:37
 **/
@Data
@TableName("email_record")
public class EmailRecordEntity {


    //主键
    private String id;
    //发送的邮箱，多个逗号分隔
    private String toEmail;
    //    是否成功 0成功 1失败
    private int isSuccess;
    //    发送的内容
    private String content;
    //    邮件类型 0，注册 1，登录 2、简单邮箱信息...
    private int type;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String lastUpdateBy;
    /**
     * 更新时间
     */
    private Date lastUpdateTime;

}
