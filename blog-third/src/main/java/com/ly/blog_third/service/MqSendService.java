package com.ly.blog_third.service;

import com.ly.blog_common.entity.mq.MqMsgEntity;

/**
 * @author ly create at 2021/6/29 - 11:52
 **/
public interface MqSendService {

    boolean send(MqMsgEntity mqMsgEntity);

}
