package com.ly.blog_third.service.impl;

import com.ly.blog_common.utils.FileUtils;
import com.ly.blog_common.utils.StringUtils;
import com.ly.blog_third.entity.OssEntity;
import com.ly.blog_third.service.FileService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ly create at 2021/6/21 - 20:17
 **/
@Service
public class FileServiceImpl implements FileService {

    String a = System.getProperty("user.dir");

    String rootPath = "/file/";
    String host = "http://localhost:8000/third/file/";

    @Override
    public String upload(OssEntity ossEntity, MultipartFile file) {

        //替换 / 适配
        File directory = new File(rootPath + ossEntity.getDir());

        if (!directory.exists()) {
            directory.mkdirs();
        }

        OutputStream out = null;
        try {
            byte[] b = file.getBytes();
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {// 调整异常数据
                    b[i] += 256;
                }
            }

            //检测文件是否已加载
            File fileTemp = new File(rootPath + ossEntity.getKey());
            out = new FileOutputStream(fileTemp);
            out.write(b);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return host + ossEntity.getKey();
    }


    @Override
    public void downLoad(String directory, String fileName, HttpServletResponse response) {

        byte[] buff = new byte[1024];
        //创建缓冲输入流
        BufferedInputStream bis = null;
        OutputStream outputStream = null;

        try {
            outputStream = response.getOutputStream();

            //这个路径为待下载文件的路径
            bis = new BufferedInputStream(new FileInputStream(new File(rootPath+ directory +"/"+ fileName)));
            int read = bis.read(buff);

            //通过while循环写入到指定了的文件夹中
            while (read != -1) {
                outputStream.write(buff, 0, buff.length);
                outputStream.flush();
                read = bis.read(buff);
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    @Override
    public String netFileUpload(String netUrl, Map<String, Object> data) throws Exception {
        OssEntity ossEntity = new OssEntity();
        ossEntity.setOssaccessKeyId(String.valueOf(data.get("accessid")));
        ossEntity.setAccessKeyId(String.valueOf(data.get("accessid")));
        ossEntity.setPolicy(String.valueOf(data.get("policy")));
        ossEntity.setSignature(String.valueOf(data.get("signature")));
        ossEntity.setKey(String.valueOf(data.get("dir")) + StringUtils.generateUUID()+"_" + netUrl.substring(netUrl.lastIndexOf("/") + 1));
        ossEntity.setDir(String.valueOf(data.get("dir")));
        ossEntity.setHost(String.valueOf(data.get("host")));

        byte[] bytes = FileUtils.getFileFromUrl(netUrl);
        Map<String, Object> map = new HashMap<>();
        String filePath = upload(ossEntity, FileUtils.convertFile2MultipartFile(bytes));
        return filePath;
    }


}
