package com.ly.blog_third.service;

import com.ly.blog_third.entity.OssEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author ly create at 2021/6/21 - 20:17
 **/
public interface FileService {

    String upload(OssEntity ossEntity, MultipartFile file);

    void downLoad(String directory, String fileName, HttpServletResponse response);

    String netFileUpload(String netUrl, Map<String, Object> data) throws Exception;
}
