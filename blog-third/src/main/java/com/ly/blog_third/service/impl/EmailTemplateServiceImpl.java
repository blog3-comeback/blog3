package com.ly.blog_third.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;

import com.ly.blog_third.dao.EmailTemplateDao;
import com.ly.blog_third.entity.EmailTemplateEntity;
import com.ly.blog_third.service.EmailTemplateService;


@Service("emailTemplateService")
public class EmailTemplateServiceImpl extends ServiceImpl<EmailTemplateDao, EmailTemplateEntity> implements EmailTemplateService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<EmailTemplateEntity> page = this.page(
                new Query<EmailTemplateEntity>().getPage(params),
                new QueryWrapper<EmailTemplateEntity>()
        );

        return new PageUtils(page);
    }

}