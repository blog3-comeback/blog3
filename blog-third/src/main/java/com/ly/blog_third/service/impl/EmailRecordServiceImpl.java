package com.ly.blog_third.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_third.dao.EmailRecordDao;
import com.ly.blog_third.entity.EmailRecordEntity;
import com.ly.blog_third.service.EmailRecordService;
import org.springframework.stereotype.Service;


@Service("emailRecordService")
public class EmailRecordServiceImpl extends ServiceImpl<EmailRecordDao, EmailRecordEntity> implements EmailRecordService {

}