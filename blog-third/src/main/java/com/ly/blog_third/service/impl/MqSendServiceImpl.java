package com.ly.blog_third.service.impl;

import com.ly.blog_common.entity.mq.EmailMqInfo;
import com.ly.blog_common.entity.mq.MqMsgEntity;
import com.ly.blog_common.utils.Map2ObjectUtils;
import com.ly.blog_third.config.MQEmailConfig;
import com.ly.blog_third.service.MqSendService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 只做MQ发送，不做业务区分 ----
 * @author ly create at 2021/6/29 - 11:52
 **/
@Slf4j
@Service
public class MqSendServiceImpl implements MqSendService {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Override
    public boolean send(MqMsgEntity mqMsgEntity) {
        log.info("【发送MQ信息】 -- 开始发送MQ信息 - 类型：{}，内容：{}", mqMsgEntity.getType(), mqMsgEntity.getMsg());
        try {
            switch (mqMsgEntity.getType()) {
                // MQ触发邮箱 转发
                case EMAIL:
                    EmailMqInfo emailMqInfo = Map2ObjectUtils.mapToObject((Map<String, Object>) mqMsgEntity.getMsg(), EmailMqInfo.class);
                    this.amqpTemplate.convertAndSend(MQEmailConfig.EXCHANGE_EMAIL, MQEmailConfig.QUEUE_EMAIL, emailMqInfo);
                    log.info("【发送MQ信息 - 邮件信息】 -- 成功发送MQ信息 - 类型：{}，内容：{}", mqMsgEntity.getType(), mqMsgEntity.getMsg());
                    break;
                default:
                    log.info("【发送MQ信息】 -- 没有类型的信息，无法发送MQ - 类型：{}，内容：{}", mqMsgEntity.getType(), mqMsgEntity.getMsg());

            }
            return true;
        } catch (Exception e) {
            log.error("【发送MQ信息】 -- 发送MQ失败 - 类型：{}，内容：{}，错误信息：{}", mqMsgEntity.getType(), mqMsgEntity.getMsg(),e.getMessage());
        }
        return false;
    }
}
