package com.ly.blog_third.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_third.entity.EmailRecordEntity;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-30 09:37:16
 */
public interface EmailRecordService extends IService<EmailRecordEntity> {

}

