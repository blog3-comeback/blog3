package com.ly.blog_third.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_third.entity.EmailTemplateEntity;

import java.util.Map;

/**
 * 
 *
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-09-15 09:34:04
 */
public interface EmailTemplateService extends IService<EmailTemplateEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

