package com.ly.blog_third;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ly create at 2021/5/11 - 10:04
 **/

@EnableRabbit
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.ly"})
public class ThirdApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThirdApplication.class,args);
    }

}
