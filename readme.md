# BLOG3 - 个人博客第三版

## 1、基本信息

​		blog3版本是自己基于之前的blog、blog2两个版本之后，完全重置的版本，是自己新技术的尝试，也是新的架构尝试和实践

​		**blog3整体包含三个大的模块，基本数据配置和编译模块、前端UI模块和后端逻辑模块。**

​		**基本数据模块（blog-base）**，包含数据库的基础sql，nacos的基础配置；**基础数据编译模块（blog-complie）**，围绕jenkins和docker实现的自动化部署。

​		**前端UI模块（blog-ui）**，基于vue-cli 4.0为基础，构建的自己的前端展示点和后端数据管控点，部署后，由nginx作为后端服务支持。

​		**后端模块**，采用springCloud+springCloudAlibaba相结合，gateway+nacos+feign，实现数据服务的切分后，相互联系。业务点，整体围绕article文章作为业务展示核心，user用户作为整个权限和菜单配置核心，data数据作为外部数据获取，third第三方作为文件上传下载、MQ邮件交互工具依托。

## 2、代码划分

### 2-1、基本模块

#### 	blog-base

​		nacos、数据库基本信息配置

#### 	blog-compile

​		jenkins、docker自动化编译脚本

### 2-2、UI模块

#### 	blog-UI

​		vue为核心构建的纯前端页面。整体氛围前端展示页面和后端管理页面。

### 2-3、后端模块

#### 	blog-user

​	用户信息、角色信息、菜单信息、配置信息、监控信息、日志信息整合全平台基础管理点

#### 	blog-article

​	核心展示数据点，展示以文章为核心，列表、明细、搜索、标签化功能。管理文章配合user的菜单实现文章管理。

#### 	blog-third

​	第三支撑点，文件上传功能，支持阿里云远程文件服务器和本身third构建的服务器。配合rabbitMQ实现邮件的发送。

#### 	blog-data

​	原创数据过于难弄，采用网络抓取的方式获取最新资讯。填充到文章中。

#### 	blog-gateway

​	网关，动态实现各个服务的转发。集群配置。

## 3、运行效果图
###     前端展示
####    首页：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181331_e1092642_1258465.png "屏幕截图.png")
####    列表：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181418_5e173f8c_1258465.png "屏幕截图.png")
####    详情：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181616_eb50d725_1258465.png "屏幕截图.png")
####    留言：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181648_06d04312_1258465.png "屏幕截图.png")
###    后端展示：
####    登录、注册：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181718_e897d277_1258465.png "屏幕截图.png")
####    后端权限菜单：
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/181836_b7e5cc59_1258465.png "屏幕截图.png")
####    用户设置：（用户设置、角色设置、菜单设置- 有参考其他开源界面设置）  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/182025_f362028e_1258465.png "屏幕截图.png") 
####    文章统一管理、数据抓取，理财管理（理财管理待设计开发）
![输入图片说明](https://images.gitee.com/uploads/images/2021/0803/182202_0355e7f3_1258465.png "屏幕截图.png")

