#!/bin/sh

cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime;
echo Asia/Shanghai > /etc/timezone;
echo "设置时间OK";

java -Xmx512m   -Xms512m -jar /blog-user-3.0.0.jar &
java  -Xmx512m   -Xms512m -jar /blog-article-3.0.0.jar  &
java  -Xmx64m   -Xms64m -jar /blog-data-3.0.0.jar  &
java  -Xmx64m   -Xms64m -jar /blog-gateway-3.0.0.jar &
java -Xmx64m  -Xms64m -jar /blog-third-3.0.0.jar;
echo "#####################【SUCCESS】启动成功#####################";

# 注释： 针对服务启动顺序问题，采用日志监听的方式，间隔性审查，执行。