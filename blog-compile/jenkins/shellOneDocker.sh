#定义变量
API_NAME="blog"
API_VERSION="3.2.1"
API_PORT=58000
IMAGE_NAME="192.168.0.101:5001/liyang/$API_NAME:$BUILD_NUMBER"
CONTAINER_NAME=$API_NAME-$API_VERSION

#复制Dockerfile
cd $WORKSPACE/
cp /var/jenkins_home/workspace/blog-maven-test/blog-compile/docker/Dockerfile .

#构建Docker镜像
docker build -t $IMAGE_NAME .

#若存在原镜像的容器，删除
cid=$(docker ps -a | grep "$CONTAINER_NAME" | awk '{print $1}')
if [ "$cid" != "" ];then
	docker rm -f $cid
fi

#启动容器
docker run -d -p $API_PORT:8000 --name $CONTAINER_NAME $IMAGE_NAME

#删除Dockerfile文件
rm -f  Dockerfile