#!/bin/bash
#支持更多远程shell语法
# 删除制作镜像脚本

#定义变量
API_NAME="blog"
API_VERSION="3.2.1"
JENKINS_PRO_NAME="blog-maven-test"
AUTHOR="liyang"

DOCKER_HUB_HOST="192.168.0.101"
DOCKER_HUB_PORT="5001"

#定义服务和对应端口
SERVER_NAME_ARR[0]="article"
SERVER_NAME_ARR[1]="data"
SERVER_NAME_ARR[2]="gateway"
SERVER_NAME_ARR[3]="third"
SERVER_NAME_ARR[4]="user"
SERVER_NAME_ARR[5]="ui"



#循环服务操作 - 生成对应docker文件
#while循环遍历
j=35
tag=$1
echo $tag
while [ $j -le $tag ]
do
    echo $j
    i=0
    while [[ i -lt ${#SERVER_NAME_ARR[@]} ]]; do
        MODULE_NAME=$API_NAME-${SERVER_NAME_ARR[i]}
        echo "【镜像删除】开始 - "$MODULE_NAME

        #定义镜像名称
        IMAGE_NAME="$DOCKER_HUB_HOST:$DOCKER_HUB_PORT/$AUTHOR/$MODULE_NAME:${j}"

        cid=$(docker images | grep "$MODULE_NAME" | grep -w $j | awk '{print $3}')
        if [ "$cid" != "" ];then
        #删除Docker镜像
        docker image rm $IMAGE_NAME
        fi

        echo "【镜像删除】成功 - "$IMAGE_NAME
        let i++
    done
    let j++
done