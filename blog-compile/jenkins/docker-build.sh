#!/bin/bash

#第一步以模板创建时间临时文件夹
echo "`date '+%Y-%m-%d %H:%M:%S'`"
guoch=`date +%Y%m%d%H%M%S`
#判断Dockerfile文件夹是否存在
if [ ! -d "../Dockerfile-${guoch}" ];then
echo "---------创建目录Dockerfile-${guoch}-----------"
cp -r ../Dockerfile/ ../Dockerfile-${guoch}
#删除原始opt文件中的jar 包
rm -rf ./opt/target/
#获取jar包名字 cc_atm_resources_msa-1.1.2-1.jar
var=../Dockerfile-${guoch}/opt/target/*.jar
FileName1=$(basename ${var})
#获取服务名 cc_atm_resources_msa
FileName2=${FileName1%.*}
FileName3=${FileName2%-*}
APP_NAME=${FileName3%-*}
echo "---------获取服务信息-----------"
echo APP_NAME ${APP_NAME}
#获取jar版本号 1.1.2-1
APP_VERSION=${FileName2#*-}
echo APP_VERSION ${APP_VERSION}
#制作镜像
TAG_NAME=${APP_VERSION}
IMAGE_NAME=${APP_NAME}
PROJECT=cnoic
REGISTRY_URL=10.1.2.116:180
if [ ! -d $1 ]; then
    PROJECT=$1
fi
#将jar包改名
mv ../Dockerfile-${guoch}/opt/target/*.jar ../Dockerfile-${guoch}/opt/app.jar
rm -rf ../Dockerfile-${guoch}/opt/target/
echo "---------进入目录Dockerfile-${guoch}-----------"
cd ../Dockerfile-${guoch}/
echo "---------构建镜像-----------"
docker build --rm -f "Dockerfile" -t="$REGISTRY_URL/$PROJECT/$IMAGE_NAME:$TAG_NAME" .
echo "---------上传镜像至镜像仓库-----------"
docker push "$REGISTRY_URL/$PROJECT/$IMAGE_NAME:$TAG_NAME"
echo "---------删除本地镜像-----------"
docker rmi "$REGISTRY_URL/$PROJECT/$IMAGE_NAME:$TAG_NAME"

#执行完任务,删除目录
echo "---------执行完任务,删除目录Dockerfile-${guoch}-----------"
rm -rf ../Dockerfile-${guoch}
else
echo "文件夹已经存在，稍后操作"
fi