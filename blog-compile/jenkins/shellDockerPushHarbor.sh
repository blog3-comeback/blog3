#!/bin/bash
#支持更多远程shell语法

#定义变量
API_NAME="blog"
API_VERSION="3.2.1"
JENKINS_PRO_NAME="blog-maven-test"
AUTHOR="liyang"

DOCKER_HUB_HOST="192.168.160.50"
DOCKER_HUB_PORT="80"

#定义服务和对应端口
SERVER_NAME_ARR[0]="article"
SERVER_NAME_ARR[1]="data"
SERVER_NAME_ARR[2]="gateway"
SERVER_NAME_ARR[3]="third"
SERVER_NAME_ARR[4]="user"

SERVER_NAME_PORT_ARR[0]=8200
SERVER_NAME_PORT_ARR[1]=9200
SERVER_NAME_PORT_ARR[2]=8000
SERVER_NAME_PORT_ARR[3]=9100
SERVER_NAME_PORT_ARR[4]=8100



#登录harbor
echo "登录harbor服务 - 开始"
echo "Harbor12345" > harborpw.txt
docker login -u admin --password-stdin < harborpw.txt 192.168.160.50:80
echo "登录harbor服务 - 结束"
rm -f harborpw.txt


#复制Dockerfile
cd $WORKSPACE/

#循环服务操作 - 生成对应docker文件
#while循环遍历
i=0
while [[ i -lt ${#SERVER_NAME_ARR[@]} ]]; do


        MODULE_NAME=$API_NAME-${SERVER_NAME_ARR[i]}
        echo "【编译docker镜像】开始 - "$MODULE_NAME

        #添加镜像到服务中
        cp /var/jenkins_home/workspace/$JENKINS_PRO_NAME/blog-compile/docker/${SERVER_NAME_ARR[i]}/Dockerfile .

        #定义镜像名称
        IMAGE_NAME="$DOCKER_HUB_HOST:$DOCKER_HUB_PORT/$AUTHOR/$MODULE_NAME:$BUILD_NUMBER"

        #构建Docker镜像
        docker build -t $IMAGE_NAME .

        #若存在原镜像的容器，删除
        echo "【编译docker镜像】清理原始容器 -开始 - "$MODULE_NAME
        CONTAINER_NAME=$MODULE_NAME-$API_VERSION
        cid=$(docker ps -a | grep "$CONTAINER_NAME" | awk '{print $1}')
        if [ "$cid" != "" ];then
            docker rm -f $cid
            echo "【编译docker镜像】清理原始容器 -发现存在并清理 - "$MODULE_NAME
        fi
        echo "【编译docker镜像】清理原始容器 -结束 - "$MODULE_NAME

        #推送至harbor
        docker push $IMAGE_NAME
        echo "【编译docker镜像】推送harbor成功 - "$MODULE_NAME


        #删除Dockerfile文件
        rm -f  Dockerfile

        echo "【编译docker镜像】结束 - "$MODULE_NAME
    let i++
done


#开始构建ui的docker镜像
SERVER_NAME_ARR[5]="ui"
SERVER_NAME_PORT_ARR[5]=8080
echo "【编译docker - ui 镜像】开始 - "

    # 跳转到ui项目下
    cd /var/jenkins_home/workspace/$JENKINS_PRO_NAME/blog-ui
    # 添加镜像到服务中
    cp /var/jenkins_home/workspace/$JENKINS_PRO_NAME/blog-compile/docker/${SERVER_NAME_ARR[5]}/Dockerfile .

    # 只有项目资源，需要便以为可执行的dist文件=》 初始化vue项目，并编译，资源准备完毕
    npm install
    rm -rf ./dist/*
    npm run build

    # 开始构建
    CONTAINER_UI=blog-ui-$API_VERSION
    IMAGE_UI=$DOCKER_HUB_HOST:$DOCKER_HUB_PORT/$AUTHOR/blog-ui:$BUILD_NUMBER
    docker build -t $IMAGE_UI  .

    #检测ui容器是否能存在
    cid=$(docker ps -a | grep "$CONTAINER_UI" | awk '{print $1}')
    if [ "$cid" != "" ];then
        docker rm -f $cid
        echo "【编译docker - ui 镜像】清理容器 - "$MODULE_NAME
    fi

    #推送至harbor
    docker push $IMAGE_NAME
    echo "【编译docker - ui 镜像】推送harbor成功"

echo "【编译docker - ui 镜像】结束 - "

