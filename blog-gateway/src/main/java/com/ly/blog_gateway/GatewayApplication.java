package com.ly.blog_gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ly  * create at 2021/5/11 - 10:04
 **/
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.ly.blog_gateway","com.ly.blog_common.handler"})
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }

}
