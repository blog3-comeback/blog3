/**
 * 全局常量、方法封装模块
 * 通过原型挂载到Vue属性
 * 通过 this.Global 调用
 */

//本地服务
// const ip = 'localhost';
// 192.168.1.56
const ip = '192.168.1.56';

//docker服务
// const ip = '192.168.0.107';
const ip = '169.254.75.100'; //docker服务

//数据访问路径
export const baseUrl = 'http://'+ip+':8000';
// export const baseUrl = 'http://192.168.0.102:8000'

//操作文件路径，路径修改，以前存储图片可能不可用，保证服务存在可用
export const fileUrl = "http://"+ip+":8000/third/file"; //本地路径
// export const fileUrl =  "https://blog-yunglee.oss-cn-chengdu.aliyuncs.com"; //oss路径


export default {
    baseUrl,
    fileUrl
}
