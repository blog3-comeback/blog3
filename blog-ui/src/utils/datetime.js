/**
 * 时间日期相关操作
 */


/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成 2018/09/23 11:54:16
 * @param datetime 国际化日期格式
 */
export function format(datetime) {
    return formatWithSeperator(datetime, "-", ":");
}

export function formatDate(datetime) {
    return formatWithSeperatorToDate(datetime, "-");
}

/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成类似 2018/09/23 11:54:16
 * 可以指定日期和时间分隔符
 * @param datetime 国际化日期格式
 */
export function formatWithSeperator(datetime, dateSeprator, timeSeprator) {
    if (datetime != null) {
        const dateMat = new Date(datetime);
        const year = dateMat.getFullYear();
        const month = dateMat.getMonth() + 1;
        const day = dateMat.getDate();
        const hh = dateMat.getHours();
        const mm = dateMat.getMinutes();
        const ss = dateMat.getSeconds();
        const timeFormat = year + dateSeprator + month + dateSeprator + day + " " + hh + timeSeprator + mm + timeSeprator + ss;
        return timeFormat;
    }
}

/**
 * 时间格式化 - 去除时间
 * 将 2018-09-23T11:54:16.000+0000 格式化成类似 2018/09/23 11:54:16
 * 可以指定日期和时间分隔符
 * @param datetime 国际化日期格式
 */
export function formatWithSeperatorToDate(datetime, dateSeprator) {
    if (datetime != null) {
        const dateMat = new Date(datetime);
        const year = dateMat.getFullYear();
        const month = dateMat.getMonth() + 1;
        const day = dateMat.getDate();
        const dateFormat = year + dateSeprator + month + dateSeprator + day;
        return dateFormat;
    }
}


function padLeftZero(str) {
    return ('00' + str).substr(str.length);
}


export function formatWithSeperatorNewChina(datetime) {
    // return formatWithSeperatorNew(datetime,'yyyy年MM月dd日 hh:mm');
    return formatWithSeperatorNew(datetime,'yyyy-MM-dd hh:mm');
}
/**
 * 时间格式化
 * 将 2018-09-23T11:54:16.000+0000 格式化成类似 2018/09/23 11:54:16
 * 可以指定日期和时间分隔符
 * @param datetime 国际化日期格式
 */
export function formatWithSeperatorNew(datetime, fmt) {
    if (datetime != null) {
        const dateMat = new Date(datetime);
        const year = dateMat.getFullYear();
        const month = dateMat.getMonth() + 1;
        const day = dateMat.getDate();
        const hh = dateMat.getHours();
        const mm = dateMat.getMinutes();
        const ss = dateMat.getSeconds();
        // const timeFormat = year + dateSeprator + month + dateSeprator + day + " " + hh + timeSeprator + mm + timeSeprator + ss;


        if (/(y+)/.test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (year + '').substr(4 - RegExp.$1.length));
        }

        let o = {
            'M+': month,
            'd+': day,
            'h+': hh,
            'm+': mm,
            's+': ss,
        }

        for (let k in o) {
            if (new RegExp(`(${k})`).test(fmt)) {
                let str = o[k] + '';
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : padLeftZero(str))
            }
        }

        return fmt;
    }
}
