
import store from '@/store';
import Cookies from "js-cookie";

/**
 * 获取uuid
 */
export function getUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
    })
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth(key) {
    // return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
    return store.state.common.permissions.indexOf("key");
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate(data, id = '0') {


    return data.filter(menu => menu.parentId == id)
            .map(menu =>{
                let menuList = treeDataTranslate(data,menu.menuId);
                if(menuList && menuList.length > 0)
                    menu['children'] = menuList;
                return menu;
            });

}

/**
 * 清除登录信息
 */
export function clearLoginInfo() {
    //清除cookie
    Cookies.remove('token');
    //清除
    sessionStorage.removeItem("userInfo");
    sessionStorage.removeItem("permissions");
    sessionStorage.removeItem("menuList");
    //清除vuex
    store.commit('clearLoginInfo');
}

export function changeGetParam(data) {

    let param = '?';
    for(let key in data){
        param += `${key}=${data[key]}&`;
    }
    param = param.substring(0,param.length-1);
    console.log('参数信息：：'+param);
    return param;
}
