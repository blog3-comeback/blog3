/**
 * 混入日期转换
 */
import {formatWithSeperatorNew} from "@/utils/datetime"

export const formatDate = {
    filters: {
        formatDate(date) {
            return formatWithSeperatorNew(date, 'yyyy年MM月dd日 hh:mm');
        }
    }
}

