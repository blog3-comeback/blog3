/*
 * 接口统一集成模块
 */

import * as Login from './modules/sys/login';
import * as User from './modules/sys/user';
import * as Menu from './modules/sys/menu';
import * as Role from './modules/sys/role';
import * as Config from './modules/sys/config';
import * as Log from './modules/sys/log';
import * as Msg from './modules/sys/msg';

import * as Category from './modules/article/category';
import * as Info from './modules/article/info';
import * as Chapter from './modules/article/chapter';
import * as Label from './modules/article/label';
import * as Search from './modules/article/search';
import * as Issue from './modules/article/issue';

import * as File from './modules/third/file';

import * as DataTask from './modules/data/dataTask';
import * as DataResult from './modules/data/dataResult';

import * as EmailTemplate from './modules/email/template';


// 默认全部导出
export default {
    Login,
    User,
    Menu,
    Role,
    Config,
    Log,
    Msg,

    Category,
    Info,
    Chapter,
    Label,
    Search,
    Issue,

    File,

    DataTask,
    DataResult,

    EmailTemplate,

}
