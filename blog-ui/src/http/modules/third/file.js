import axios from '../../axios'

import { baseUrl } from '@/config/global'

/* 获取oss标签 */
export const ossPolicy = (data) => {
    return axios({
        url: `${baseUrl}/third/oss/policy`,
        method: 'post',
        data,
    })
};