import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


export const listAll = () => {
    return axios({
        url: `${baseUrl}/article/info/list/all`,
        method: 'get',
    })
};

export const list = (data) => {
    return axios({
        url: `${baseUrl}/article/info/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const map = (data) => {
    return axios({
        url: `${baseUrl}/article/info/list/map${changeGetParam(data)}`,
        method: 'get',
    })
};

//不采用axios调度，直接使用click
export const exportData = (data) => {
    window.location.href=`${baseUrl}/article/info/list/excel${changeGetParam(data)}`;
};

export const listBySearchKey = (data) => {
    return axios({
        url: `${baseUrl}/article/info/search/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/article/info/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/info/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/info/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/info/delete`,
        method: 'post',
        data
    })
};


//文章展示相关接口
export const showList = (data) => {
    return axios({
        url: `${baseUrl}/article/info/show/list${changeGetParam(data)}`,
        method: 'get',
    })
};

//文章展示相关接口
export const showInfo = (id) => {
    return axios({
        url: `${baseUrl}/article/info/show/info/${id}`,
        method: 'get',
    })
};