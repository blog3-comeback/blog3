import axios from '../../axios'

import { baseUrl } from '@/config/global'


// 获取树状list
export const listTree = () => {
    return axios({
        url: `${baseUrl}/article/category/list/tree`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/category/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/category/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/category/delete`,
        method: 'post',
        data
    })
};