import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


export const list = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const showList = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/show/list${changeGetParam(data)}`,
        method: 'get',
    })
};


export const info = (id) => {
    return axios({
        url: `${baseUrl}/article/issue/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/save`,
        method: 'post',
        data,
    })
};

export const addZan = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/update/zan/add`,
        method: 'post',
        data,
    })
};
export const cancelZan = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/update/zan/cancel`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/issue/delete`,
        method: 'post',
        data
    })
};