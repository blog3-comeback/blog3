import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


export const list = (data) => {
    return axios({
        url: `${baseUrl}/article/search/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const showList = (data) => {
    return axios({
        url: `${baseUrl}/article/search/show/list${changeGetParam(data)}`,
        method: 'get',
    })
};


export const info = (id) => {
    return axios({
        url: `${baseUrl}/article/search/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/search/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/search/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/search/delete`,
        method: 'post',
        data
    })
};