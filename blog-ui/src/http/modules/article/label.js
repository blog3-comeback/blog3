import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


export const list = (data) => {
    return axios({
        url: `${baseUrl}/article/label/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const showList = (data) => {
    return axios({
        url: `${baseUrl}/article/label/show/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const listAll = () => {
    return axios({
        url: `${baseUrl}/article/label/list/all`,
        method: 'get',
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/article/label/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/label/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/label/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/label/delete`,
        method: 'post',
        data
    })
};