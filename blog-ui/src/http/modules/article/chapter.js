import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


export const list = (data) => {
    return axios({
        url: `${baseUrl}/article/chapter/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const listShow = (data) => {
    return axios({
        url: `${baseUrl}/article/chapter/show/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/article/chapter/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/article/chapter/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}/article/chapter/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/article/chapter/delete`,
        method: 'post',
        data
    })
};