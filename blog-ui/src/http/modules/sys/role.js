import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


// 获取角色
export const list = (data) => {
    return axios({
        url: `${baseUrl}/sys/role/list${changeGetParam(data)}`,
        method: 'get',
    })
};

// 获取删除角色
export const del = (data) => {
    return axios({
        url: `${baseUrl}/sys/role/delete`,
        method: 'post',
        data
    })
};

// 获取角色
export const info = (id) => {
    return axios({
        url: `${baseUrl}/sys/role/info/${id}`,
        method: 'get',
    })
};

// 获取角色 - 获取用户自己拥有的角色
export const select = () => {
    return axios({
        url: `${baseUrl}/sys/role/select`,
        method: 'get',
    })
};


export const save = (data) => {
    return axios({
        url: `${baseUrl}/sys/role/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/sys/role/update`,
        method: 'post',
        data
    })
};
