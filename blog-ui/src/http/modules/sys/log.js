import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'

// 获取配置列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/sys/log/list${changeGetParam(data)}`,
        method: 'get',
    })
};