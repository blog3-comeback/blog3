import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'
/*
 * 系统用户模块
 */

// 获取用户信息
export const info = (id) => {
    return axios({
        url: baseUrl+'/sys/msg/info' + (id?`/${id}`:''),
        method: 'get',
    })
};

// 获取信息列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/sys/msg/list${changeGetParam(data)}`,
        method: 'get',
    })
};

// 获取用户未读列表
export const listNo = (data) => {
    return axios({
        url: `${baseUrl}/sys/msg/no/list${changeGetParam(data)}`,
        method: 'get',
    })
};

// 获取用户未读列表
export const listSys = () => {
    return axios({
        url: `${baseUrl}/sys/msg/sys/list`,
        method: 'get',
    })
};

// 获取用户信息状态
export const state = () => {
    return axios({
        url: `${baseUrl}/sys/msg/state`,
        method: 'get',
    })
};

// 获取删除用户
export const del = (data) => {
    return axios({
        url: `${baseUrl}/sys/msg/delete`,
        method: 'post',
        data
    })
};


export const save = (data) => {
    return axios({
        url: `${baseUrl}/sys/msg/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/sys/msg/update`,
        method: 'post',
        data
    })
};



