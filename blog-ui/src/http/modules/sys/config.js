import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


// 获取配置列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/sys/config/list${changeGetParam(data)}`,
        method: 'get',
    })
};
// 获取配置列表 - show
export const listShow = (type) => {
    return axios({
        url: `${baseUrl}/sys/config/show/list/${type}`,
        method: 'get',
    })
};
export const del = (data) => {
    return axios({
        url: `${baseUrl}/sys/config/delete`,
        method: 'post',
        data
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/sys/config/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/sys/config/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/sys/config/update`,
        method: 'post',
        data
    })
};