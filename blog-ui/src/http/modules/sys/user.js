import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'
/*
 * 系统用户模块
 */

// 获取用户信息
export const info = (id) => {
    return axios({
        url: baseUrl+'/sys/user/info' + (id?`/${id}`:''),
        method: 'get',
    })
};

// 获取展示的用户信息
export const showInfo = (id) => {
    return axios({
        url: baseUrl+'/sys/user/info/show' + (id?`/${id}`:''),
        method: 'get',
    })
};

// 获取登录的用户权限
export const permissions = () => {
    return axios({
        url: baseUrl+'/sys/user/permissions',
        method: 'get',
    })
}


// 获取用户列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/sys/user/list${changeGetParam(data)}`,
        method: 'get',
    })
};

// 获取删除用户
export const del = (data) => {
    return axios({
        url: `${baseUrl}/sys/user/delete`,
        method: 'post',
        data
    })
};


export const save = (data) => {
    return axios({
        url: `${baseUrl}/sys/user/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/sys/user/update`,
        method: 'post',
        data
    })
};

export const updatePassword = (data) => {
    return axios({
        url: `${baseUrl}/sys/user/password`,
        method: 'post',
        data
    })
};


