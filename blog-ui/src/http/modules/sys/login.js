import axios from '../../axios'

import { baseUrl } from '@/config/global'
/*
 * 系统登录模块
 */

// 登录
export const login = data => {
    return axios({
        url: baseUrl+'/sys/login',
        method: 'post',
        data
    })
}

export const loginEmail = data => {
    return axios({
        url: baseUrl+'/sys/login/email',
        method: 'post',
        data
    })
}

export const checkUsername = (username) => {
    return axios({
        url: `${baseUrl}/sys/check/username/${username}`,
        method: 'get'
    })
};

export const logout = () => {
    return axios({
        url: `${baseUrl}/sys/logout`,
        method: 'get'
    })
};

// 注册
export const register = data => {
    return axios({
        url: baseUrl+'/sys/register',
        method: 'post',
        data
    })
}

// 发送邮件
export const send = data => {
    return axios({
        url: baseUrl+'/sys/send',
        method: 'post',
        data
    })
}

