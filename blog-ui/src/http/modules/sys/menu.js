import axios from '../../axios'

import { baseUrl } from '@/config/global'
/*
 * 系统菜单模块
 */

// 获取用户菜单 -后端
export const nav = () => {
    return axios({
        url: baseUrl+'/sys/menu/nav',
        method: 'get',
    })
};

// 获取用户菜单 - 前端
export const showNav = () => {
    return axios({
        url: baseUrl+'/sys/menu/show/nav',
        method: 'get',
    })
};

// 获取全部菜单
export const list = () => {
    return axios({
        url: `${baseUrl}/sys/menu/list`,
        method: 'get',
    })
};

// 获取删除菜单
export const del = (id) => {
    return axios({
        url: `${baseUrl}/sys/menu/delete/${id}`,
        method: 'post',
    })
};

// 获取没有按钮的菜单信息
export const selectNoButtonMenuList = () => {
    return axios({
        url: `${baseUrl}/sys/menu/select`,
        method: 'get',
    })
};

// 获取没有按钮的菜单信息
export const info = (id) => {
    return axios({
        url: `${baseUrl}/sys/menu/info/${id}`,
        method: 'get',
    })
};

// 获取没有按钮的菜单信息
export const save = (data) => {
    return axios({
        url: `${baseUrl}/sys/menu/save`,
        method: 'post',
        data
    })
};

// 获取没有按钮的菜单信息
export const update = (data) => {
    return axios({
        url: `${baseUrl}/sys/menu/update`,
        method: 'post',
        data
    })
};

