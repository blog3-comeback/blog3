import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


// 获取配置列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/delete`,
        method: 'post',
        data
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/data/datatask/info/${id}`,
        method: 'get',
    })
};

export const exec = (id) => {
    return axios({
        url: `${baseUrl}/data/datatask/exec/${id}`,
        method: 'get',
    })
};

export const execPercentage = (id) => {
    return axios({
        url: `${baseUrl}/data/datatask/exec/percentage`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/update`,
        method: 'post',
        data
    })
};

export const parse = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/parse`,
        method: 'post',
        data
    })
};

export const check = (data) => {
    return axios({
        url: `${baseUrl}/data/datatask/check`,
        method: 'post',
        data
    })
};