import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'


// 获取配置列表
export const list = (data) => {
    return axios({
        url: `${baseUrl}/data/dataresult/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}/data/dataresult/delete`,
        method: 'post',
        data
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}/data/dataresult/info/${id}`,
        method: 'get',
    })
};

//推送文章
export const push = (data) => {
    return axios({
        url: `${baseUrl}/data/dataresult/push${changeGetParam(data)}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}/data/dataresult/save`,
        method: 'post',
        data
    })
};


export const update = (data) => {
    return axios({
        url: `${baseUrl}/data/dataresult/update`,
        method: 'post',
        data
    })
};