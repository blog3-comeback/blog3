import axios from '../../axios'

import { baseUrl } from '@/config/global'
import {changeGetParam } from '@/utils/index'

const coreUrl = "/third/emailtemplate";

export const list = (data) => {
    return axios({
        url: `${baseUrl}${coreUrl}/list${changeGetParam(data)}`,
        method: 'get',
    })
};

export const info = (id) => {
    return axios({
        url: `${baseUrl}${coreUrl}/info/${id}`,
        method: 'get',
    })
};

export const save = (data) => {
    return axios({
        url: `${baseUrl}${coreUrl}/save`,
        method: 'post',
        data,
    })
};

export const update = (data) => {
    return axios({
        url: `${baseUrl}${coreUrl}/update`,
        method: 'post',
        data,
    })
};

export const del = (data) => {
    return axios({
        url: `${baseUrl}${coreUrl}/delete`,
        method: 'post',
        data
    })
};