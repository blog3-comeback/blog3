import Vue from 'vue'
import App from './App.vue'
import Router from './router'
import Store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import api from '@/http'
import global from '@/config/global'
import 'font-awesome/css/font-awesome.min.css'

import { isAuth } from '@/utils' //鉴权使用
import {formatWithSeperatorNewChina} from "@/utils/datetime" //日期转换使用

Vue.config.productionTip = false;

Vue.use(ElementUI); // 注册使用Element
Vue.use(api);  // 注册使用API模块
Vue.prototype.global = global // 挂载全局配置模块
Vue.prototype.isAuth = isAuth     // 权限方法
Vue.prototype.formatDate = formatWithSeperatorNewChina     // 日期转换


/**
 * 地图（高德）
 * 初始化插件 - 初始化时，需要在index.html中引入对应js文件
 * */
import VueAMap from 'vue-amap'  //引入
Vue.use(VueAMap);		//全局注册
// 初始化vue-amap
VueAMap.initAMapApiLoader({
    //高德地图api开放平台申请的key
    key: '55480a5f539d30f29dd73539bdd02484',
    // 插件集合 （插件按需引入）
    plugin: [
        'AMap.Autocomplete',
        'AMap.PlaceSearch',
        'AMap.Scale',
        'AMap.OverView',
        'AMap.ToolBar',
        'AMap.MapType',
        'AMap.PolyEditor',
        'AMap.CircleEditor',
        'AMap.Geolocation'
    ],//依赖配置，根据自己的需求引入
    // 默认高德 sdk 版本为 1.4.4
    v: '1.4.4'
});


new Vue({
  router: Router,
  store:Store,
  render: h => h(App),
}).$mount('#app');
