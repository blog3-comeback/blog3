/**
 * 主题配置
 **/

export default {
    state: {
        themeColor:'white'
    },
    mutations: {
        updateThemeColor (state, themeColor) {
            state.themeColor = themeColor
        },
    },
}
