/**
 * 通用性配置 全局保存
 **/


export default {
    state: {
        /*公用信息配置 -s*/
        //登录用户信息
        userInfo:{},
        //权限信息
        permissions:[],
        /*公用信息配置 -e*/


        /*后端界面通用配置 - s*/
        // 侧边栏, 折叠状态
        sidebarFold: false,
        //选中菜单
        menuActiveName: 'home',
        //用户菜单列表 - 路由初始化
        menuList: [],
        //菜单展示标签列表
        mainTabs:[],
        //当前选择的标签
        mainTabsActiveName: '',
        /*后端界面通用配置 - e*/


        /*前端界面通用配置 - s*/

        showMenuList:[],
        showMenuActiveName:'showHome',

        address:'',

        /*前端界面通用配置 - e*/


        /* 当前页面状态 pre:前端 back后端 -s */
        pageState:'pre',
        /* 当前页面状态 -e */

    },

    mutations: {

        updateSidebarFold (state, fold) {
            state.sidebarFold = fold
        },
        updateMenuActiveName (state, activeName) {
            state.menuActiveName = activeName
        },
        updateMenuList (state, list) {
            state.menuList = list
        },
        updateMainTabs (state, list) {
            state.mainTabs = list
        },
        updateMainTabsActiveName (state, mainTabsActiveName) {
            state.mainTabsActiveName = mainTabsActiveName
        },
        updateUserInfo (state, userInfo) {
            state.userInfo = userInfo
        },
        updatePermissions (state, permissions) {
            state.permissions = permissions
        },
        clearLoginInfo(state){
            state.menuActiveName = ''
            state.menuList = []
            state.mainTabs = []
            state.mainTabsActiveName = ''
            state.userInfo = {}
            state.permissions = []

        },



        updateShowMenuList (state, list) {
            state.showMenuList = list
        },
        updateShowMenuActiveName (state, showMenuActiveName) {
            state.showMenuActiveName = showMenuActiveName
        },
        updatePageState (state, pageState) {
            state.pageState = pageState
        },

    }
}
