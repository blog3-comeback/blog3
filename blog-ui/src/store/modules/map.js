/**
 * 地图信息
 **/

export default {
    state: {
        //当前位置
        currentAddress:{},
    },
    mutations: {
        updateCurrentAddress (state, currentAddress) {
            state.currentAddress = currentAddress
        },
    },
}
