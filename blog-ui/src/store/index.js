import Vue from 'vue'
import vuex from 'vuex'
// 引入子模块

Vue.use(vuex);

import common from './modules/common'
import theme from './modules/theme'
import map from './modules/map'

const store = new vuex.Store({
    modules:{
        common,
        theme,
        map,
    },
    mutations: {
    }
})

export default store
