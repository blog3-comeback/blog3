module.exports = {

    lintOnSave: false,

    configureWebpack(config) {
        config.externals = {
            'AMap': 'AMap' // 高德地图配置
        }
    },

    // publicPath: './',
    // outputDir: 'blog-ui',
    // assetsDir: 'static',
    // productionSourceMap: false,

    devServer: {
        open: true,
        disableHostCheck: true,
        // host: 'localhost',
        port: 8080,
        https: false,
        hotOnly: false,
        proxy: {
            '/api': {
                target: 'http://localhost:8080',
                changeOrigin: true
            }
        },


    },
    //configureWebpack: {},

    //解决 localhost可以访问，但本地无法访问的问题
    // devServer: {
    //     //host: '0.0.0.0',
    //     //port: 8080,
    //
    //     proxy: null, // string | Object
    //     public: '0.0.0.0:8080',
    //     disableHostCheck: true,
    //     before(app){}
    // }


};