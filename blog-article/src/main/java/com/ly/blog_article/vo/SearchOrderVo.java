package com.ly.blog_article.vo;

import lombok.Data;

/**
 * 检索值历史汇总输出
 * @author ly create at 2021/6/30 - 17:44
 **/
@Data
public class SearchOrderVo {

    private String searchKey;

    private String number;

}
