package com.ly.blog_article.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 文章地图实体类
 *
 * @author ly
 * create at 2021/9/26 - 17:51
 **/
@Data
public class InfoMapVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String lng;
    private String lat;
    private String addr;
    //文章量
    private int articleNum;
    //文章作者量
    private int userNum;

}
