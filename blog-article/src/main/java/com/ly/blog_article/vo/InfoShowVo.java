package com.ly.blog_article.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 展示文章用的Bean
 * @author ly create at 2021/6/18 - 23:33
 **/
@Data
public class InfoShowVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 作者
     */
    private String author;
    /**
     * 标题
     */
    private String title;
    /**
     * 浏览次数
     */
    private Long watchTime;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String lastUpdateBy;
    /**
     * 更新时间
     */
    private Date lastUpdateTime;
    /**
     * 文章展示的URL
     */
    private String showImg;
    /**
     * 标签ID，选择 , 先隔开
     */
    private String labelName;
    /**
     * 分类
     */
    private String catName;
    /**
     * 内容
     */
    private String content;
}
