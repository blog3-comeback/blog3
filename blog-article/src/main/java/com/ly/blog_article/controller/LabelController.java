package com.ly.blog_article.controller;

import com.ly.blog_article.entity.LabelEntity;
import com.ly.blog_article.service.LabelService;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@RestController
@RequestMapping("article/label")
public class LabelController {
    @Autowired
    private LabelService labelService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @PreAuthorize("hasAuthority('article:label:list')")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = labelService.queryPage(params);
        return R.ok().put("page", page);
    }

    @RequestMapping("show/list")
    public R showList(@RequestParam Map<String, Object> params){
        PageUtils page = labelService.queryShowPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @RequestMapping("/list/all")
    @PreAuthorize("hasAuthority('article:label:list')")
    public R listAll(){
        List<LabelEntity> labelEntities= labelService.listAll();

        return R.ok().put("labelList", labelEntities);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @PreAuthorize("hasAuthority('article:label:info')")
    public R info(@PathVariable("id") Integer id){
		LabelEntity label = labelService.getById(id);

        return R.ok().put("label", label);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @PreAuthorize("hasAuthority('article:label:save')")
    public R save(@RequestBody LabelEntity label){
        label.setCreateBy(SecurityUtils.getUserId());
        label.setCreateTime(new Date());
        label.setLastUpdateBy(SecurityUtils.getUserId());
        label.setLastUpdateTime(new Date());
		labelService.save(label);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('article:label:update')")
    public R update(@RequestBody LabelEntity label){
        label.setLastUpdateBy(SecurityUtils.getUserId());
        label.setLastUpdateTime(new Date());
		labelService.updateById(label);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @PreAuthorize("hasAuthority('article:label:delete')")
    public R delete(@RequestBody Integer[] ids){
		labelService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
