package com.ly.blog_article.controller;

import com.ly.blog_article.service.ChapterService;
import com.ly.blog_common.entity.ChapterEntity;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@RestController
@RequestMapping("article/chapter")
public class ChapterController {
    @Autowired
    private ChapterService chapterService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = chapterService.queryPage(params);

        return R.ok().put("page", page);
    }

    @RequestMapping("/show/list")
    public R listShow(@RequestParam Map<String, Object> params){
        PageUtils page = chapterService.queryPageShow(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @PreAuthorize("hasAuthority('article:chapter:info')")
    public R info(@PathVariable("id") Long id){
		ChapterEntity chapter = chapterService.getById(id);

        //增加浏览记录
        chapter.setWatchTime(chapter.getWatchTime() + 1);
        boolean update = chapterService.updateById(chapter);

        return R.ok().put("chapter", chapter);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @PreAuthorize("hasAuthority('article:chapter:save')")
    public R save(@RequestBody ChapterEntity chapter){
        chapter.setCreateBy(SecurityUtils.getUserId());
        chapter.setCreateTime(new Date());
        chapter.setLastUpdateBy(SecurityUtils.getUserId());
        chapter.setLastUpdateTime(new Date());
		chapterService.save(chapter);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('article:chapter:update')")
    public R update(@RequestBody ChapterEntity chapter){
        chapter.setLastUpdateBy(SecurityUtils.getUserId());
        chapter.setLastUpdateTime(new Date());
		chapterService.updateById(chapter);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @PreAuthorize("hasAuthority('article:chapter:delete')")
    public R delete(@RequestBody Long[] ids){
		chapterService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }


    /**
     * feign推送接口调度
     * /

    /** 检测检索结果是否调度
     * @param id
     * @return
     */
    @RequestMapping("/push/result")
    public Integer pushResult(@RequestBody Integer id){
        return chapterService.pushResult(id);
    }

}
