package com.ly.blog_article.controller;

import com.ly.blog_article.entity.CategoryEntity;
import com.ly.blog_article.service.CategoryService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@RestController
@RequestMapping("article/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @PreAuthorize("hasAuthority('article:category:list')")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = categoryService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 树状列表
     */
    @RequestMapping("/list/tree")
    public R listWithTree(){
        List<CategoryEntity> categoryTrees = categoryService.listWithTree();
        return R.ok().put("categoryTreeList", categoryTrees);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @PreAuthorize("hasAuthority('article:category:info')")
    public R info(@PathVariable("id") Long id){
		CategoryEntity category = categoryService.getById(id);
        return R.ok().put("category", category);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @PreAuthorize("hasAuthority('article:category:save')")
    public R save(@RequestBody CategoryEntity category){
		categoryService.save(category);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @PreAuthorize("hasAuthority('article:category:update')")
    public R update(@RequestBody CategoryEntity category){
		categoryService.updateById(category);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @PreAuthorize("hasAuthority('article:category:delete')")
    public R delete(@RequestBody Long[] ids){
		categoryService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }

}
