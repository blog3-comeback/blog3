package com.ly.blog_article.controller;

import com.ly.blog_article.entity.SearchEntity;
import com.ly.blog_article.service.SearchService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-30 09:37:16
 */
@RestController
@RequestMapping("article/search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    /**
     * 列表
     */
    @RequestMapping("/list")
//    @PreAuthorize("hasAuthority('article:search:list')")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = searchService.queryPage(params);

        return R.ok().put("page", page);
    }

    /**
     * 展示列表
     * @param params
     * @return
     */
    @RequestMapping("/show/list")
//    @PreAuthorize("hasAuthority('article:search:list')")
    public R listShow(@RequestParam Map<String, Object> params){
        PageUtils page = searchService.queryShowPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
//    @PreAuthorize("hasAuthority('article:search:info')")
    public R info(@PathVariable("id") Integer id){
		SearchEntity search = searchService.getById(id);

        return R.ok().put("search", search);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
//    @PreAuthorize("hasAuthority('article:search:save')")
    public R save(@RequestBody SearchEntity search){
        search.setCreateTime(new Date());
        search.setLastUpdateTime(new Date());
		searchService.save(search);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
//    @PreAuthorize("hasAuthority('article:search:update')")
    public R update(@RequestBody SearchEntity search){
		searchService.updateById(search);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
//    @PreAuthorize("hasAuthority('article:search:delete')")
    public R delete(@RequestBody Integer[] ids){
		searchService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
