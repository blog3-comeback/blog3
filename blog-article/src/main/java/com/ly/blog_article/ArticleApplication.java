package com.ly.blog_article;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ly create at 2021/5/11 - 10:04
 **/
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = {"com.ly"})
@EnableFeignClients(basePackages = {"com.ly.blog_article.feign"})
public class ArticleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArticleApplication.class,args);
    }

}
