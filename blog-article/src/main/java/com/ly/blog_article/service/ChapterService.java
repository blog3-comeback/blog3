package com.ly.blog_article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.entity.ChapterEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
public interface ChapterService extends IService<ChapterEntity> {

    Integer pushResult(Integer id);

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageShow(Map<String,Object> params);
}

