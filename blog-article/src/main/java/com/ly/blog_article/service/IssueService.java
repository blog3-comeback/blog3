package com.ly.blog_article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_article.entity.IssueEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.Map;


/**
 * 
 *
 * @author liyang
 *
 * @date 2021-07-30 10:37:42
 */
public interface IssueService extends IService<IssueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryShowPage(Map<String,Object> params);

    void addRecord(IssueEntity issue);

    void cancelRecord(IssueEntity issue);
}

