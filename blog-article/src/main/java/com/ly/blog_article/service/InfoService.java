package com.ly.blog_article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_article.vo.InfoMapVo;
import com.ly.blog_article.vo.InfoShowVo;
import com.ly.blog_common.entity.InfoEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
public interface InfoService extends IService<InfoEntity> {

    List<InfoMapVo> listMap(Map<String,Object> params);

    PageUtils queryPage(Map<String, Object> params);

    List<InfoEntity> listAll();

    PageUtils queryShowPage(Map<String,Object> params);

    InfoShowVo getShowById(Long id);

    PageUtils querySearchPage(Map<String,Object> params);
}

