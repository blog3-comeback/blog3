package com.ly.blog_article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_article.dao.ChapterDao;
import com.ly.blog_article.service.ChapterService;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.entity.ChapterEntity;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import com.ly.blog_common.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;


@Service("chapterService")
public class ChapterServiceImpl extends ServiceImpl<ChapterDao, ChapterEntity> implements ChapterService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        params.put(Constant.ORDER_FIELD,"order_number");
        params.put(Constant.ORDER,Constant.ASC);

        IPage<ChapterEntity> page = this.page(
                new Query<ChapterEntity>().getPage(params),
                new QueryWrapper<ChapterEntity>().eq("info_id",params.get("infoId"))
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageShow(Map<String, Object> params) {

        params.put(Constant.ORDER_FIELD,"order_number");
        params.put(Constant.ORDER,Constant.ASC);

        IPage<ChapterEntity> page = this.page(
                new Query<ChapterEntity>().getPage(params),
                new QueryWrapper<ChapterEntity>().eq("info_id",params.get("infoId")).eq("is_show","1")
        );

        return new PageUtils(page);
    }

    @Override
    public boolean save(ChapterEntity entity) {
        entity.setContentText(StringUtils.removeHtmlMark(entity.getContent()));
        super.save(entity);
        return true;
    }

    @Override
    public boolean updateById(ChapterEntity entity) {
        entity.setContentText(StringUtils.removeHtmlMark(entity.getContent()));
        super.updateById(entity);
        return true;
    }

    @Override
    public Integer pushResult(Integer id) {
        return this.baseMapper.pushResult(id);
    }
}