package com.ly.blog_article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_article.entity.CategoryEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();
}

