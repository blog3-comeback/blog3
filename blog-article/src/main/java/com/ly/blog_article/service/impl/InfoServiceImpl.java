package com.ly.blog_article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_article.dao.InfoDao;
import com.ly.blog_article.vo.InfoMapVo;
import com.ly.blog_article.service.InfoService;
import com.ly.blog_article.vo.InfoSearchVo;
import com.ly.blog_article.vo.InfoShowVo;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.entity.InfoEntity;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.MapUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import com.ly.blog_common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("infoService")
public class InfoServiceImpl extends ServiceImpl<InfoDao, InfoEntity> implements InfoService {

    @Autowired
    private InfoDao infoDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        QueryWrapper queryWrapper = new QueryWrapper<InfoEntity>();
        String userId = SecurityUtils.getUserId();
        if(!String.valueOf(Constant.SUPER_ADMIN).equals(userId)){
            queryWrapper.eq("create_by",userId);
        }

        //添加时间倒序排序
        params.put(Constant.ORDER_FIELD,"create_time");
        params.put(Constant.ORDER,Constant.DESC);

        IPage<InfoEntity> page = this.page(
                new Query<InfoEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryShowPage(Map<String, Object> params) {
        //添加时间倒序排序
        if(params.containsKey("watchTime")){
            params.put(Constant.ORDER_FIELD,"watch_time");
            params.put(Constant.ORDER,Constant.DESC);
        }else{
            params.put(Constant.ORDER_FIELD,"create_time");
            params.put(Constant.ORDER,Constant.DESC);
        }

        String checkedKeysStr;
        if(params.get("checkedKeysStr") != null && !StringUtils.isBlank(checkedKeysStr = params.get("checkedKeysStr").toString())){
            params.put("checkedKeys",checkedKeysStr.split(","));
        }

        IPage<InfoShowVo> page = new Query<InfoShowVo>().getPage(params);
        List<InfoShowVo> infoShowVos = infoDao.queryShowPage(params);

        //处理内容格式
        infoShowVos.forEach(infoShowVo -> {
            String content = infoShowVo.getContent();
            final int limitLength = 200;
            if(content != null && content.length() > limitLength){
                content = content.substring(0,limitLength) + "...";
            }
            infoShowVo.setContent(content);
        });

        page.setRecords(infoShowVos);
        return new PageUtils(page);
    }

    @Override
    public PageUtils querySearchPage(Map<String, Object> params) {
        final String searchKey;
        if(params.get("searchKey") == null || StringUtils.isBlank(searchKey = params.get("searchKey").toString())){
            throw new BlogException("检索值不能为空！");
        }
        IPage<InfoSearchVo> page = new Query<InfoSearchVo>().getPage(params);
        List<InfoSearchVo> infoSearchVos = infoDao.querySearchPage(params);

        //处理内容格式
        infoSearchVos.forEach(infoSearchVo -> {
            String content = infoSearchVo.getContent();
            String title = infoSearchVo.getTitle();
            final int limitLength = 200;
            if("content".equals(infoSearchVo.getType())) {
                content = StringUtils.markKeyRedAndLimit(content, searchKey, limitLength);
                infoSearchVo.setContent(content);
            }
            if("title".equals(infoSearchVo.getType())) {
                title = StringUtils.markKeyRedAndLimit(title, searchKey, limitLength);
                infoSearchVo.setTitle(title);
            }
            if("content,title".equals(infoSearchVo.getType())) {
                title = StringUtils.markKeyRedAndLimit(title, searchKey, limitLength);
                content = StringUtils.markKeyRedAndLimit(content, searchKey, limitLength);
                infoSearchVo.setTitle(title);
                infoSearchVo.setContent(content);
            }
        });

        page.setRecords(infoSearchVos);
        return new PageUtils(page);
    }

    @Override
    public List<InfoEntity> listAll() {
        QueryWrapper queryWrapper = new QueryWrapper<InfoEntity>();
        queryWrapper.orderByDesc("last_update_time");
        String userId = SecurityUtils.getUserId();
        if(!String.valueOf(Constant.SUPER_ADMIN).equals(userId)){
            queryWrapper.eq("create_by",userId);
        }
        return this.list(queryWrapper);
    }

    @Override
    public InfoShowVo getShowById(Long id) {
        List<InfoShowVo> infoShowVos = infoDao.queryShowPage(new MapUtils().put("id", id));
        return (infoShowVos == null || infoShowVos.size() == 0) ? null : infoShowVos.get(0);
    }

    @Override
    public List<InfoMapVo> listMap(Map<String, Object> params) {
        return infoDao.listMap(params);
    }
}