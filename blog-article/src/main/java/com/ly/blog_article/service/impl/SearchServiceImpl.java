package com.ly.blog_article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_article.dao.SearchDao;
import com.ly.blog_article.entity.SearchEntity;
import com.ly.blog_article.service.SearchService;
import com.ly.blog_article.vo.SearchOrderVo;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("searchService")
public class SearchServiceImpl extends ServiceImpl<SearchDao, SearchEntity> implements SearchService {

    @Autowired
    private SearchDao searchDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SearchEntity> page = this.page(
                new Query<SearchEntity>().getPage(params),
                new QueryWrapper<SearchEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryShowPage(Map<String, Object> params) {
        IPage<SearchOrderVo> page = new Query<SearchOrderVo>().getPage(params);
        List<SearchOrderVo> searchEntities = searchDao.queryShowPage(params);
        page.setRecords(searchEntities);
        return new PageUtils(page);
    }
}