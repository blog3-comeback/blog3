package com.ly.blog_article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_article.entity.SearchEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-30 09:37:16
 */
public interface SearchService extends IService<SearchEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryShowPage(Map<String,Object> params);
}

