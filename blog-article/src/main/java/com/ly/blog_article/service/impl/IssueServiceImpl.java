package com.ly.blog_article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_article.dao.IssueDao;
import com.ly.blog_article.dao.RecordDao;
import com.ly.blog_article.entity.IssueEntity;
import com.ly.blog_article.entity.RecordEntity;
import com.ly.blog_article.feign.UserService;
import com.ly.blog_article.service.IssueService;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.entity.SysUserEntity;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.response.BlogResponseStatus;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;



@Service("issueService")
public class IssueServiceImpl extends ServiceImpl<IssueDao, IssueEntity> implements IssueService {

    @Autowired
    private UserService userService;
    @Autowired
    private RecordDao recordDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        params.put(Constant.ORDER_FIELD,"create_time");
        params.put(Constant.ORDER,Constant.DESC);

        IPage<IssueEntity> page = this.page(
                new Query<IssueEntity>().getPage(params),
                new QueryWrapper<IssueEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryShowPage(Map<String, Object> params) {

        //增加排序功能 + = 赞+时间
        String[] arr = {"zan","create_time"};
        String[] arrOrder = {Constant.DESC,Constant.DESC};
        params.put(Constant.ORDER_FIELD,arr);
        params.put(Constant.ORDER,arrOrder);

        IPage<IssueEntity> page = this.page(
                new Query<IssueEntity>().getPage(params),
                new QueryWrapper<IssueEntity>().eq("is_show",0).eq("content_id",params.get("contentId"))
        );

        //增加用户点赞状态 + 用户名获取
        page.getRecords().forEach(item->{

            RecordEntity recordEntity = new RecordEntity();
            recordEntity.setContentId(item.getId());
            recordEntity.setIsCancel(0);
            recordEntity.setType(2);
            String userId;
            if((userId = SecurityUtils.getUserId()) != null){
                recordEntity.setCreateBy(userId);
            }else{
                recordEntity.setCreateBy("");
            }
            RecordEntity entity = recordDao.selectOne(new QueryWrapper<>(recordEntity));
            if(entity != null){
                item.setIsZan(1);
            }else{
                item.setIsZan(0);
            }

            //用户名
            SysUserEntity sysUserEntity = userService.infoShow(Long.parseLong(item.getCreateBy()));
            item.setUsername(sysUserEntity == null?"【未知】":sysUserEntity.getAlias());

        });

        return new PageUtils(page);
    }

    @Override
    public void addRecord(IssueEntity issue) {
        RecordEntity recordEntity = new RecordEntity();
        recordEntity.setContentId(issue.getId());
        recordEntity.setType(2); //评论赞记录
        recordEntity.setIsCancel(0); // 0不取消 1取消
        recordEntity.setCreateBy(issue.getCreateBy());
        RecordEntity recordEntityReal = recordDao.selectOne(new QueryWrapper<>(recordEntity));

        if(recordEntityReal !=null){
            throw new BlogException(BlogResponseStatus.ISSUE_ADD_ZAN);
        }

        recordEntity.setCreateTime(new Date());
        recordEntity.setLastUpdateBy(issue.getLastUpdateBy());
        recordEntity.setLastUpdateTime(new Date());
        recordDao.insert(recordEntity);
    }

    @Override
    public void cancelRecord(IssueEntity issue) {
        RecordEntity recordEntity = new RecordEntity();
        recordEntity.setContentId(issue.getId());
        recordEntity.setIsCancel(0); // 查询条件 + 0不取消 1取消
        recordEntity.setType(2); // 文章记录类型： 0，文章阅读记录 1，文章点赞记录 2 文章评论点赞记录
        recordEntity.setCreateBy(issue.getLastUpdateBy());
        RecordEntity recordEntityReal = recordDao.selectOne(new QueryWrapper<>(recordEntity));

        recordEntityReal.setIsCancel(1); // 修改参数 + 0不取消 1取消
        recordEntityReal.setLastUpdateBy(issue.getLastUpdateBy());
        recordEntityReal.setLastUpdateTime(new Date());
        recordDao.updateById(recordEntityReal);
    }
}