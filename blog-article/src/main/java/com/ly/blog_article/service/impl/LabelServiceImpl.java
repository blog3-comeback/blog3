package com.ly.blog_article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_article.dao.LabelDao;
import com.ly.blog_article.entity.LabelEntity;
import com.ly.blog_article.service.LabelService;
import com.ly.blog_article.vo.LabelShowVo;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("labelService")
public class LabelServiceImpl extends ServiceImpl<LabelDao, LabelEntity> implements LabelService {
    @Autowired
    private LabelDao labelDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper queryWrapper = new QueryWrapper<LabelEntity>();
        String userId = SecurityUtils.getUserId();
        if(!String.valueOf(Constant.SUPER_ADMIN).equals(userId)){
            queryWrapper.eq("create_by",userId);
        }
        IPage<LabelEntity> page = this.page(
                new Query<LabelEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryShowPage(Map<String, Object> params) {
        IPage<LabelShowVo> page = new Query<LabelShowVo>().getPage(params);
        List<LabelShowVo> labelShowVos = labelDao.queryPage();
        page.setRecords(labelShowVos);
        return new PageUtils(page);
    }

    @Override
    public List<LabelEntity> listAll() {
        return this.list();
    }
}