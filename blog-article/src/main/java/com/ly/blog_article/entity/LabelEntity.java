package com.ly.blog_article.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Data
@TableName("article_label")
public class LabelEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 标签名
	 */
	private String name;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;
	/**
	 * 是否删除  -1：已删除  0：正常
	 */
	private Integer delFlag;

}
