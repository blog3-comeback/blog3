package com.ly.blog_article.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-07-30 10:37:42
 */
@Data
@TableName("article_issue")
public class IssueEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 评论类型 - 0、留言；1、文章评论
	 */
	private Integer type;
	/**
	 * 父级id，楼主评论，-1
	 */
	private Integer parentId;
	/**
	 * 评价对象id，对应 type=1
	 */
	private Integer contentId;
	/**
	 * 评价zan数
	 */
	private Integer zan;
	/**
	 * 言论，评价内容
	 */
	private String word;
	/**
	 * 是否展示
	 */
	private Integer isShow;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;

    /**
     * 当前用户是否点赞 0，没 1，已点
     */
    @TableField(exist = false)
	private Integer isZan;

    @TableField(exist = false)
	private String username;

}
