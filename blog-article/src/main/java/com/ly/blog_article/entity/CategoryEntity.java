package com.ly.blog_article.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Data
@TableName("article_category")
public class CategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@TableId
	private Long id;
	/**
	 * 分类名
	 */
	private String name;
	/**
	 * 父级id
	 */
	private Long parentId;
	/**
	 * 层级
	 */
	private String catLevel;
	/**
	 * 是否显示（0不显示、1显示）
	 */
	private Integer showStatus;
	/**
	 * 排序
	 */
	private Integer sort;
	/**
	 * 图标地址
	 */
	private String icon;
	/**
	 * 分类下数量
	 */
	private Integer count;
	/**
	 * 版本字段
	 */
	private Integer version;

    @JsonInclude(JsonInclude.Include.NON_EMPTY) //不为空时，放到json中
    @TableField(exist = false)
    private List children;

}
