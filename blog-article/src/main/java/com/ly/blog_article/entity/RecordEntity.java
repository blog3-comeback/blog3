package com.ly.blog_article.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-07-30 10:37:42
 */
@Data
@TableName("article_record")
public class RecordEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 文章记录类型： 0，文章阅读记录 1，文章点赞记录 2 文章评论点赞记录
	 */
	private Integer type;
	/**
	 * 评价对象id，对应 type=1
	 */
//	private Integer articleId;
    private Integer contentId;
    /**
     * 是否取消 0.不取消  1、取消
     */
    private Integer isCancel;

	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;

}
