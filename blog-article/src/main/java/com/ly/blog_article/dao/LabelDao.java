package com.ly.blog_article.dao;

import com.ly.blog_article.entity.LabelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.vo.LabelShowVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Mapper
public interface LabelDao extends BaseMapper<LabelEntity> {

    List<LabelShowVo> queryPage();
}
