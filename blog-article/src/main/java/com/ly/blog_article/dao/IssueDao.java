package com.ly.blog_article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.entity.IssueEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;


/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-07-30 10:37:42
 */
@Mapper
public interface IssueDao extends BaseMapper<IssueEntity> {

    List<IssueEntity> selectShowList(Map<String,Object> params);

}
