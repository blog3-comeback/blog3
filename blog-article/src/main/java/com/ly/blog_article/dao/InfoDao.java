package com.ly.blog_article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.vo.InfoMapVo;
import com.ly.blog_article.vo.InfoSearchVo;
import com.ly.blog_article.vo.InfoShowVo;
import com.ly.blog_common.entity.InfoEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Mapper
public interface InfoDao extends BaseMapper<InfoEntity> {

    List<InfoMapVo> listMap(Map<String,Object> params);

    List<InfoShowVo> queryShowPage(Map<String,Object> params);

    List<InfoSearchVo> querySearchPage(Map<String,Object> params);
}
