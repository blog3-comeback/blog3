package com.ly.blog_article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.entity.RecordEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-07-30 10:37:42
 */
@Mapper
public interface RecordDao extends BaseMapper<RecordEntity> {
	
}
