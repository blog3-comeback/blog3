package com.ly.blog_article.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.entity.CategoryEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
