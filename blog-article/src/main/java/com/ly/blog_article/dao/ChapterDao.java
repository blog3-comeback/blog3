package com.ly.blog_article.dao;

import com.ly.blog_common.entity.ChapterEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Mapper
public interface ChapterDao extends BaseMapper<ChapterEntity> {

    Integer pushResult(@Param("id") Integer id);
}
