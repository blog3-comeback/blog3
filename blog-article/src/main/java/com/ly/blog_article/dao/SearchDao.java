package com.ly.blog_article.dao;

import com.ly.blog_article.entity.SearchEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_article.vo.SearchOrderVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-30 09:37:16
 */
@Mapper
public interface SearchDao extends BaseMapper<SearchEntity> {

    List<SearchOrderVo> queryShowPage(Map<String,Object> params);

}
