package com.ly.blog_article.feign;

import com.ly.blog_common.entity.mq.MqMsgEntity;
import com.ly.blog_common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 第三方接口
 * @author ly create at 2021/6/24 - 9:48
 **/
@Service
@FeignClient(value = "blog-third")
public interface ThirdService {

    @PostMapping("/third/mq/send")
    public R send(@RequestBody MqMsgEntity mqMsgEntity);


}
