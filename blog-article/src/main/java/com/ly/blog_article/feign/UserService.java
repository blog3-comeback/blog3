package com.ly.blog_article.feign;

import com.ly.blog_article.feign.config.FeignConfig;
import com.ly.blog_common.entity.MsgEntity;
import com.ly.blog_common.entity.SysUserEntity;
import com.ly.blog_common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 文章接口
 * @author ly create at 2021/6/26 - 13:44
 **/
@Service
@FeignClient(value = "blog-user", configuration = FeignConfig.class)
public interface UserService {

    @PostMapping("/sys/user/info/show")
    public SysUserEntity infoShow(@RequestBody Long userId);

    @RequestMapping("/sys/msg/save")
    public R saveMsg(@RequestBody MsgEntity msg);

}
