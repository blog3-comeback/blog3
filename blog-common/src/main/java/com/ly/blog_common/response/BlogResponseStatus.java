package com.ly.blog_common.response;

/**
 * 定义返回错误码和信息
 * @author ly create at 2021/6/2 - 11:00
 *
 * 解释：
 *  系统级问题： 0-999
 *  业务问题：1000 - ...
 *      1100 - 1199 : 用户相关
 *      1200 - 1299 : 登录相关
 *
 *
 **/
public enum BlogResponseStatus {

    SYSTEM_UNKNOW(500,"未知异常，请联系管理员")

    ,USER_INFO_NO_EXIST(1100,"用户不存在")
    ,USER_PASSWORD_NO_EXIST(1101,"用户原始密码错误")

    ,ROLE_OUT_PROMISE_ERROR(1300,"新增角色的权限，已超出你的权限范围")

    ,USER_LOGIN_ERROR(1800,"用户登录失败，未知错误")
    ,USER_LOGIN_USERNAME_NO_EXIST(1801,"用户登录失败，用户不存在")
    ,USER_LOGIN_PASSWORD_ERROR(1802,"用户登录失败，密码错误")
    ,USER_LOGIN_PASSWORD_NO_EXIST(1803,"用户登录失败，密码为空")
    ,USER_LOGIN_LOCKED(1804,"用户账号已被锁定，请联系管理员")
    ,USER_LOGIN_CAPTCHA_ERROR(1805,"验证码不正确")
    ,USER_REGISTER_USERNAME_EXIST(1806,"注册登录用户名已存在")
    ,EMAIL_SEND_ERROR(1807,"发送邮件失败")
    ,EMAIL_CODE_ERROR(1808,"邮箱验证码无效")
    ,EMAIL_USED_ERROR(1809,"邮箱已被使用，无法注册")
    ,EMAIL_NO_REGISTER_ERROR(1810,"邮件没有注册账号，可以先注册下")
    ,EMAIL_NO_EMPTY(1812,"邮箱不能为空")
    ,EMAIL_SEND_DELAY(1813,"邮件需要发送过于频繁，请在等待时间后重试（1分钟）")

    ,SQL_EXCLUDE_WORD_ERROR(1900,"SQL异常，包含非法字符，SQL关键字")
    ,MQ_SEND_ERROR(2000,"MQ消息发送失败")

    ,USER_NO_LOGIN(5000,"用户没有登录，无法操作！")
    ,ISSUE_ADD_ZAN(5001,"用户已经点赞，无法再次点击！")
    ,ISSUE_WORD_EMPTY(5002,"内容不可为空哦！")
    ,USERNAME_USED(5003,"用户名已被使用，无法注册！")

    ,CATALOGUE_AREA_EMPTY(7100,"目录区间为空")
    ,CATALOGUE_CONTENT_EMPTY(7101,"目录内容为空")
    ;

    private final int value;
    private final String msg;

    private BlogResponseStatus(int value, String msg) {
        this.value = value;
        this.msg = msg;
    }

    public int value() {
        return value;
    }

    public String msg() {
        return msg;
    }

}
