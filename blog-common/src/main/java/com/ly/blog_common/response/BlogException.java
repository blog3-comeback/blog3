package com.ly.blog_common.response;

/**
 * 博客异常
 * @author ly create at 2021/6/2 - 13:55
 **/
public class BlogException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;
    private int code = 500;

    public BlogException(BlogResponseStatus blogResponseStatus) {
        super(blogResponseStatus.msg());
        this.msg = blogResponseStatus.msg();
        this.code = blogResponseStatus.value();
    }

    public BlogException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public BlogException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

    public BlogException(String msg, int code) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public BlogException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }



}
