package com.ly.blog_common.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ly.blog_common.excel.ExcelAnnotation;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Data
@TableName("article_info")
public class InfoEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 作者
	 */
	@ExcelAnnotation(order = 1,name = "作者")
	private String author;
	/**
	 * 标题
	 */
    @ExcelAnnotation(order = 2,name = "标题")
	private String title;
	/**
	 * 是否展示，0，不展示，1，展示
	 */
    @ExcelAnnotation(order = 3,name = "是否展示")
	private Integer isShow;
	/**
	 * 浏览次数
	 */
    @ExcelAnnotation(order = 4,name = "浏览次数")
	private Long watchTime;
	/**
	 * 标签ID，选择 , 先隔开
	 */
    @ExcelAnnotation(order = 5,name = "标签ID")
	private String labelId;
	/**
	 * 创建人
	 */
    @ExcelAnnotation(order = 21,name = "创建人")
	private String createBy;
	/**
	 * 创建时间
	 */
    @ExcelAnnotation(order = 31,name = "创建时间")
	private Date createTime;
	/**
	 * 更新人
	 */
    @ExcelAnnotation(order = 41,name = "更新人")
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
    @ExcelAnnotation(order = 51,name = "更新时间")
	private Date lastUpdateTime;
	/**
	 * 是否删除  0：已删除  1：正常
	 */
    @ExcelAnnotation(order = 61,name = "是否删除")
	private Integer delFlag;
	/**
	 * 文章展示的URL
	 */
    @ExcelAnnotation(order = 71,name = "文章展示的URL")
	private String showImg;
	/**
	 * 分类Id
	 */
    @ExcelAnnotation(order = 81,name = "分类Id")
	private Long catId;

    /**
     * 经度
     */
    @ExcelAnnotation(order = 82,name = "经度")
	private String lng;
	/**
	 * 纬度
	 */
    @ExcelAnnotation(order = 83,name = "纬度")
	private String lat;
	/**
	 * 地址
	 */
    @ExcelAnnotation(order = 84,name = "地址")
	private String addr;
	/**
	 * 地址
	 */
    @ExcelAnnotation(order = 85,name = "省")
	private String provincial;
	/**
	 * 地址
	 */
    @ExcelAnnotation(order = 86,name = "市")
	private String urban;
	/**
	 * 地址
	 */
    @ExcelAnnotation(order = 87,name = "区")
	private String areas;

}
