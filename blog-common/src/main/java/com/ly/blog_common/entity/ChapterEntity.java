package com.ly.blog_common.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-12 15:45:28
 */
@Data
@TableName("article_chapter")
public class ChapterEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Long id;
	/**
	 * 文章id
	 */
	private Long infoId;
	/**
	 * 章节名
	 */
	private String chapterName;
	/**
	 * 内容
	 */
	private String content;
	/**
	 * text纯文本内容
	 */
	private String contentText;
	/**
	 * 是否展示，0，不展示，1，展示
	 */
	private Integer isShow;
	/**
	 * 浏览次数
	 */
	private Long watchTime;
	/**
	 * 是否删除  0：已删除  1：正常
	 */
	private Integer delFlag;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;
	/**
	 * 顺序
	 */
	private Long orderNumber;
	/**
	 * 来源id，手动为0
	 */
	private Integer sourceId;

}
