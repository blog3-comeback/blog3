package com.ly.blog_common.entity.mq;

import lombok.Data;

/**
 * 消息中间件信息体
 * @author ly create at 2021/6/29 - 14:03
 **/
@Data
public class MqMsgEntity<T> {

    //类型
    private Type type;

    //消息内容
    private T msg;


    public enum Type{
        EMAIL
        ;
        Type(){}
    }
}
