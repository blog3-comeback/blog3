package com.ly.blog_common.entity.mq;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

/**
 * mq的email信息
 * @author ly create at 2021/6/29 - 14:40
 **/
@Data
@NoArgsConstructor
public class EmailMqInfo implements Serializable {

    private static final long serialVersionUID = -3911255650485738676L;

    private Map<String,String> param;

    private String[] email;

    private Integer type;

    public EmailMqInfo(Map<String,String> param,Integer type,String... email){
        this.param = param;
        this.email = email;
        this.type = type;
    }

    public void setEmail(String... email) {
        this.email = email;
    }


    /**
     * 各个枚举对应 模板数据库主键
     */
    public enum Type{
        //注册
        REGISTER(1)
        //登录
        ,LOGIN(2)
        //通知
        ,NOTICE(3)
        ;
        private int typeNumber;

        Type(int typeNumber){
            this.typeNumber = typeNumber;
        }

        public int getTypeNumber() {
            return typeNumber;
        }
    }
}
