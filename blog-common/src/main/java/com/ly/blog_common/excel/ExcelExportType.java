package com.ly.blog_common.excel;

/**
 * 定义excel导出标题行 格式类型
 *
 * @author ly
 * create at 2021/8/30 - 10:38
 **/
public enum ExcelExportType {

    EXPORT_TYPE(1);

    private int exportType;

    ExcelExportType(int exportType){
        this.exportType = exportType;
    }

    public int getExportType() {
        return exportType;
    }
}
