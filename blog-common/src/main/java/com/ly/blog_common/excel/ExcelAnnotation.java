package com.ly.blog_common.excel;

import java.lang.annotation.*;

/**
 * excel对应实体类注解定义，用于导出列名
 *
 * @author ly
 * create at 2021/8/30 - 10:33
 **/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ExcelAnnotation {

    //Excel列序号
    int order();
    //Excel列名 - 数组列名
    String[] name();
    //Excel列宽
    int width() default 5000;

}
