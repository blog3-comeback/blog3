package com.ly.blog_common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.Map;

/**
 * Map和Object转换
 * @author ly create at 2021/6/24 - 21:41
 **/
public class Map2ObjectUtils {

    public static <T> T mapToObject(Map<String, Object> map, Class<T> beanClass) throws Exception {
        if (map == null) {
            return null;
        }
        return JSON.parseObject(JSONObject.toJSONString(map),beanClass);
    }

}
