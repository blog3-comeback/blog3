package com.ly.blog_common.utils;

import com.ly.blog_common.constant.Constant;

import javax.servlet.http.HttpServletRequest;

/**
 * 请求相关公共操作
 * @author ly create at 2021/7/6 - 16:18
 **/
public class RequestUtils {



    /**
     * 获取请求中的token值
     *
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        return request.getHeader(Constant.TOKEN);
    }


}
