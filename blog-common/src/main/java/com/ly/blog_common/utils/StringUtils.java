package com.ly.blog_common.utils;

import java.util.Random;
import java.util.UUID;

/**
 * 业务需要的字符串转换
 * @author ly create at 2021/6/19 - 8:40
 **/
public class StringUtils extends org.apache.commons.lang3.StringUtils {

    //生成uuid
    public static String generateUUID() {
        return UUID.randomUUID().toString().replace("-","");
    }

    //移除Html标记
    public static String removeHtmlMark(String content) {

        if(isBlank(content)){
            return content;
        }
        //正则 ? 的 贪婪模式（最大匹配）和懒惰模式（最小匹配） (((?!img)).) (?!pattern) 零宽负向先行断言 或者 (?<!pattern) 零宽负向后行断言
        content = content.replaceAll("<(((?!img)).)+?>","");

        content = content.replaceAll("(\\s|(&nbsp;))","");

//        content = content.replaceAll("<.+?>","<img src=\"\" alt=\"详情中可展示！\">");

        content = content.replaceAll("<.+?>","");
        return content;

    }


    /**
     * 生成随机的数字 - 字符串型
     * size 位数
     * @return
     */
    public static String randomNumber(int size) {
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0 ;i<size;i++){
            stringBuilder.append(random.nextInt(9));
        }
        return stringBuilder.toString();
    }

    /**
     * 将content标记为红色
     * @param content
     * @param markValue 标记值
     * @param limitLength  限制长度
     * @return
     */
    public static String markKeyRedAndLimit(String content, String markValue, int limitLength) {
        String mark = "<span style='color: red;'>" + markValue + "</span>";
        int position = content.indexOf(markValue);
        int markValueLength = markValue.length();
        if(position != -1 && content.length() > limitLength && limitLength > markValueLength){
            String temp = markValue;
            int contentLegnth  = content.length();
            char[] chars = content.toCharArray();
            for(int beg=position-1,end=position+markValueLength;temp.length() <= limitLength;beg--,end++ ){
                if(beg > -1){
                    temp = chars[beg] + temp;
                }
                if(end < contentLegnth){
                    temp += chars[end];
                }
            }
            return temp.replace(markValue,mark);
        }

        if(content.length() > limitLength){
            content = content.substring(0,limitLength) + "...";
        }
        content = content.replace(markValue,mark);
        return content;
    }



    /**
     * 对应javascript的unescape()函数
     * 方法名 ： escape
     * @param src
     * @return String DOM对象
     */
    public static String escape(String src) {
        int i;
        char j;
        StringBuffer tmp = new StringBuffer();
        tmp.ensureCapacity(src.length() * 6);
        for (i = 0; i < src.length(); i++) {
            j = src.charAt(i);
            if (Character.isDigit(j) || Character.isLowerCase(j)
                || Character.isUpperCase(j))
                tmp.append(j);
            else if (j < 256) {
                tmp.append("%");
                if (j < 16)
                    tmp.append("0");
                tmp.append(Integer.toString(j, 16));
            } else {
                tmp.append("%u");
                tmp.append(Integer.toString(j, 16));
            }
        }
        return tmp.toString();
    }

    /*
     * 对应javascript的unescape()函数
     */
    public static String unescape(String src) {
        StringBuffer tmp = new StringBuffer();
        tmp.ensureCapacity(src.length());
        int lastPos = 0, pos = 0;
        char ch;
        while (lastPos < src.length()) {
            pos = src.indexOf("%", lastPos);
            if (pos == lastPos) {
                if (src.charAt(pos + 1) == 'u') {
                    ch = (char) Integer.parseInt(
                        src.substring(pos + 2, pos + 6), 16);
                    tmp.append(ch);
                    lastPos = pos + 6;
                } else {
                    ch = (char) Integer.parseInt(
                        src.substring(pos + 1, pos + 3), 16);
                    tmp.append(ch);
                    lastPos = pos + 3;
                }
            } else {
                if (pos == -1) {
                    tmp.append(src.substring(lastPos));
                    lastPos = src.length();
                } else {
                    tmp.append(src.substring(lastPos, pos));
                    lastPos = pos;
                }
            }
        }
        return tmp.toString();
    }


}
