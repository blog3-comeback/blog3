

package com.ly.blog_common.utils;

import java.util.HashMap;


/**
 * Map工具类 - 支持一行实现参数添加 put增加返回值
 *
 *
 */
public class MapUtils extends HashMap<String, Object> {

    @Override
    public MapUtils put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
