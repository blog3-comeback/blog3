package com.ly.blog_common.utils;

import com.ly.blog_common.response.BlogResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 定制通用返回格式
 * @author ly create at 2021/6/2 - 10:50
 **/
public class R extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", 0);
        put("msg", "success");
    }

    public static R error() {
        return error(BlogResponseStatus.SYSTEM_UNKNOW);
    }

    public static R error(String msg) {
        return error(BlogResponseStatus.SYSTEM_UNKNOW.value(), msg);
    }

    public static R error(int code,String msg) {
        R r = new R();
        r.put("code", code);
        r.put("msg", msg);
        return r;
    }

    public static R error(BlogResponseStatus blogResponseStatus) {
        R r = new R();
        r.put("code", blogResponseStatus.value());
        r.put("msg", blogResponseStatus.msg());
        return r;
    }

    public static R ok(String msg) {
        R r = new R();
        r.put("msg", msg);
        return r;
    }

    public static R ok(Map<String, Object> map) {
        R r = new R();
        r.putAll(map);
        return r;
    }

    public static R ok() {
        return new R();
    }

    @Override
    public R put(String key, Object value) {
        super.put(key, value);
        return this;
    }

    public int getCode(){
        return Integer.parseInt(String.valueOf(this.get("code")));
    }

    public String getMsg(){
        return String.valueOf(this.get("msg"));
    }
}
