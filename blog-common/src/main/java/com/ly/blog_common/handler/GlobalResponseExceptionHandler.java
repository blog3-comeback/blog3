package com.ly.blog_common.handler;

import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局响应异常处理点 - 但无法处理 403等系统已经处理的异常 + controller处理
 * @author ly create at 2021/6/5 - 9:55
 **/
// 引入时出现不生效问题 ， 自定义的返回对象必须转义json格式，不然会采用默认机制处理
@Slf4j
@RestControllerAdvice
public class GlobalResponseExceptionHandler {

    @ExceptionHandler(BlogException.class)
    public R blogExceptionHandle(BlogException e){
        log.error(e.getMsg());
        return R.error(e.getCode(),e.getMsg());
    }

    @ExceptionHandler(Exception.class)
    public R unknownExceptionHandle(Exception e){
        log.error(e.getMessage());
        return R.error().put("msg",e.getMessage());
    }

}
