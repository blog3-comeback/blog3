package com.ly.blog_common.constant;

/**
 * 用户常量定义
 * @author ly create at 2021/6/2 - 11:23
 **/
public class Constant {

    /** 超级管理员ID */
    public static final int SUPER_ADMIN = 1;

    /** token 关键字 */
    public static final String TOKEN = "token";


    /**
     * token过期时间
     */
    public static final int EXPIRE = 2 * 3600;



    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     *  升序
     */
    public static final String ASC = "asc";
    public static final String DESC = "desc";

    /**
     * 标准时间格式
     */
    public static final Object STANDAR_DATE_FORMART = "yyyy-MM-dd HH:mm:SS";


    /**
     * 菜单类型
     *
     */
    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


    public enum AreaType {

        /**
         * 后端根节点
         */
        BACK(0L),
        /**
         * 前端根节点
         */
        PRE(100L);

        private Long value;

        AreaType(Long value) {
            this.value = value;
        }

        public Long getValue() {
            return value;
        }
    }

    public static final String LOGIN_EMAIL = "email-OK";

}
