package com.ly.blog_common.constant;

/**
 * 数据常量配置
 *
 * @author ly
 * create at 2021/9/19 - 15:47
 **/
public class DataConstant {

    //进度redis存储键
    public static final String TASK_PERCENTAGE = "task.percentage";

}
