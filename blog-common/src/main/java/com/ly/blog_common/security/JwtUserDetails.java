package com.ly.blog_common.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ly.blog_common.entity.SysUserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * 用户处理对象 - 通过后用户信息、权限信息
 * @author ly create at 2021/6/3 - 17:29
 **/
@NoArgsConstructor
@Data
public class JwtUserDetails implements UserDetails {

    private static final long serialVersionUID = 1L;

    private SysUserEntity sysUserEntity;
    private List<GrantedAuthorityImpl> authorities;

    public JwtUserDetails(SysUserEntity sysUserEntity,List<GrantedAuthorityImpl> authorities) {
        this.sysUserEntity = sysUserEntity;
        this.authorities = authorities;
    }

    @Override
    public String getUsername() {
        return sysUserEntity.getUsername();
    }

    public Long getUserId() {
        return sysUserEntity.getUserId();
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return sysUserEntity.getPassword();
    }

    public String getSalt() {
        return sysUserEntity.getSalt();
    }

    public SysUserEntity getUser() {
        return this.sysUserEntity;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }
}
