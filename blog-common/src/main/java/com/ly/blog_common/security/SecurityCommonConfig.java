package com.ly.blog_common.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 权限配置 - 除了user服务之间的全部权限校验点.....同时配置configuration会出现冲突
 * @author ly create at 2021/6/3 - 16:47
 **/

@Configuration
@EnableWebSecurity    // 开启Spring Security
@EnableGlobalMethodSecurity(prePostEnabled = true)	// 开启权限注解，如：@PreAuthorize注解
public class SecurityCommonConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private RedisTemplate redisTemplate;

    public static void setHttpConfigure(HttpSecurity http) throws Exception{
        http.headers().frameOptions().disable(); // 允许嵌套
        // 禁用 csrf, 由于使用的是JWT，我们这里不需要csrf
        http.cors().and().csrf().disable()
                .authorizeRequests()
                // 跨域预检请求
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
                //通用配置
                .antMatchers("/favicon.ico").permitAll()


                //user - 后端  -start
                // web jars
                .antMatchers("/webjars/**").permitAll()
                // 查看SQL监控（druid）
                .antMatchers("/druid/**").permitAll()
                //登录接口
                .antMatchers("/sys/login").permitAll()
                .antMatchers("/sys/login/email").permitAll()
                .antMatchers("/sys/register").permitAll()
                .antMatchers("/sys/send").permitAll()
                //前端资源
                .antMatchers("/sys/menu/show/nav").permitAll()
                .antMatchers("/sys/msg/sys/list").permitAll()
                //用户数据
                .antMatchers("/sys/user/info/show").permitAll()
                .antMatchers("/sys/user/info/show/*").permitAll()
                .antMatchers("/sys/check/username/*").permitAll()
                //配置接口
                .antMatchers("/sys/config/show/list/*").permitAll()
                //user - 后端  -end


                //article - 前端展示URL -start
                .antMatchers(
                        "/article/info/show/info/*"
                        ,"/article/info/search/list"
                        ,"/article/info/show/list"
                        ,"/article/info/list/excel"
                        ,"/article/info/list/map"
                        ,"/article/chapter/show/list"
                        ,"/article/label/show/list"
                        ,"/article/category/list/tree"
                        ,"/article/search/show/list"
                        ,"/article/search/save"
                        ,"/article/issue/save"
                        ,"/article/issue/update"
                        ,"/article/issue/update/zan/*"
                        ,"/article/issue/show/list"
                ).permitAll()
                //article - 前端展示URL - end


                //third - start
                .antMatchers(
                        "/third/file/*/*"
                        ,"/third/mq/send"
                ).permitAll()
                //third - end


                //data


                // 其他所有请求需要身份认证
                .anyRequest().authenticated();
    }

    //基础配置
    //过滤器校验 功能权限
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        setHttpConfigure(http);
        // token验证过滤器
        http.addFilterBefore(
                new JwtAuthenticationFilter(authenticationManager(),redisTemplate),
                UsernamePasswordAuthenticationFilter.class
        );
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //springSecurity直接忽略这些URl  http是对于这些放行，但还是会通过过滤器
        // 服务监控 /actuator/health
        web.ignoring().mvcMatchers("/actuator/**"
                        ,"/captcha.jpg**" //验证码
                        ,"/register"
                        ,"/userLogout"
                        ,"/swagger-ui.html" // swagger
                        ,"/swagger-resources/**"
                        ,"/v2/api-docs"
                        ,"/webjars/springfox-swagger-ui/**"
                );
        super.configure(web);
    }

}
