package com.ly.blog_common.security;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 添加权限过滤器，在用户密码认证过滤器之前
 * @author ly create at 2021/6/3 - 17:04
 **/
public class JwtAuthenticationFilter extends BasicAuthenticationFilter {

    private RedisTemplate redisTemplate;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager,
                                   RedisTemplate redisTemplate) {
        super(authenticationManager);
        this.redisTemplate = redisTemplate;
    }

    /**
     * 过滤器执行点 -- 全都请求都会执行 -- 维护全局认证信息
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        SecurityUtils.checkAndUpdateToken(request,redisTemplate);
        chain.doFilter(request, response);

    }
}
