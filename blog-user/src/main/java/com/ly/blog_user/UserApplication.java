package com.ly.blog_user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author ly create at 2021/6/2 - 9:47
 **/
@EnableFeignClients(basePackages = {"com.ly.blog_user.feign"})
@SpringBootApplication(scanBasePackages = {"com.ly.blog_common.config","com.ly.blog_common.handler","com.ly.blog_user"})
@Slf4j
public class UserApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserApplication.class,args);
        log.info("BLOG-USER START SUCCESS 。。。。。。。。。。。");
    }

}
