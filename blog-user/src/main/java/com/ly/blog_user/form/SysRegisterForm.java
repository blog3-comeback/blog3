package com.ly.blog_user.form;

import lombok.Data;

/**
 * 注册信息
 * @author ly create at 2021/6/28 - 20:43
 **/
@Data
public class SysRegisterForm {

    private String username;

    private String alias;

    private String password;

    private String captcha;

    private String uuid;

    private String email;

    private String emailUuid;

    //邮箱验证码
    private String emailCode;

}
