package com.ly.blog_user.form;

import lombok.Data;

/**
 * @author ly create at 2021/6/29 - 15:08
 **/
@Data
public class EmailForm {

    private String email;

    private String uuid;

    //登录的邮件发送 1还是 注册的邮件发送0
    private Integer type;

}
