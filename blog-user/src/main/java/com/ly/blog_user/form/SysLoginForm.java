

package com.ly.blog_user.form;

import lombok.Data;

/**
 * 登录表单
 *
 *
 */
@Data
public class SysLoginForm {

    /**
     * 用户名或者邮箱地址
     */
    private String username;

    private String password;

    private String captcha;

    private String uuid;

    //支持邮箱验证码登录
    private String emailUuid;
}
