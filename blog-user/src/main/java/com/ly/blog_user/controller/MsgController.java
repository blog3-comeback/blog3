package com.ly.blog_user.controller;

import com.ly.blog_common.entity.MsgEntity;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_user.annotation.SysLog;
import com.ly.blog_user.security.SecurityUtils;
import com.ly.blog_user.service.MsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;



/**
 * 
 *
 * @author ly
 * @date 2021-08-12 10:10:12
 */
@RestController
@RequestMapping("sys/msg")
public class MsgController {
    @Autowired
    private MsgService msgService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@PreAuthorize("hasAuthority('blog_user:msg:list')")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = msgService.queryPageMoreInfo(params);

        return R.ok().put("page", page);
    }

    /**
     * 系统信息
     * @return
     */
    @RequestMapping("/sys/list")
    //@PreAuthorize("hasAuthority('blog_user:msg:list')")
    public R sysReadList(){
        return R.ok().put("data", msgService.querySysNotice());
    }

    /**
     * 列表 - 信息展示 - 展示修改为已读
     */
    @RequestMapping("/no/list")
    //@PreAuthorize("hasAuthority('blog_user:msg:list')")
    public R noReadList(@RequestParam Map<String, Object> params){
        return R.ok().put("page", msgService.queryNoReadAndReaded(params));
    }

    /**
     * 用户信息状态
     * @return
     */
    @RequestMapping("/state")
    //@PreAuthorize("hasAuthority('blog_user:msg:list')")
    public R state(){
        Integer stateNo = msgService.countNoRead();
        Integer stateAll = msgService.countAllRead();
        return R.ok().put("stateNo", stateNo).put("stateAll", stateAll);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@PreAuthorize("hasAuthority('blog_user:msg:info')")
    public R info(@PathVariable("id") Integer id){
		MsgEntity msg = msgService.getById(id);
        return R.ok().put("msg", msg);
    }

    /**
     * 保存
     */
    @SysLog("发送信息-系统、评论")
    @RequestMapping("/save")
    //@PreAuthorize("hasAuthority('blog_user:msg:save')")
    public R save(@RequestBody MsgEntity msg){
        msg.setCreateBy(SecurityUtils.getUserId());
        msg.setLastUpdateBy(SecurityUtils.getUserId());
        msg.setCreateTime(new Date());
        msg.setLastUpdateTime(new Date());
        msg.setFromId(Integer.parseInt(SecurityUtils.getUserId()));
        msgService.save(msg);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@PreAuthorize("hasAuthority('blog_user:msg:update')")
    public R update(@RequestBody MsgEntity msg){
        msg.setCreateTime(new Date());
        msg.setLastUpdateTime(new Date());
		msgService.updateById(msg);
        return R.ok();
    }

    /**
     * 删除
     */
    @SysLog("删除信息-系统、评论")
    @RequestMapping("/delete")
    //@PreAuthorize("hasAuthority('blog_user:msg:delete')")
    public R delete(@RequestBody Integer[] ids){
		msgService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
