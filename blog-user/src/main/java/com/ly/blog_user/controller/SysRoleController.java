

package com.ly.blog_user.controller;

import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_user.annotation.SysLog;
import com.ly.blog_user.base.AbstractController;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_user.entity.SysRoleEntity;
import com.ly.blog_user.service.SysRoleMenuService;
import com.ly.blog_user.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 角色管理
 *
 *
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

	/**
	 * 角色列表
	 */
	@GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:role:list')")
	public R list(@RequestParam Map<String, Object> params){
		//如果不是超级管理员，则只查询自己创建的角色列表
		if(getUserId() != Constant.SUPER_ADMIN){
			params.put("createUserId", getUserId());
		}

		PageUtils page = sysRoleService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 角色列表
	 */
	@GetMapping("/select")
    @PreAuthorize("hasAuthority('sys:role:select')")
	public R select(){
		Map<String, Object> map = new HashMap<>();
		
		//如果不是超级管理员，则只查询自己所拥有的角色列表
		if(getUserId() != Constant.SUPER_ADMIN){
			map.put("create_user_id", getUserId());
		}
		List<SysRoleEntity> list = (List<SysRoleEntity>) sysRoleService.listByMap(map);
		
		return R.ok().put("list", list);
	}
	
	/**
	 * 角色信息  + 追加处理，前端树状展示，只需要叶子节点
	 */
	@GetMapping("/info/{roleId}")
    @PreAuthorize("hasAuthority('sys:role:info')")
	public R info(@PathVariable("roleId") Long roleId){
		SysRoleEntity role = sysRoleService.getById(roleId);
		
		//查询角色对应的菜单
		List<SysMenuEntity> menuIdList = sysRoleMenuService.queryMenuIdList(roleId);
        Set<Long> parentIdList = menuIdList.stream().map(item->item.getParentId()).collect(Collectors.toSet());
		List<Long> ids = menuIdList.stream()
                .filter(item ->!parentIdList.contains(item.getMenuId()))
                .map(item->item.getMenuId()).collect(Collectors.toList());
		role.setMenuIdList(ids);
		
		return R.ok().put("role", role);
	}
	
	/**
	 * 保存角色
	 */
	@SysLog("保存角色")
	@PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:role:save')")
	public R save(@RequestBody SysRoleEntity role){
		role.setCreateUserId(getUserId());
		sysRoleService.saveRole(role);
		
		return R.ok();
	}
	
	/**
	 * 修改角色
	 */
	@SysLog("修改角色")
	@PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:role:update')")
	public R update(@RequestBody SysRoleEntity role){
		
		role.setCreateUserId(getUserId());
		sysRoleService.update(role);
		
		return R.ok();
	}
	
	/**
	 * 删除角色
	 */
	@SysLog("删除角色")
	@PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:role:delete')")
	public R delete(@RequestBody Long[] roleIds){
		sysRoleService.deleteBatch(roleIds);
		
		return R.ok();
	}
}
