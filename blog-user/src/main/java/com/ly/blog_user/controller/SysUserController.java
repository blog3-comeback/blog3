

package com.ly.blog_user.controller;

import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.entity.SysUserEntity;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_user.annotation.SysLog;
import com.ly.blog_user.base.AbstractController;
import com.ly.blog_user.form.PasswordForm;
import com.ly.blog_user.service.SysUserRoleService;
import com.ly.blog_user.service.SysUserService;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 系统用户
 *
 *
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysUserRoleService sysUserRoleService;


	/**
	 * 所有用户列表 ， 获取当前用户创建的用户列表
	 */
	@GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:user:list')")
	public R list(@RequestParam Map<String, Object> params){
		//只有超级管理员，才能查看所有管理员列表
		if(getUserId() != Constant.SUPER_ADMIN){
			params.put("createUserId", getUserId());
		}
		PageUtils page = sysUserService.queryPage(params);

		return R.ok().put("page", page);
	}
	
	/**
	 * 获取登录的用户信息 - 从token中解析 -登录信息本身不准确，用户修改后，无法及时同步用户信息
	 */
	@GetMapping("/info")
	public R info(){
	    //存在用户信息变化，导致用户信息不准确
        SysUserEntity userFromToken = getUser();
        return R.ok().put("user", sysUserService.getById(userFromToken.getUserId()));
	}

	/**
	 * 获取登录的用户权限
	 */
	@GetMapping("/permissions")
	public R permissions(){
		return R.ok().put("permissions", getPermissions());
	}
	
	/**
	 * 修改登录用户密码
	 */
	@SysLog("修改密码")
	@PostMapping("/password")
    @PreAuthorize("hasAuthority('sys:schedule:update')")
	public R password(@RequestBody PasswordForm form){
		//更新密码
		sysUserService.updatePassword(getUserId(), form.getPassword(), form.getNewPassword());
		return R.ok();
	}
	
	/**
	 * 用户信息
	 */
	@GetMapping("/info/{userId}")
	public R info(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.getById(userId);
		
		//获取用户所属的角色列表
		List<Long> roleIdList = sysUserRoleService.queryRoleIdList(userId);
		user.setRoleIdList(roleIdList);
		
		return R.ok().put("user", user);
	}


    /**
     * 用户信息
     */
    @PostMapping("/info/show")
    public SysUserEntity infoShow(@RequestBody Long userId){
        SysUserEntity user = sysUserService.getById(userId);
        return user;
    }

	/**
	 * 展示用户信息
	 */
	@GetMapping("/info/show/{userId}")
	public R infoShowByUserId(@PathVariable("userId") Long userId){
		SysUserEntity user = sysUserService.getShowById(userId);
        return R.ok().put("user", user);
	}
	
	/**
	 * 保存用户
	 */
	@SysLog("保存用户")
	@PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:user:save')")
	public R save(@RequestBody SysUserEntity user){
		user.setCreateUserId(getUserId());
		sysUserService.saveUser(user);
		return R.ok();
	}
	
	/**
	 * 修改用户
	 */
	@SysLog("修改用户")
	@PostMapping("/update")
//    @PreAuthorize("hasAuthority('sys:user:update')")
	public R update(@RequestBody SysUserEntity user){
//        if(1L == user.getUserId()){
//            return R.error("系统管理员不能修改");
//        }
		user.setCreateUserId(getUserId());
		sysUserService.update(user);
		
		return R.ok();
	}
	
	/**
	 * 删除用户
	 */
	@SysLog("删除用户")
	@PostMapping("/delete")
    @PreAuthorize("hasAuthority('sys:user:delete')")
	public R delete(@RequestBody Long[] userIds){
        if(ArrayUtils.contains(userIds, 1L)){
            return R.error("系统管理员不能删除");
        }
		
		if(ArrayUtils.contains(userIds, getUserId())){
			return R.error("当前用户不能删除");
		}
		
		sysUserService.deleteBatch(userIds);
		
		return R.ok();
	}
}
