

package com.ly.blog_user.controller;

import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.utils.R;
import com.ly.blog_user.annotation.SysLog;
import com.ly.blog_user.base.AbstractController;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_user.service.SysMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统菜单
 *
 *
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {
    
	@Autowired
	private SysMenuService sysMenuService;

	/**
	 * 导航菜单
	 */
	@GetMapping("/nav")
	public R nav(){
		List<SysMenuEntity> menuList = sysMenuService.getUserMenuList(getUserId());
		return R.ok().put("menuList", menuList).put("permissions", getPermissions());
	}

	/**
	 * 导航菜单
	 */
	@GetMapping("/show/nav")
	public R showNav(){
		List<SysMenuEntity> menuList = sysMenuService.getUserShowMenuList((long) Constant.SUPER_ADMIN);
		return R.ok().put("menuList", menuList).put("permissions", getPermissions());
	}
	
	/**
	 * 所有菜单列表
	 */
	@GetMapping("/list")
    @PreAuthorize("hasAuthority('sys:menu:list')")
	public R list(){
		List<SysMenuEntity> menuList = sysMenuService.list();
		for(SysMenuEntity sysMenuEntity : menuList){
			SysMenuEntity parentMenuEntity = sysMenuService.getById(sysMenuEntity.getParentId());
			if(parentMenuEntity != null){
				sysMenuEntity.setParentName(parentMenuEntity.getName());
			}
		}

		return R.ok().put("menuList",menuList);
	}
	
	/**
	 * 选择菜单(添加、修改菜单) - 没有按钮的维度全部菜单
	 */
	@GetMapping("/select")
    @PreAuthorize("hasAuthority('sys:menu:select')")
	public R select(){
		//查询列表数据
		List<SysMenuEntity> menuList = sysMenuService.queryNotButtonList();
		
		//添加顶级菜单
		SysMenuEntity root = new SysMenuEntity();
		root.setMenuId(-1L);
		root.setName("根菜单");
		root.setParentId(-2L); //定义最外围
		root.setOpen(true);
		menuList.add(root);
		
		return R.ok().put("menuList", menuList);
	}
	
	/**
	 * 菜单信息
	 */
	@GetMapping("/info/{menuId}")
    @PreAuthorize("hasAuthority('sys:menu:info')")
	public R info(@PathVariable("menuId") Long menuId){
		SysMenuEntity menu = sysMenuService.getById(menuId);
		return R.ok().put("menu", menu);
	}
	
	/**
	 * 保存
	 */
	@SysLog("保存菜单")
	@PostMapping("/save")
    @PreAuthorize("hasAuthority('sys:menu:save')")
	public R save(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
		
		sysMenuService.save(menu);
		
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@SysLog("修改菜单")
	@PostMapping("/update")
    @PreAuthorize("hasAuthority('sys:menu:update')")
	public R update(@RequestBody SysMenuEntity menu){
		//数据校验
		verifyForm(menu);
				
		sysMenuService.updateById(menu);
		
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@SysLog("删除菜单")
	@PostMapping("/delete/{menuId}")
    @PreAuthorize("hasAuthority('sys:menu:delete')")
	public R delete(@PathVariable("menuId") long menuId){
		if(menuId <= 31){
			return R.error("系统菜单，不能删除");
		}

		//判断是否有子菜单或按钮
		List<SysMenuEntity> menuList = sysMenuService.queryListParentId(menuId);
		if(menuList.size() > 0){
			return R.error("请先删除子菜单或按钮");
		}

		sysMenuService.delete(menuId);

		return R.ok();
	}
	
	/**
	 * 验证参数是否正确
	 */
	private void verifyForm(SysMenuEntity menu){
		if(StringUtils.isBlank(menu.getName())){
			throw new BlogException("菜单名称不能为空");
		}
		
		if(menu.getParentId() == null){
			throw new BlogException("上级菜单不能为空");
		}
		
		//菜单
		if(menu.getType() == Constant.MenuType.MENU.getValue()){
			if(StringUtils.isBlank(menu.getUrl())){
				throw new BlogException("菜单URL不能为空");
			}
		}
		
		//上级菜单类型
		int parentType = Constant.MenuType.CATALOG.getValue();
		if(menu.getParentId() != 0){
			SysMenuEntity parentMenu = sysMenuService.getById(menu.getParentId());
			parentType = parentMenu.getType();
		}
		
		//目录、菜单
		if(menu.getType() == Constant.MenuType.CATALOG.getValue() ||
				menu.getType() == Constant.MenuType.MENU.getValue()){
			if(parentType != Constant.MenuType.CATALOG.getValue()){
				throw new BlogException("上级菜单只能为目录类型");
			}
			return ;
		}
		
		//按钮
		if(menu.getType() == Constant.MenuType.BUTTON.getValue()){
			if(parentType != Constant.MenuType.MENU.getValue()){
				throw new BlogException("上级菜单只能为菜单类型");
			}
			return ;
		}
	}
}
