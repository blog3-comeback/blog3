

package com.ly.blog_user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.response.BlogResponseStatus;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import com.ly.blog_user.dao.SysConfigDao;
import com.ly.blog_user.entity.SysConfigEntity;
import com.ly.blog_user.service.SysConfigService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service("sysConfigService")
public class SysConfigServiceImpl extends ServiceImpl<SysConfigDao, SysConfigEntity> implements SysConfigService {


	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String paramKey = (String)params.get("paramKey");

		IPage<SysConfigEntity> page = this.page(
			new Query<SysConfigEntity>().getPage(params),
			new QueryWrapper<SysConfigEntity>()
				.like(StringUtils.isNotBlank(paramKey),"param_key", paramKey)
				.eq("status", 1)
		);

		return new PageUtils(page);
	}

	@Override
	public void saveConfig(SysConfigEntity config) {
		this.save(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void update(SysConfigEntity config) {
		this.updateById(config);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void updateValueByKey(String key, String value) {
		baseMapper.updateValueByKey(key, value);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public void deleteBatch(Long[] ids) {
		for(Long id : ids){
			SysConfigEntity config = this.getById(id);
		}

		this.removeByIds(Arrays.asList(ids));
	}

	@Override
	public String getValue(String key) {
        SysConfigEntity config =  baseMapper.queryByKey(key);
		return config == null ? null : config.getParamValue();
	}
	
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key);
		if(StringUtils.isNotBlank(value)){
			return new Gson().fromJson(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new BlogException("获取参数失败");
		}
	}

    /**
     * 获取展示的系统配置信息
     * @param type 类型 0 全部；1、错误配置（枚举中获取）
     * @return
     */
    @Override
    public List<SysConfigEntity> listShow(Integer type) {
        List<SysConfigEntity> sysConfigEntities = Lists.newArrayList();
        switch (type){
            case 0:break;
            case 1:
                Arrays.asList(BlogResponseStatus.values()).forEach(blogResponseStatus -> {
                    SysConfigEntity sysConfigEntity = new SysConfigEntity();
                    sysConfigEntity.setParamKey(String.valueOf(blogResponseStatus.value()));
                    sysConfigEntity.setParamValue(blogResponseStatus.msg());
                    sysConfigEntities.add(sysConfigEntity);
                });
                break;
            default:break;
        }
        return sysConfigEntities;
    }
}
