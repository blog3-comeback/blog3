

package com.ly.blog_user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.entity.SysUserTokenEntity;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户Token
 *
 *
 */
public interface SysUserTokenService extends IService<SysUserTokenEntity> {

	/**
	 * 生成token
     * @param authentication  当前用户全部信息
     */
	String createToken(Authentication authentication);

	/**
	 * 退出，修改token值
     * @param userId  用户ID
     * @param request
     */
	void logout(long userId, HttpServletRequest request);


    SysUserTokenEntity getSysUserTokenEntityFromToken(String token);
}
