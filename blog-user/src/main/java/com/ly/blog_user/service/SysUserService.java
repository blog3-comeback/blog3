

package com.ly.blog_user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_common.entity.SysUserEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * 系统用户
 *
 *
 */
public interface SysUserService extends IService<SysUserEntity> {

    SysUserEntity getShowById(Long userId);

    PageUtils queryPage(Map<String, Object> params);

	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);

	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	/**
	 * 保存用户
	 */
	void saveUser(SysUserEntity user);
	
	/**
	 * 修改用户
	 */
	void update(SysUserEntity user);
	
	/**
	 * 删除用户
	 */
	void deleteBatch(Long[] userIds);

	/**
	 * 修改密码
	 * @param userId       用户ID
	 * @param password     原密码
	 * @param newPassword  新密码
	 */
	boolean updatePassword(Long userId, String password, String newPassword);

    /**
     * 用户权限
     * @param userId
     * @return
     */
    Set<String> getUserPermissions(long userId);

    /**
     * 查询出当前id的全部菜单集合
     * @param userId 用户id
     * @return 菜单集合
     */
    List<SysMenuEntity> queryAllMenuList(Long userId);

    SysUserEntity queryByEmail(String username);


    int checkEmailUsed(String email);

    int checkUsernameUsed(String username);
}
