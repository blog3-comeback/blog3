package com.ly.blog_user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.entity.MsgEntity;
import com.ly.blog_common.utils.PageUtils;

import java.util.Map;

/**
 * 
 *
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-08-12 10:10:12
 */
public interface MsgService extends IService<MsgEntity> {

    PageUtils queryPageMoreInfo(Map<String, Object> params);

    PageUtils queryPage(Map<String,Object> params);

    Integer countNoRead();

    PageUtils queryNoReadAndReaded(Map<String, Object> params);

    Integer countAllRead();

    MsgEntity querySysNotice();
}

