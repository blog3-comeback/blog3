package com.ly.blog_user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import com.ly.blog_user.dao.MsgDao;
import com.ly.blog_common.entity.MsgEntity;
import com.ly.blog_user.security.SecurityUtils;
import com.ly.blog_user.service.MsgService;
import com.ly.blog_user.service.SysUserService;
import com.ly.blog_user.vo.MsgEntityVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("msgService")
public class MsgServiceImpl extends ServiceImpl<MsgDao, MsgEntity> implements MsgService {

    @Autowired
    private MsgDao msgDao;

    @Autowired
    private SysUserService sysUserService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String)params.get("key");

        IPage<MsgEntity> page = this.page(
                new Query<MsgEntity>().getPage(params),
                new QueryWrapper<MsgEntity>().like(StringUtils.isNotBlank(key),"username", key)
        );

        return new PageUtils(page);
    }

    @Override
    public Integer countNoRead() {
        QueryWrapper queryWrapper = initQueryNoRead();
        queryWrapper.eq("is_read",0);
        Integer state = this.count(queryWrapper);
        return state;
    }

    @Override
    public Integer countAllRead() {
        QueryWrapper queryWrapper = initQueryNoRead();
        Integer state = this.count(queryWrapper);
        return state;
    }

    @Override
    public PageUtils queryNoReadAndReaded(Map<String, Object> params) {
        params.put(Constant.ORDER_FIELD,"create_time");
        params.put(Constant.ORDER,Constant.DESC);

        IPage<MsgEntityVo> page = new Query<MsgEntityVo>().getPage(params);
        params.put("toId",SecurityUtils.getUserId());
        List<MsgEntityVo> msgEntityVos = msgDao.queryPage(params);
        page.setRecords(msgEntityVos);

        //修改信息状态
        page.getRecords().stream()
                .filter(item -> item.getIsRead() == 0)
                .forEach(item ->{
                    MsgEntity temp = new MsgEntity();
                    BeanUtils.copyProperties(item,temp);
                    temp.setIsRead(1);
                    temp.setLastUpdateTime(new Date());
                    temp.setLastUpdateBy(SecurityUtils.getUserId());
                    this.updateById(temp);
        });

        return new PageUtils(page);
    }

    @Override
    public MsgEntity querySysNotice() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("create_time");
        queryWrapper.eq("type",1);
        List<MsgEntity> list = this.list(queryWrapper);
        return list==null?null:list.get(0);
    }

    private QueryWrapper initQueryNoRead(){
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("to_id",SecurityUtils.getUserId());
        return queryWrapper;
    }

    @Override
    public PageUtils queryPageMoreInfo(Map<String, Object> params) {

        params.put(Constant.ORDER_FIELD,"create_time");
        params.put(Constant.ORDER,Constant.DESC);

        IPage<MsgEntityVo> page = new Query<MsgEntityVo>().getPage(params);
        List<MsgEntityVo> msgEntityVos = msgDao.queryPage(params);
        page.setRecords(msgEntityVos);

        return new PageUtils(page);
    }

}