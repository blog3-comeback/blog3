

package com.ly.blog_user.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_user.entity.SysLogEntity;

import java.util.Map;


/**
 * 系统日志
 *
 *
 */
public interface SysLogService extends IService<SysLogEntity> {

    PageUtils queryPage(Map<String, Object> params);

}
