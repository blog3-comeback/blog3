

package com.ly.blog_user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.response.BlogResponseStatus;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.Query;
import com.ly.blog_user.dao.SysMenuDao;
import com.ly.blog_user.dao.SysUserDao;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_common.entity.SysUserEntity;
import com.ly.blog_user.security.MyBCryptPasswordEncoder;
import com.ly.blog_user.service.SysRoleService;
import com.ly.blog_user.service.SysUserRoleService;
import com.ly.blog_user.service.SysUserService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * 系统用户
 *
 *
 */
@Service("sysUserService")
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUserEntity> implements SysUserService {
	@Autowired
	private SysUserRoleService sysUserRoleService;
	@Autowired
	private SysRoleService sysRoleService;
	@Autowired
	private SysMenuDao sysMenuDao;
	@Autowired
	private SysUserDao sysUserDao;

	@Autowired
	private MyBCryptPasswordEncoder myBCryptPasswordEncoder;

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		String username = (String)params.get("username");
		Long createUserId = (Long)params.get("createUserId");

		IPage<SysUserEntity> page = this.page(
			new Query<SysUserEntity>().getPage(params),
			new QueryWrapper<SysUserEntity>()
				.like(StringUtils.isNotBlank(username),"username", username)
				.eq(createUserId != null,"create_user_id", createUserId)
		);

		return new PageUtils(page);
	}

	@Override
	public List<String> queryAllPerms(Long userId) {
		return baseMapper.queryAllPerms(userId);
	}

	@Override
	public List<Long> queryAllMenuId(Long userId) {
		return baseMapper.queryAllMenuId(userId);
	}

    @Override
    public List<SysMenuEntity> queryAllMenuList(Long userId) {
        return baseMapper.queryAllMenuListByUserId(userId);
    }

    @Override
	public SysUserEntity queryByUserName(String username) {
		return baseMapper.queryByUserName(username);
	}
    @Override
	public SysUserEntity queryByEmail(String username) {
		return baseMapper.queryByEmail(username);
	}

    @Override
    public int checkEmailUsed(String email) {
        return baseMapper.checkEmailUsed(email);
    }

    @Override
    public int checkUsernameUsed(String username) {
        return baseMapper.checkUsernameUsed(username);
    }

    @Override
	@Transactional
	public void saveUser(SysUserEntity user) {
		user.setCreateTime(new Date());
		//sha256加密
		String salt = RandomStringUtils.randomAlphanumeric(20);
		user.setPassword(myBCryptPasswordEncoder.encode(user.getPassword(),salt));
		user.setSalt(salt);
		this.save(user);
		
		//检查角色是否越权
//        checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	@Transactional
	public void update(SysUserEntity user) {
		if(StringUtils.isBlank(user.getPassword())){
			user.setPassword(null);
		}else{
            user.setPassword(myBCryptPasswordEncoder.encode(user.getPassword(),user.getSalt()));
		}
		this.updateById(user);
		
		//检查角色是否越权
//		checkRole(user);
		
		//保存用户与角色关系
		sysUserRoleService.saveOrUpdate(user.getUserId(), user.getRoleIdList());
	}

	@Override
	public void deleteBatch(Long[] userId) {
		this.removeByIds(Arrays.asList(userId));
	}

	@Override
	public boolean updatePassword(Long userId, String password, String newPassword) {
        SysUserEntity userEntity = this.getById(userId);
        if(userEntity == null){
            throw new BlogException(BlogResponseStatus.USER_INFO_NO_EXIST);
        }
        if(!myBCryptPasswordEncoder.matches(password,userEntity.getSalt(),userEntity.getPassword())){
            throw new BlogException(BlogResponseStatus.USER_PASSWORD_NO_EXIST);
        }
        userEntity.setPassword(myBCryptPasswordEncoder.encode(newPassword,userEntity.getSalt()));
		return this.update(userEntity,
				new QueryWrapper<SysUserEntity>().eq("user_id", userId));
	}
	
	/**
	 * 检查角色是否越权
     * 当前用户创建的角色才可以使用分配给用户
	 */
	private void checkRole(SysUserEntity user){
		if(user.getRoleIdList() == null || user.getRoleIdList().size() == 0){
			return;
		}
		//如果不是超级管理员，则需要判断用户的角色是否自己创建
		if(user.getCreateUserId() == Constant.SUPER_ADMIN){
			return ;
		}
		
		//查询用户创建的角色列表
		List<Long> roleIdList = sysRoleService.queryRoleIdList(user.getCreateUserId());

		//角色修改前后一致，不做处理


		//判断是否越权
		if(!roleIdList.containsAll(user.getRoleIdList())){
			throw new BlogException("新增用户所选角色，不是本人创建");
		}
	}


    @Override
    public Set<String> getUserPermissions(long userId) {
        List<String> permsList;

        //系统管理员，拥有最高权限
        if(userId == Constant.SUPER_ADMIN){
            List<SysMenuEntity> menuList = sysMenuDao.selectList(null);
            permsList = new ArrayList<>(menuList.size());
            for(SysMenuEntity menu : menuList){
                permsList.add(menu.getPerms());
            }
        }else{
            permsList = sysUserDao.queryAllPerms(userId);
        }
        //用户权限列表
        Set<String> permsSet = new HashSet<>();
        for(String perms : permsList){
            if(StringUtils.isBlank(perms)){
                continue;
            }
            permsSet.addAll(Arrays.asList(perms.trim().split(",")));
        }
        return permsSet;
    }

    /**
     * 展示的用户信息
     * @param userId
     * @return
     */
    @Override
    public SysUserEntity getShowById(Long userId) {
        return sysUserDao.getShowById(userId);
    }
}