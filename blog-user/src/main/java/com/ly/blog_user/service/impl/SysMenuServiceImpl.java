

package com.ly.blog_user.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ly.blog_common.constant.Constant;
import com.ly.blog_common.utils.MapUtils;
import com.ly.blog_user.dao.SysMenuDao;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_user.service.SysMenuService;
import com.ly.blog_user.service.SysRoleMenuService;
import com.ly.blog_user.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenuEntity> implements SysMenuService {
	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private SysRoleMenuService sysRoleMenuService;

    /**
     *
     * @param parentId 父菜单ID
     * @param menuIdList  用户菜单ID
     * @return
     */
	@Override
	public List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList) {
		List<SysMenuEntity> menuList = queryListParentId(parentId); //查询出父id的直系菜单列
		if(menuIdList == null){
			return menuList;
		}
		
		List<SysMenuEntity> userMenuList = new ArrayList<>();
		for(SysMenuEntity menu : menuList){
			if(menuIdList.contains(menu.getMenuId())){
				userMenuList.add(menu);
			}
		}
		return userMenuList;
	}

	@Override
	public List<SysMenuEntity> queryListParentId(Long parentId) {
		return baseMapper.queryListParentId(parentId);
	}


	@Override
	public List<SysMenuEntity> queryNotButtonList() {
		return baseMapper.queryNotButtonList();
	}

    /**
     * 查询后端菜单
     * @param userId
     * @return
     */
    @Override
    public List<SysMenuEntity> getUserMenuList(Long userId) {

        List<SysMenuEntity> sysMenuEntities = getUserShowMenuListByUserId(userId);

        return treeMenuList(Constant.AreaType.BACK.getValue(),
                sysMenuEntities.stream()
                        .filter(sysMenuEntity ->  //保留button ， 前端非展示路由
                                sysMenuEntity != null && sysMenuEntity.getType() != null && sysMenuEntity.getType() != Constant.MenuType.BUTTON.getValue()
                        ).collect(Collectors.toList()));
    }

    /**
     * 查询前端菜单
     * @param userId
     * @return
     */
    @Override
    public List<SysMenuEntity> getUserShowMenuList(Long userId) {
        List<SysMenuEntity> sysMenuEntities = getUserShowMenuListByUserId(userId);

        return treeMenuList(Constant.AreaType.PRE.getValue(),
                sysMenuEntities.stream()
//                        .filter(sysMenuEntity ->  //保留button ， 前端非展示路由
//                                sysMenuEntity.getType() != Constant.MenuType.BUTTON.getValue())
                        .collect(Collectors.toList()));
    }

    //前后公用 - 存在差异
    private List<SysMenuEntity> getUserShowMenuListByUserId(Long userId) {

        //自己实现展示的菜单树状关系
        List<SysMenuEntity> sysMenuEntities;
        if(Constant.SUPER_ADMIN == userId){
            sysMenuEntities = this.list();
        }else{
            sysMenuEntities = sysUserService.queryAllMenuList(userId);
        }
        return sysMenuEntities;
    }



    //递归解决树状结构
    private List<SysMenuEntity> treeMenuList(long parentId, List<SysMenuEntity> sysMenuEntities) {
	    return sysMenuEntities.stream()
                .filter(sysMenuEntity -> sysMenuEntity.getParentId() == parentId)
                .map(sysMenuEntity -> {
                    sysMenuEntity.setList(treeMenuList(sysMenuEntity.getMenuId(),sysMenuEntities));
                    return sysMenuEntity;
                })
                .sorted((sysMenuEntity1,sysMenuEntity2) -> sysMenuEntity1.getOrderNum() - sysMenuEntity2.getOrderNum()) //排序
                .collect(Collectors.toList());
    }

    @Override
	public void delete(Long menuId){
		//删除菜单
		this.removeById(menuId);
		//删除菜单与角色关联
		sysRoleMenuService.removeByMap(new MapUtils().put("menu_id", menuId));
	}

	/**
	 * 获取所有菜单列表
	 */
	private List<SysMenuEntity> getAllMenuList(List<Long> menuIdList){
		//查询根菜单列表
		List<SysMenuEntity> menuList = queryListParentId(0L, menuIdList);
		//递归获取子菜单
        return getMenuTreeList(menuList, menuIdList);
		
	}

	/**
	 * 递归
	 */
	private List<SysMenuEntity> getMenuTreeList(List<SysMenuEntity> menuList, List<Long> menuIdList){
		List<SysMenuEntity> subMenuList = new ArrayList<SysMenuEntity>();
		
		for(SysMenuEntity entity : menuList){
			//目录
			if(entity.getType() == Constant.MenuType.CATALOG.getValue()){
				entity.setList(getMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
			}
			subMenuList.add(entity);
		}
		
		return subMenuList;
	}
}
