package com.ly.blog_user.config;

import com.ly.blog_common.security.SecurityCommonConfig;
import com.ly.blog_user.security.JwtAuthenticationFilter;
import com.ly.blog_user.security.JwtAuthenticationProvider;
import com.ly.blog_user.security.MyBCryptPasswordEncoder;
import com.ly.blog_user.service.SysUserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * 权限配置
 * @author ly create at 2021/6/3 - 16:47
 **/

@Configuration
@EnableWebSecurity    // 开启Spring Security
@EnableGlobalMethodSecurity(prePostEnabled = true)	// 开启权限注解，如：@PreAuthorize注解
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SysUserTokenService sysUserTokenService;

    @Autowired
    private RedisTemplate redisTemplate;

    //认证 - 用户获取 - 认证授权
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 使用自定义身份验证组件
        auth.authenticationProvider(new JwtAuthenticationProvider(userDetailsService));
    }

    //基础配置
    //过滤器校验 功能权限
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        SecurityCommonConfig.setHttpConfigure(http);
        // token验证过滤器
        http.addFilterBefore(
                new JwtAuthenticationFilter(authenticationManager(),sysUserTokenService,redisTemplate),
                UsernamePasswordAuthenticationFilter.class
        );
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //springSecurity直接忽略这些URl  http是对于这些放行，但还是会通过过滤器
        // 服务监控 /actuator/health
        web.ignoring().mvcMatchers("/actuator/**"
                        ,"/captcha.jpg**" //验证码
                        ,"/register"
                        ,"/userLogout"
                        ,"/swagger-ui.html" // swagger
                        ,"/swagger-resources/**"
                        ,"/v2/api-docs"
                        ,"/webjars/springfox-swagger-ui/**"
                );
        super.configure(web);
    }


    /**
     * 密码转义器，单项
     * @return
     */
    @Bean
    public MyBCryptPasswordEncoder myBCryptPasswordEncoder(){
        return new MyBCryptPasswordEncoder();
    }



}
