

package com.ly.blog_user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_user.entity.SysMenuEntity;
import com.ly.blog_common.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 系统用户
 *
 *
 */
@Mapper
public interface SysUserDao extends BaseMapper<SysUserEntity> {

    SysUserEntity getShowById(Long userId);

    /**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	List<Long> queryAllMenuId(Long userId);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	SysUserEntity queryByUserName(String username);

	SysUserEntity queryByEmail(String username);

    List<SysMenuEntity> queryAllMenuListByUserId(Long userId);

    int checkEmailUsed(String email);

    int checkUsernameUsed(String username);
}
