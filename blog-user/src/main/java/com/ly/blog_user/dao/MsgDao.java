package com.ly.blog_user.dao;

import com.ly.blog_common.entity.MsgEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_user.vo.MsgEntityVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 
 * 
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-08-12 10:10:12
 */
@Mapper
public interface MsgDao extends BaseMapper<MsgEntity> {

    List<MsgEntityVo> queryPage(Map<String,Object> params);

}
