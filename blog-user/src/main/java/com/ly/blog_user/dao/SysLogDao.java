

package com.ly.blog_user.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_user.entity.SysLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统日志
 *
 *
 */
@Mapper
public interface SysLogDao extends BaseMapper<SysLogEntity> {
	
}
