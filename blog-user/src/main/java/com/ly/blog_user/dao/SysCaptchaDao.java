

package com.ly.blog_user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_user.entity.SysCaptchaEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 验证码
 *
 *
 */
@Mapper
public interface SysCaptchaDao extends BaseMapper<SysCaptchaEntity> {

}
