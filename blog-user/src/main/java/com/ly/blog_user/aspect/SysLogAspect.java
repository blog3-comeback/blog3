

package com.ly.blog_user.aspect;

import com.alibaba.fastjson.JSONObject;
import com.ly.blog_common.utils.AddrUtils;
import com.ly.blog_common.utils.HttpContextUtils;
import com.ly.blog_common.utils.IPUtils;
import com.ly.blog_user.annotation.SysLog;
import com.ly.blog_user.entity.SysLogEntity;
import com.ly.blog_user.security.SecurityUtils;
import com.ly.blog_user.service.SysLogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;


/**
 * 系统日志，切面处理类
 *
 *
 */
@Slf4j
@Aspect
@Component
public class SysLogAspect {
	@Autowired
	private SysLogService sysLogService;
	
	@Pointcut("@annotation(com.ly.blog_user.annotation.SysLog)")
	public void logPointCut() { 
		
	}

	@Around("logPointCut()")
	public Object around(ProceedingJoinPoint point) throws Throwable {
		long beginTime = System.currentTimeMillis();
        String username = SecurityUtils.getUsername();
		//执行方法
		Object result = point.proceed();
		//执行时长(毫秒)
		long time = System.currentTimeMillis() - beginTime;

		//保存日志
		saveSysLog(point, time ,result,username);

		return result;
	}

	private void saveSysLog(ProceedingJoinPoint joinPoint, long time, Object result, String username) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();

		SysLogEntity sysLog = new SysLogEntity();
        SysLog syslog = method.getAnnotation(SysLog.class);
		if(syslog != null){
			//注解上的描述
			sysLog.setOperation(syslog.value());
		}

		//请求的方法名
		String className = joinPoint.getTarget().getClass().getName();
		String methodName = signature.getName();
		sysLog.setMethod(className + "." + methodName + "()");

		//请求的参数
		Object[] args = joinPoint.getArgs();
		try{
			String params = JSONObject.toJSONString(args[0]);
			sysLog.setParams(params);
		}catch (Exception e){
            log.error("【日志记录】 - 获取参数信息异常：{}",e.getMessage());
		}

		//获取结果
        try {
            String resultStr = JSONObject.toJSONString(result);
            sysLog.setResult(resultStr);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        //获取request
		HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
		//设置IP地址
        String ipAddr = IPUtils.getIpAddr(request);
        sysLog.setIp(ipAddr);
        JSONObject currentPositionFromHeader = AddrUtils.getCurrentPositionFromHeader(request);
        if(null == currentPositionFromHeader){
            sysLog.setAddr(AddrUtils.getAllAddrByIp(ipAddr));
        }else{
            sysLog.setAddr(currentPositionFromHeader.getString(AddrUtils.ADDRESS_DETAIL));
            sysLog.setLng(currentPositionFromHeader.getString(AddrUtils.LNG));
            sysLog.setLat(currentPositionFromHeader.getString(AddrUtils.LAT));
        }

		//用户名
		sysLog.setUsername(username);

		sysLog.setTime(time);
		sysLog.setCreateDate(new Date());
		//保存系统日志
		sysLogService.save(sysLog);
	}
}
