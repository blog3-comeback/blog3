

package com.ly.blog_user.base;

import com.ly.blog_common.entity.SysUserEntity;
import com.ly.blog_common.response.BlogException;
import com.ly.blog_common.security.JwtUserDetails;
import com.ly.blog_user.security.SecurityUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;

import java.util.Set;

/**
 * Controller公共组件
 */
@Slf4j
public abstract class AbstractController {

	protected JwtUserDetails getJwtUserDetails() {
        Authentication authentication = SecurityUtils.getAuthentication();
        if(authentication != null){
            JwtUserDetails jwtUserDetails = (JwtUserDetails) authentication.getPrincipal();
            return jwtUserDetails;
        }
        throw new BlogException("用户没有登录");
	}

	protected SysUserEntity getUser() {
        return getJwtUserDetails().getUser();
	}

	protected Long getUserId() {
		return getUser().getUserId();
	}

	protected static Set<String> getPermissions() {
        return SecurityUtils.getPermissions();
	}
}
