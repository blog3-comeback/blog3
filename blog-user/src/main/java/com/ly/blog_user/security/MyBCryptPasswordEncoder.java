package com.ly.blog_user.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 在原来的密码转义器上添加盐值拼接，简化操作
 * @author ly create at 2021/6/4 - 10:24
 **/
public class MyBCryptPasswordEncoder extends BCryptPasswordEncoder {


    public String encode(CharSequence rawPassword , CharSequence salt) {
        StringBuffer s = new StringBuffer(rawPassword);
        s.append("_");
        s.append(salt);
        return super.encode(s.toString());
    }

    public boolean matches(CharSequence rawPassword, CharSequence salt, String encodedPassword) {
        StringBuffer s = new StringBuffer(rawPassword);
        s.append("_");
        s.append(salt);
        return super.matches(s.toString(), encodedPassword);
    }
}
