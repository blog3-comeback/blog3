package com.ly.blog_user.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 输出消息展示
 * 
 * @author ly
 * @email yunglee995@gmail.com
 * @date 2021-08-12 10:10:12
 */
@Data
@TableName("sys_msg")
public class MsgEntityVo implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 消息类型，1：系统、 2：用户
	 */
	private Integer type;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 是否已读，0 ，未读 1，已读
	 */
	private Integer isRead;
	/**
	 * 来源谁，-1 ： 系统 、其余：用户
	 */
	private Integer fromId;
	/**
	 * 发给谁，用户id
	 */
	private Integer toId;
	/**
	 * 创建人
	 */
	private String createBy;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新人
	 */
	private String lastUpdateBy;
	/**
	 * 更新时间
	 */
	private Date lastUpdateTime;
	/**
	 * 是否展示
	 */
	private Integer isShow;

    /**
     * 新增展示
     */
    private String toUsername;
    private String toAlias;
    private String fromUsername;
    private String fromAlias;
}
