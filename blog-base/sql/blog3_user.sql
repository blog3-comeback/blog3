/*
 Navicat Premium Data Transfer

 Source Server         : 169.254.75.100（3307-prod）
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 169.254.75.100:3307
 Source Schema         : blog3_user

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 28/09/2021 09:14:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for email_record
-- ----------------------------
DROP TABLE IF EXISTS `email_record`;
CREATE TABLE `email_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `to_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送的邮箱，多个逗号分隔',
  `is_success` int(2) NULL DEFAULT NULL COMMENT '是否成功 0成功 1失败',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '发送的内容',
  `type` int(2) NULL DEFAULT NULL COMMENT '邮件类型 0，注册 1，登录...',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha`  (
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'uuid',
  `code` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '验证码',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统验证码' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('481822a7-c2f8-433c-8fb7-4ea79c73b4b0', '4753n', '2021-09-19 17:34:16');
INSERT INTO `sys_captcha` VALUES ('4d0b17ff-53bc-45b6-81ff-fb1fd726ceca', 'pyg6n', '2021-08-24 17:45:07');
INSERT INTO `sys_captcha` VALUES ('5683a6d7-b7b8-406d-846c-e6598f5da113', 'pewbn', '2021-09-11 19:22:26');
INSERT INTO `sys_captcha` VALUES ('a5795dee-4aba-4799-8357-5489e355d94b', 'dy3g4', '2021-08-03 14:17:23');
INSERT INTO `sys_captcha` VALUES ('baa95237-4c88-4753-8f1f-ba418a3617f6', 'wyxan', '2021-09-14 18:28:26');
INSERT INTO `sys_captcha` VALUES ('c44da376-9305-4888-86fb-e97e73729637', 'pe452', '2021-09-14 17:32:40');
INSERT INTO `sys_captcha` VALUES ('e7c4cc8f-e79f-449b-8863-3a297447792b', '7m254', '2021-07-27 10:20:32');
INSERT INTO `sys_captcha` VALUES ('f4ba8782-208d-44a1-8d00-6be87808c704', 'ww2ng', '2021-07-27 10:20:33');
INSERT INTO `sys_captcha` VALUES ('feb2c74d-9b49-4431-8619-004a04a6f994', 'fampc', '2021-08-02 16:57:16');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) NULL DEFAULT 1 COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `param_key`(`param_key`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, 'CLOUD_STORAGE_CONFIG_KEY', '{\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"aliyunDomain\":\"\",\"aliyunEndPoint\":\"\",\"aliyunPrefix\":\"\",\"qcloudBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuBucketName\":\"ios-app\",\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"type\":1}', 1, '云存储配置信息');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `result` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '执行结果',
  `addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '执行地点',
  `lng` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '经度',
  `lat` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (11, NULL, '修改用户', 'com.ly.blog_user.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"email\":\"1419975745@qq.com\",\"mobile\":\"17326059019\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1}]', 22, '0:0:0:0:0:0:0:1', '2021-07-01 11:42:51', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (12, NULL, '修改角色', 'com.ly.blog_user.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"文章编写人员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[-9]}]', 6, '0:0:0:0:0:0:0:1', '2021-07-01 11:46:21', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (13, NULL, '修改角色', 'com.ly.blog_user.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"文章编写人员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[39,40,41,-9,0,37]}]', 9, '0:0:0:0:0:0:0:1', '2021-07-01 11:50:16', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (14, 'admin', '修改角色', 'com.ly.blog_user.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"文章编写人员\",\"remark\":\"用户注册默认角色，支持用户抓取网络数据和展示自己的文章，和抓取的文章。\",\"createUserId\":1,\"menuIdList\":[39,125,126,127,128,40,121,122,123,124,41,117,118,119,120,-9,0,37]}]', 42, '169.254.75.1', '2021-08-03 14:32:57', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (15, 'admin', '修改用户', 'com.ly.blog_user.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"email\":\"1419975745@qq.com\",\"img\":\"https://blog-yunglee.oss-cn-chengdu.aliyuncs.com/2021-08-24/5fbc0ba9-959e-4038-86a6-884a191e4fd5_logo-collapse.png\",\"remark\":\"yunglee博客建站初始站主，希望通过这个平台可以认识更多的朋友\",\"addr\":\"重庆市\",\"school\":\"重庆工商大学\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"alias\":\"站主\"}]', 64, '169.254.75.1', '2021-08-24 18:07:43', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (16, 'admin', '修改用户', 'com.ly.blog_user.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"email\":\"1419975745@qq.com\",\"img\":\"https://blog-yunglee.oss-cn-chengdu.aliyuncs.com/2021-08-24/267a2b8f-feb1-4b61-84f7-252b397361f2_user.png\",\"remark\":\"yunglee博客建站初始站主，希望通过这个平台可以认识更多的朋友\",\"addr\":\"重庆市\",\"school\":\"重庆工商大学\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"alias\":\"站主\"}]', 11, '169.254.75.1', '2021-08-24 18:08:43', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (17, 'admin', '修改用户', 'com.ly.blog_user.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"email\":\"1419975745@qq.com\",\"img\":\"https://blog-yunglee.oss-cn-chengdu.aliyuncs.com/2021-08-24/e5bca916-2a23-4d2f-8621-6ce40fef04be_logo.png\",\"remark\":\"yunglee博客建站初始站主，希望通过这个平台可以认识更多的朋友\",\"addr\":\"重庆市\",\"school\":\"重庆工商大学\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"alias\":\"站主\"}]', 12, '169.254.75.1', '2021-08-24 18:08:55', NULL, NULL, NULL, NULL);
INSERT INTO `sys_log` VALUES (18, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"pnnnc\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"b8214918-4315-4ee9-87ee-2bec39f268dc\"}', 479, '169.254.75.1', '2021-09-04 18:34:40', '{\"msg\":\"success\",\"code\":0,\"token\":\"7eac06785002ae0b3740b525666d845f\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (19, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"3eyc5\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"6b8dcc57-80f8-49fe-8625-ecae92958fe9\"}', 272, '169.254.75.1', '2021-09-11 18:00:53', '{\"msg\":\"success\",\"code\":0,\"token\":\"82cd88865526f63d90b1cf871946e445\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (20, 'admin', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 25, '169.254.75.1', '2021-09-11 19:17:26', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (21, NULL, '注册用户', 'com.ly.blog_user.controller.SysLoginController.register()', '{\"alias\":\"\",\"captcha\":\"\",\"email\":\"yunglee995@gmail.com\",\"emailCode\":\"401674\",\"emailUuid\":\"81c192c3-ed56-47d6-8873-2331e1154bea\",\"password\":\"yunglee995\",\"username\":\"yunglee995\"}', 130, '169.254.75.1', '2021-09-11 19:18:55', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (22, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"6gmdn\",\"password\":\"yunglee995\",\"username\":\"yunglee995\",\"uuid\":\"d9c6d64e-64ab-4b1e-8cbd-f10f6d6e8ba1\"}', 267, '169.254.75.1', '2021-09-11 19:19:04', '{\"msg\":\"success\",\"code\":0,\"token\":\"0fecd839028f2926db4cd9fb91944985\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (23, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"yx62n\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"a7871f7d-adf7-4e78-8a1b-7b3d3e7bb3d6\"}', 101, '169.254.75.1', '2021-09-11 19:21:16', '{\"msg\":\"success\",\"code\":0,\"token\":\"5f0a0745b00c7e7bfcd115997e2f181f\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (24, 'admin', '保存角色', 'com.ly.blog_user.controller.SysRoleController.save()', '{\"createTime\":1631359424481,\"createUserId\":1,\"menuIdList\":[15,39,125,126,127,128,40,121,122,123,124,41,117,118,119,120,104,105,106,-9,0,1,2,37],\"remark\":\"\",\"roleId\":2,\"roleName\":\"文章+用户角色\"}', 45, '169.254.75.1', '2021-09-11 19:23:45', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (25, 'yunglee995', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 3, '169.254.75.1', '2021-09-11 19:25:36', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (26, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"825yp\",\"password\":\"yunglee995\",\"username\":\"yunglee995\",\"uuid\":\"aa195825-5b1b-4c62-86a5-21f1e8aa5425\"}', 228, '169.254.75.1', '2021-09-11 19:32:27', '{\"msg\":\"success\",\"code\":0,\"token\":\"649f6bce8c158cb63cfef463ac3c5e3b\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (27, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"2w53p\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"7fc3fa7a-7ee2-42c9-85ad-9fdf11ebcbce\"}', 302, '169.254.75.1', '2021-09-14 17:28:05', '{\"msg\":\"success\",\"code\":0,\"token\":\"c64942b666134383f164c51859a5184b\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (28, 'admin', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 23, '169.254.75.1', '2021-09-14 17:43:19', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (29, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"ayy4a\",\"password\":\"yunglee995\",\"username\":\"yunglee995\",\"uuid\":\"43dbd23d-eec3-4530-8b54-c4a60e59006a\"}', 240, '169.254.75.1', '2021-09-14 17:43:38', '{\"msg\":\"success\",\"code\":0,\"token\":\"34edc46b45e79c97b500a8399158fd97\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (30, 'yunglee995', '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"daad2\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"7fa8e1b4-dcbc-452d-804e-ad48acaa0fd6\"}', 8, '169.254.75.1', '2021-09-14 18:19:07', '{\"msg\":\"验证码不正确\",\"code\":1805}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (31, 'yunglee995', '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"n2n8f\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"68c77cdc-c6cb-4905-83ec-c0556da13988\"}', 275, '169.254.75.1', '2021-09-14 18:19:47', '{\"msg\":\"success\",\"code\":0,\"token\":\"c40db845388d33e2655ef2fe22ac0be4\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (32, 'admin', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 12, '169.254.75.1', '2021-09-14 18:22:25', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (33, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"6nbgy\",\"password\":\"guest\",\"username\":\"guest\",\"uuid\":\"8ff4bcef-b9ca-408b-8d62-91e499fe9531\"}', 103, '169.254.75.1', '2021-09-14 18:22:43', '{\"msg\":\"success\",\"code\":0,\"token\":\"356f0723e2744d9bbf1b302296ba7684\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (34, 'guest', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 8, '169.254.75.1', '2021-09-14 18:23:26', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (35, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"w376m\",\"password\":\"guest\",\"username\":\"guest\",\"uuid\":\"1b019d56-35eb-4a38-8f2a-5675f030591b\"}', 88, '169.254.75.1', '2021-09-14 18:25:15', '{\"msg\":\"success\",\"code\":0,\"token\":\"fa2c68b782fb98f5cdb766098c264725\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (36, 'guest', '发送信息-系统、评论', 'com.ly.blog_user.controller.MsgController.save()', '{\"content\":\"断舍离...，断：进入的选择；舍：出去的剥离；离：立于其间，静心待命。\",\"createBy\":\"5\",\"createTime\":1631615352036,\"fromId\":5,\"id\":2,\"lastUpdateBy\":\"5\",\"lastUpdateTime\":1631615352036,\"toId\":1,\"type\":2}', 14, '172.17.0.9', '2021-09-14 18:29:12', '{\"msg\":\"success\",\"code\":0}', '局域网_对方和您在同一内部网', NULL, NULL);
INSERT INTO `sys_log` VALUES (37, 'guest', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 47, '169.254.75.1', '2021-09-14 18:43:31', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (38, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"bfxa6\",\"password\":\"yunglee995\",\"username\":\"yunglee995\",\"uuid\":\"512b3883-5576-4656-8a40-02a626e302b7\"}', 226, '169.254.75.1', '2021-09-14 18:43:46', '{\"msg\":\"success\",\"code\":0,\"token\":\"845f0a1375ef37de7d4d0d563eebe891\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (39, 'yunglee995', '发送信息-系统、评论', 'com.ly.blog_user.controller.MsgController.save()', '{\"content\":\"旁边好像是在看直播，闹腾着要做些什么事情...\",\"createBy\":\"6\",\"createTime\":1631616620474,\"fromId\":6,\"id\":3,\"lastUpdateBy\":\"6\",\"lastUpdateTime\":1631616620474,\"toId\":6,\"type\":2}', 10, '172.17.0.9', '2021-09-14 18:50:21', '{\"msg\":\"success\",\"code\":0}', '局域网_对方和您在同一内部网', NULL, NULL);
INSERT INTO `sys_log` VALUES (40, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"npmwb\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"f6b94bf6-d037-425c-8eea-e99d3becc0dc\"}', 325, '169.254.75.1', '2021-09-15 17:38:37', '{\"msg\":\"success\",\"code\":0,\"token\":\"6eb77c5368d9c879565b08f654858c1c\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (41, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"cc4p2\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"ae01b016-50b2-4fec-8a70-052905ca0168\"}', 387, '169.254.75.1', '2021-09-19 17:25:53', '{\"msg\":\"success\",\"code\":0,\"token\":\"3c469c85f261414427c0d3f07f6b507c\"}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (42, 'admin', '登出用户', 'com.ly.blog_user.controller.SysLoginController.logout()', NULL, 15, '169.254.75.1', '2021-09-19 17:29:16', '{\"msg\":\"success\",\"code\":0}', 'IANA_保留地址用于本地自动配置', NULL, NULL);
INSERT INTO `sys_log` VALUES (43, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"8md83\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"69c328e5-a2d1-49b3-8027-6e854b147f52\"}', 298, '169.254.75.1', '2021-09-26 17:22:29', '{\"msg\":\"success\",\"code\":0,\"token\":\"e559219cf4f2e76e67521f806ded7933\"}', '山东省-烟台市-福山区-古现街道-长江路-302号（山东省烟台市福山区古现街道人才公寓业达科技园）', '121.18112', '37.56352');
INSERT INTO `sys_log` VALUES (44, NULL, '用户名登录', 'com.ly.blog_user.controller.SysLoginController.login()', '{\"captcha\":\"ng4pf\",\"password\":\"admin\",\"username\":\"admin\",\"uuid\":\"1a33d1d7-888c-40ff-8d62-e9fb22cf5d33\"}', 249, '169.254.75.1', '2021-09-27 09:08:08', '{\"msg\":\"success\",\"code\":0,\"token\":\"99a47df52a08bfc924c568c39da9a50e\"}', '山东省-烟台市-福山区-古现街道-长江路-302号（山东省烟台市福山区古现街道人才公寓业达科技园）', '121.18112', '37.56352');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) NULL DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单管理' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (0, -1, '后端菜单', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', NULL, NULL, 0, 'cogs', 0);
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', 'sys/user', NULL, 1, 'users', 1);
INSERT INTO `sys_menu` VALUES (3, 1, '角色管理', 'sys/role', NULL, 1, 'tags', 2);
INSERT INTO `sys_menu` VALUES (4, 1, '菜单管理', 'sys/menu', NULL, 1, 'th-list', 3);
INSERT INTO `sys_menu` VALUES (5, 1, 'SQL监控', 'http://localhost:8080/renren-fast/druid/sql.html', NULL, 1, 'database', 4);
INSERT INTO `sys_menu` VALUES (6, 1, '定时任务', 'job/schedule', NULL, 1, 'clock-o', 5);
INSERT INTO `sys_menu` VALUES (7, 6, '查看', NULL, 'sys:schedule:list,sys:schedule:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (8, 6, '新增', NULL, 'sys:schedule:save', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (9, 6, '修改', NULL, 'sys:schedule:update', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (10, 6, '删除', NULL, 'sys:schedule:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (11, 6, '暂停', NULL, 'sys:schedule:pause', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (12, 6, '恢复', NULL, 'sys:schedule:resume', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (13, 6, '立即执行', NULL, 'sys:schedule:run', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (14, 6, '日志列表', NULL, 'sys:schedule:log', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (15, 2, '查看', NULL, 'sys:user:list,sys:user:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (16, 2, '新增', NULL, 'sys:user:save,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (17, 2, '修改', NULL, 'sys:user:update,sys:role:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (18, 2, '删除', NULL, 'sys:user:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (19, 3, '查看', NULL, 'sys:role:list,sys:role:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (20, 3, '新增', NULL, 'sys:role:save,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (21, 3, '修改', NULL, 'sys:role:update,sys:menu:list', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (22, 3, '删除', NULL, 'sys:role:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (23, 4, '查看', NULL, 'sys:menu:list,sys:menu:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (24, 4, '新增', NULL, 'sys:menu:save,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (25, 4, '修改', NULL, 'sys:menu:update,sys:menu:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (26, 4, '删除', NULL, 'sys:menu:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (27, 1, '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', 1, 'language', 6);
INSERT INTO `sys_menu` VALUES (29, 1, '系统日志', 'sys/log', 'sys:log:list', 1, 'files-o', 7);
INSERT INTO `sys_menu` VALUES (30, 1, '文件上传', 'oss/oss', 'sys:oss:all', 1, 'folder', 6);
INSERT INTO `sys_menu` VALUES (37, 0, '文章管理', '', '', 0, 'book', 0);
INSERT INTO `sys_menu` VALUES (38, 37, '分类管理', 'article/category', '', 1, 'fighter-jet', 0);
INSERT INTO `sys_menu` VALUES (39, 37, '标签管理', 'article/label', '', 1, 'eyedropper', 0);
INSERT INTO `sys_menu` VALUES (40, 37, '信息维护', 'article/info', '', 1, 'file', 0);
INSERT INTO `sys_menu` VALUES (41, 37, '内容维护', 'article/chapter', '', 1, 'etsy', 0);
INSERT INTO `sys_menu` VALUES (100, -1, '前端菜单', NULL, NULL, 0, NULL, NULL);
INSERT INTO `sys_menu` VALUES (101, 100, '文章', '', '', 0, 'book', 0);
INSERT INTO `sys_menu` VALUES (102, 101, '全部文章', 'show/article/list', 'show:article:list', 1, 'book', 0);
INSERT INTO `sys_menu` VALUES (103, 101, '文章详情', 'show/article/detail', 'show:article:detail', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (104, 0, '数据获取', '', '', 0, 'eyedropper ', 3);
INSERT INTO `sys_menu` VALUES (105, 104, '任务配置', 'data/datatask', '', 1, 'html5', 0);
INSERT INTO `sys_menu` VALUES (106, 104, '任务结果', 'data/dataresult', '', 1, 'html5', 0);
INSERT INTO `sys_menu` VALUES (107, 0, '理财管理', '', '', 0, 'money', 0);
INSERT INTO `sys_menu` VALUES (108, 107, '股票任务', 'financial/financialtask', '', 1, 'money', 0);
INSERT INTO `sys_menu` VALUES (109, 100, '财经', '', '', 0, 'money', 0);
INSERT INTO `sys_menu` VALUES (110, 109, '财经实时', 'show/financial/live', '', 2, 'money', 0);
INSERT INTO `sys_menu` VALUES (111, 38, '查看', NULL, 'article:category:list,article:category:info,article:category:list:tree', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (112, 38, '新增', NULL, 'article:category:save,article:category:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (113, 38, '修改', NULL, 'article:category:update,article:category:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (114, 38, '删除', NULL, 'article:category:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (117, 41, '查看', NULL, 'article:chapter:list,article:chapter:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (118, 41, '新增', NULL, 'article:chapter:save,article:chapter:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (119, 41, '修改', NULL, 'article:chapter:update,article:chapter:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (120, 41, '删除', NULL, 'article:chapter:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (121, 40, '查看', NULL, 'article:info:list,article:info:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (122, 40, '新增', NULL, 'article:info:save,article:info:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (123, 40, '修改', NULL, 'article:info:update,article:info:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (124, 40, '删除', NULL, 'article:info:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (125, 39, '查看', NULL, 'article:label:list,article:label:info', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (126, 39, '新增', NULL, 'article:label:save,article:label:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (127, 39, '修改', NULL, 'article:label:update,article:label:select', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (128, 39, '删除', NULL, 'article:label:delete', 2, NULL, 0);
INSERT INTO `sys_menu` VALUES (129, 37, '评论留言', 'article/issue', 'issue:list', 1, 'coffee', 0);
INSERT INTO `sys_menu` VALUES (130, 101, '留言板', 'show/article/word', '', 1, 'coffee', 0);
INSERT INTO `sys_menu` VALUES (131, 1, '信息管理', 'sys/msg', '', 1, 'comments ', 9);
INSERT INTO `sys_menu` VALUES (132, 0, '邮箱管理', '', '', 0, 'envelope', 4);
INSERT INTO `sys_menu` VALUES (133, 132, '邮箱模板', 'email/emailtemplate', '', 1, 'file-text', 0);
INSERT INTO `sys_menu` VALUES (134, 101, '五湖四海', 'show/article/map', '', 1, 'street-view', 0);

-- ----------------------------
-- Table structure for sys_msg
-- ----------------------------
DROP TABLE IF EXISTS `sys_msg`;
CREATE TABLE `sys_msg`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(11) NULL DEFAULT NULL COMMENT '消息类型，1：系统、 2：用户-评论 3、用户-留言 ',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '消息内容',
  `is_read` int(11) NULL DEFAULT 0 COMMENT '是否已读，0 ，未读 1，已读',
  `from_id` int(11) NULL DEFAULT NULL COMMENT '来源谁，-1 ： 系统 、其余：用户',
  `to_id` int(11) NULL DEFAULT NULL COMMENT '发给谁，用户id,逗号分隔',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `is_show` int(11) NULL DEFAULT 0 COMMENT '是否展示， 0展示 1不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_msg
-- ----------------------------
INSERT INTO `sys_msg` VALUES (1, 1, '2021-09-11 致全体用户，预祝 中秋快乐~~~~~', 0, 1, 12, '1', '2021-09-11 13:52:43', '1', '2021-09-11 13:52:43', 0);
INSERT INTO `sys_msg` VALUES (2, 2, '断舍离...，断：进入的选择；舍：出去的剥离；离：立于其间，静心待命。', 1, 5, 1, '5', '2021-09-14 18:29:12', '1', '2021-09-15 17:51:34', 0);
INSERT INTO `sys_msg` VALUES (3, 2, '旁边好像是在看直播，闹腾着要做些什么事情...', 0, 6, 6, '6', '2021-09-14 18:50:20', '6', '2021-09-14 18:50:20', 0);

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '文件上传' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '文章编写人员', '用户注册默认角色，支持用户抓取网络数据和展示自己的文章，和抓取的文章。', 1, '2021-06-13 09:51:50');
INSERT INTO `sys_role` VALUES (2, '文章+用户角色', '', 1, '2021-09-11 19:23:44');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NULL DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色与菜单对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (12, 1, 39);
INSERT INTO `sys_role_menu` VALUES (13, 1, 125);
INSERT INTO `sys_role_menu` VALUES (14, 1, 126);
INSERT INTO `sys_role_menu` VALUES (15, 1, 127);
INSERT INTO `sys_role_menu` VALUES (16, 1, 128);
INSERT INTO `sys_role_menu` VALUES (17, 1, 40);
INSERT INTO `sys_role_menu` VALUES (18, 1, 121);
INSERT INTO `sys_role_menu` VALUES (19, 1, 122);
INSERT INTO `sys_role_menu` VALUES (20, 1, 123);
INSERT INTO `sys_role_menu` VALUES (21, 1, 124);
INSERT INTO `sys_role_menu` VALUES (22, 1, 41);
INSERT INTO `sys_role_menu` VALUES (23, 1, 117);
INSERT INTO `sys_role_menu` VALUES (24, 1, 118);
INSERT INTO `sys_role_menu` VALUES (25, 1, 119);
INSERT INTO `sys_role_menu` VALUES (26, 1, 120);
INSERT INTO `sys_role_menu` VALUES (27, 1, 0);
INSERT INTO `sys_role_menu` VALUES (28, 1, 37);
INSERT INTO `sys_role_menu` VALUES (29, 2, 15);
INSERT INTO `sys_role_menu` VALUES (30, 2, 39);
INSERT INTO `sys_role_menu` VALUES (31, 2, 125);
INSERT INTO `sys_role_menu` VALUES (32, 2, 126);
INSERT INTO `sys_role_menu` VALUES (33, 2, 127);
INSERT INTO `sys_role_menu` VALUES (34, 2, 128);
INSERT INTO `sys_role_menu` VALUES (35, 2, 40);
INSERT INTO `sys_role_menu` VALUES (36, 2, 121);
INSERT INTO `sys_role_menu` VALUES (37, 2, 122);
INSERT INTO `sys_role_menu` VALUES (38, 2, 123);
INSERT INTO `sys_role_menu` VALUES (39, 2, 124);
INSERT INTO `sys_role_menu` VALUES (40, 2, 41);
INSERT INTO `sys_role_menu` VALUES (41, 2, 117);
INSERT INTO `sys_role_menu` VALUES (42, 2, 118);
INSERT INTO `sys_role_menu` VALUES (43, 2, 119);
INSERT INTO `sys_role_menu` VALUES (44, 2, 120);
INSERT INTO `sys_role_menu` VALUES (45, 2, 104);
INSERT INTO `sys_role_menu` VALUES (46, 2, 105);
INSERT INTO `sys_role_menu` VALUES (47, 2, 106);
INSERT INTO `sys_role_menu` VALUES (48, 2, 0);
INSERT INTO `sys_role_menu` VALUES (49, 2, 1);
INSERT INTO `sys_role_menu` VALUES (50, 2, 2);
INSERT INTO `sys_role_menu` VALUES (51, 2, 37);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) NULL DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `alias` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '昵称、别名',
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '简介',
  `addr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `school` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学校',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$jEOxf4Flf3J/QyL3oN5O2OpuQTvPA6kkQaWsdHlyVx5hY6LZ3BW26', 'lJwNG8tPohVvmxvOhcvE', '1419975745@qq.com', '17326059019', 1, 1, '2021-06-04 11:11:28', '站主', 'https://blog-yunglee.oss-cn-chengdu.aliyuncs.com/2021-08-24/e5bca916-2a23-4d2f-8621-6ce40fef04be_logo.png', 'yunglee博客建站初始站主，希望通过这个平台可以认识更多的朋友', '重庆市', '重庆工商大学');
INSERT INTO `sys_user` VALUES (5, 'guest', '$2a$10$uNAio0XaYs2ikfsY6KijRuKdfv.ZyuvlPksHkKY/UXsy1IvUx9uNK', 'BSqXIDoFr9xTNbA5IFCZ', 'yunglee95@163.com', NULL, 1, NULL, '2021-07-01 13:59:08', '访客', NULL, NULL, NULL, NULL);
INSERT INTO `sys_user` VALUES (6, 'yunglee995', '$2a$10$FRndjiCSvG.jTfczw0VgQODP5NpY5XXj1NrkrDxqhWVQ/UBF2HodK', 'CNLzTEblyR2a8cFXiZj2', 'yunglee995@gmail.com', NULL, 1, 1, '2021-09-11 19:18:55', 'yunglee995', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) NULL DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与角色对应关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (3, 5, 1);
INSERT INTO `sys_user_role` VALUES (6, 1, 1);
INSERT INTO `sys_user_role` VALUES (8, 6, 2);

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'token',
  `user_detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户认证信息',
  `expire_time` datetime(0) NULL DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `token`(`token`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户Token' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES (1, '99a47df52a08bfc924c568c39da9a50e', '{\"accountNonExpired\":true,\"accountNonLocked\":true,\"authorities\":[{\"authority\":\"sys:config:save\"},{\"authority\":\"sys:config:update\"},{\"authority\":\"article:info:update\"},{\"authority\":\"article:category:update\"},{\"authority\":\"show:article:list\"},{\"authority\":\"article:label:update\"},{\"authority\":\"article:label:delete\"},{\"authority\":\"article:category:delete\"},{\"authority\":\"sys:role:list\"},{\"authority\":\"sys:menu:select\"},{\"authority\":\"article:category:info\"},{\"authority\":\"sys:schedule:update\"},{\"authority\":\"sys:schedule:save\"},{\"authority\":\"sys:role:save\"},{\"authority\":\"show:article:detail\"},{\"authority\":\"article:chapter:select\"},{\"authority\":\"sys:schedule:log\"},{\"authority\":\"article:chapter:delete\"},{\"authority\":\"sys:role:update\"},{\"authority\":\"sys:schedule:list\"},{\"authority\":\"article:category:save\"},{\"authority\":\"article:category:select\"},{\"authority\":\"sys:user:save\"},{\"authority\":\"sys:oss:all\"},{\"authority\":\"sys:schedule:info\"},{\"authority\":\"article:chapter:save\"},{\"authority\":\"sys:menu:update\"},{\"authority\":\"sys:menu:delete\"},{\"authority\":\"sys:config:info\"},{\"authority\":\"article:label:select\"},{\"authority\":\"sys:menu:list\"},{\"authority\":\"sys:schedule:resume\"},{\"authority\":\"sys:user:delete\"},{\"authority\":\"article:info:save\"},{\"authority\":\"sys:config:list\"},{\"authority\":\"article:category:list\"},{\"authority\":\"sys:user:update\"},{\"authority\":\"sys:menu:info\"},{\"authority\":\"sys:role:select\"},{\"authority\":\"article:info:select\"},{\"authority\":\"article:label:save\"},{\"authority\":\"sys:user:list\"},{\"authority\":\"sys:menu:save\"},{\"authority\":\"article:chapter:list\"},{\"authority\":\"issue:list\"},{\"authority\":\"sys:role:info\"},{\"authority\":\"sys:schedule:delete\"},{\"authority\":\"article:info:list\"},{\"authority\":\"article:category:list:tree\"},{\"authority\":\"article:label:info\"},{\"authority\":\"sys:user:info\"},{\"authority\":\"article:chapter:update\"},{\"authority\":\"article:info:delete\"},{\"authority\":\"sys:schedule:run\"},{\"authority\":\"sys:config:delete\"},{\"authority\":\"article:chapter:info\"},{\"authority\":\"sys:role:delete\"},{\"authority\":\"article:info:info\"},{\"authority\":\"sys:schedule:pause\"},{\"authority\":\"article:label:list\"},{\"authority\":\"sys:log:list\"}],\"credentialsNonExpired\":true,\"enabled\":true,\"password\":\"$2a$10$jEOxf4Flf3J/QyL3oN5O2OpuQTvPA6kkQaWsdHlyVx5hY6LZ3BW26\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"sysUserEntity\":{\"addr\":\"重庆市\",\"alias\":\"站主\",\"createTime\":1622776288000,\"createUserId\":1,\"email\":\"1419975745@qq.com\",\"img\":\"https://blog-yunglee.oss-cn-chengdu.aliyuncs.com/2021-08-24/e5bca916-2a23-4d2f-8621-6ce40fef04be_logo.png\",\"password\":\"$2a$10$jEOxf4Flf3J/QyL3oN5O2OpuQTvPA6kkQaWsdHlyVx5hY6LZ3BW26\",\"remark\":\"yunglee博客建站初始站主，希望通过这个平台可以认识更多的朋友\",\"salt\":\"lJwNG8tPohVvmxvOhcvE\",\"school\":\"重庆工商大学\",\"status\":1,\"userId\":1,\"username\":\"admin\"},\"user\":{\"$ref\":\"$.sysUserEntity\"},\"userId\":1,\"username\":\"admin\"}', '2021-09-27 11:08:08', '2021-09-27 09:08:08');
INSERT INTO `sys_user_token` VALUES (5, 'fa2c68b782fb98f5cdb766098c264725', '{\"accountNonExpired\":true,\"accountNonLocked\":true,\"authorities\":[{\"authority\":\"article:chapter:save\"},{\"authority\":\"article:chapter:list\"},{\"authority\":\"article:label:select\"},{\"authority\":\"article:chapter:select\"},{\"authority\":\"article:info:list\"},{\"authority\":\"article:info:update\"},{\"authority\":\"article:label:info\"},{\"authority\":\"article:chapter:delete\"},{\"authority\":\"article:chapter:update\"},{\"authority\":\"article:label:update\"},{\"authority\":\"article:label:delete\"},{\"authority\":\"article:info:delete\"},{\"authority\":\"article:info:save\"},{\"authority\":\"article:chapter:info\"},{\"authority\":\"article:info:info\"},{\"authority\":\"article:info:select\"},{\"authority\":\"article:label:list\"},{\"authority\":\"article:label:save\"}],\"credentialsNonExpired\":true,\"enabled\":true,\"password\":\"$2a$10$uNAio0XaYs2ikfsY6KijRuKdfv.ZyuvlPksHkKY/UXsy1IvUx9uNK\",\"salt\":\"BSqXIDoFr9xTNbA5IFCZ\",\"sysUserEntity\":{\"alias\":\"访客\",\"createTime\":1625119148000,\"email\":\"yunglee95@163.com\",\"password\":\"$2a$10$uNAio0XaYs2ikfsY6KijRuKdfv.ZyuvlPksHkKY/UXsy1IvUx9uNK\",\"salt\":\"BSqXIDoFr9xTNbA5IFCZ\",\"status\":1,\"userId\":5,\"username\":\"guest\"},\"user\":{\"$ref\":\"$.sysUserEntity\"},\"userId\":5,\"username\":\"guest\"}', '2021-09-14 20:43:31', '2021-09-14 18:43:31');
INSERT INTO `sys_user_token` VALUES (6, '845f0a1375ef37de7d4d0d563eebe891', '{\"accountNonExpired\":true,\"accountNonLocked\":true,\"authorities\":[{\"authority\":\"article:chapter:save\"},{\"authority\":\"sys:user:list\"},{\"authority\":\"article:chapter:list\"},{\"authority\":\"article:label:select\"},{\"authority\":\"article:chapter:select\"},{\"authority\":\"article:info:list\"},{\"authority\":\"article:info:update\"},{\"authority\":\"article:label:info\"},{\"authority\":\"article:chapter:delete\"},{\"authority\":\"sys:user:info\"},{\"authority\":\"article:chapter:update\"},{\"authority\":\"article:label:update\"},{\"authority\":\"article:label:delete\"},{\"authority\":\"article:info:delete\"},{\"authority\":\"article:info:save\"},{\"authority\":\"article:chapter:info\"},{\"authority\":\"article:info:info\"},{\"authority\":\"article:info:select\"},{\"authority\":\"article:label:list\"},{\"authority\":\"article:label:save\"}],\"credentialsNonExpired\":true,\"enabled\":true,\"password\":\"$2a$10$FRndjiCSvG.jTfczw0VgQODP5NpY5XXj1NrkrDxqhWVQ/UBF2HodK\",\"salt\":\"CNLzTEblyR2a8cFXiZj2\",\"sysUserEntity\":{\"alias\":\"yunglee995\",\"createTime\":1631359135000,\"createUserId\":1,\"email\":\"yunglee995@gmail.com\",\"password\":\"$2a$10$FRndjiCSvG.jTfczw0VgQODP5NpY5XXj1NrkrDxqhWVQ/UBF2HodK\",\"salt\":\"CNLzTEblyR2a8cFXiZj2\",\"status\":1,\"userId\":6,\"username\":\"yunglee995\"},\"user\":{\"$ref\":\"$.sysUserEntity\"},\"userId\":6,\"username\":\"yunglee995\"}', '2021-09-14 20:43:46', '2021-09-14 18:43:46');

SET FOREIGN_KEY_CHECKS = 1;
