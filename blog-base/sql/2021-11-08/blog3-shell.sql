
ALTER TABLE `blog3_article`.`article_chapter`
ADD COLUMN `source_id` bigint(20) NULL COMMENT '来源id，自己输入为 0；其他来源，记录result的id值' AFTER `content_text`;

ALTER TABLE `blog3_article`.`article_chapter`
MODIFY COLUMN `id` bigint(20) NOT NULL COMMENT '主键' FIRST,
MODIFY COLUMN `source_id` bigint(20) NULL DEFAULT 0 COMMENT '来源id，自己输入为 0；其他来源，记录result的id值' AFTER `content_text`;

