/*
 Navicat Premium Data Transfer

 Source Server         : 169.254.75.100（3307-prod）
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 169.254.75.100:3307
 Source Schema         : blog3-nacos

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 28/09/2021 09:14:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for config_info
-- ----------------------------
DROP TABLE IF EXISTS `config_info`;
CREATE TABLE `config_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  `c_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_use` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `effect` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `c_schema` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfo_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of config_info
-- ----------------------------
INSERT INTO `config_info` VALUES (1, 'common.yaml', 'DEFAULT_GROUP', '\r\n#通用配置点\r\ncommon:\r\n  port:\r\n    blog-gateway: 8000\r\n    blog-user: 8100\r\n    blog-article: 8200\r\n    blog-third: 9100\r\n    blog-data: 9200\r\n\r\n\r\n\r\n#mybatis\r\nmybatis-plus:\r\n  mapper-locations: classpath*:/mapper/**/*.xml\r\n  #实体扫描，多个package用逗号或者分号分隔\r\n  typeAliasesPackage: com.ly.blog*.*.entity\r\n  global-config:\r\n    #数据库相关配置\r\n    db-config:\r\n      #主键类型  AUTO:\"数据库ID自增\", INPUT:\"用户输入ID\", ID_WORKER:\"全局唯一ID (数字类型唯一ID)\", UUID:\"全局唯一ID UUID\";\r\n      id-type: AUTO\r\n      logic-delete-value: -1\r\n      logic-not-delete-value: 0\r\n    banner: false\r\n  #原生配置\r\n  configuration:\r\n    map-underscore-to-camel-case: true\r\n    cache-enabled: false\r\n    call-setters-on-nulls: true\r\n    jdbc-type-for-null: \'null\'\r\n\r\n\r\n\r\n#日志级别\r\n# 自定义配置需添加配置文件\r\n#  config: xxxx.xml\r\n#logging:\r\n#  level:\r\n#    com.ly.blog_user: info #无效\r\n\r\nlogging:\r\n  level:\r\n    root: info\r\n\r\n', '15150388d819ccfcb2c52d6b10e6f72a', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (2, 'user.yaml', 'DEFAULT_GROUP', '\r\nspring:\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', '635d7121caf41b1e82b11d5f2b8ad637', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (3, 'article.yaml', 'DEFAULT_GROUP', '\r\n\r\nspring:\r\n\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', 'b249bae2f547c4d0b34095487e6c969a', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (4, 'third.yaml', 'DEFAULT_GROUP', 'spring:\r\n  cloud:\r\n    \r\n    alicloud:\r\n      access-key: LTAI5tBMviENdzK5QrE5YFAG\r\n      secret-key: cPgOwwZsbglMeQJOUQT38qh2jUXgS5\r\n      oss:\r\n        endpoint: oss-cn-chengdu.aliyuncs.com\r\n        bucket-name: blog-yunglee\r\n\r\n  \r\n  servlet:\r\n    multipart:\r\n      max-file-size: 1024MB  #默认1M\r\n      max-request-size: 1024MB #默认10M\r\n\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', '942390003e7fe542f1b4dfa9b5bcc8f3', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (5, 'source-user.yaml', 'DEFAULT_GROUP', '\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        url: jdbc:mysql://169.254.244.100:3306/blog3_user?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        #url: jdbc:mysql://169.254.244.100:3307/blog3_user?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        username: root\r\n        password: 123456\r\n        initial-size: 10\r\n        max-active: 100\r\n        min-idle: 10\r\n        max-wait: 60000\r\n        pool-prepared-statements: true\r\n        max-pool-prepared-statement-per-connection-size: 20\r\n        time-between-eviction-runs-millis: 60000\r\n        min-evictable-idle-time-millis: 300000\r\n        #Oracle需要打开注释\r\n        #validation-query: SELECT 1 FROM DUAL\r\n        test-while-idle: true\r\n        test-on-borrow: false\r\n        test-on-return: false\r\n        stat-view-servlet:\r\n            enabled: true\r\n            url-pattern: /druid/*\r\n            #login-username: admin\r\n            #login-password: admin\r\n        filter:\r\n            stat:\r\n                log-slow-sql: true\r\n                slow-sql-millis: 1000\r\n                merge-sql: false\r\n            wall:\r\n                config:\r\n                    multi-statement-allow: true\r\n\r\n', '455af012c5c4da684283f71c59a16b41', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (6, 'source-article.yaml', 'DEFAULT_GROUP', '\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        url: jdbc:mysql://169.254.244.100:3306/blog3_article?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        #url: jdbc:mysql://169.254.244.100:3307/blog3_article?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        username: root\r\n        password: 123456\r\n        initial-size: 10\r\n        max-active: 100\r\n        min-idle: 10\r\n        max-wait: 60000\r\n        pool-prepared-statements: true\r\n        max-pool-prepared-statement-per-connection-size: 20\r\n        time-between-eviction-runs-millis: 60000\r\n        min-evictable-idle-time-millis: 300000\r\n        #Oracle需要打开注释\r\n        #validation-query: SELECT 1 FROM DUAL\r\n        test-while-idle: true\r\n        test-on-borrow: false\r\n        test-on-return: false\r\n        stat-view-servlet:\r\n            enabled: true\r\n            url-pattern: /druid/*\r\n            #login-username: admin\r\n            #login-password: admin\r\n        filter:\r\n            stat:\r\n                log-slow-sql: true\r\n                slow-sql-millis: 1000\r\n                merge-sql: false\r\n            wall:\r\n                config:\r\n                    multi-statement-allow: true\r\n\r\n', 'fe3555ecae5df37f41368bb3426e8b48', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);
INSERT INTO `config_info` VALUES (7, 'data.yaml', 'DEFAULT_GROUP', 'id: name', 'e546daca3dd13a67d0cbc5800edb482a', '2021-06-25 18:54:08', '2021-06-25 18:54:08', NULL, '169.254.244.1', '', '', NULL, NULL, NULL, 'yaml', NULL);

-- ----------------------------
-- Table structure for config_info_aggr
-- ----------------------------
DROP TABLE IF EXISTS `config_info_aggr`;
CREATE TABLE `config_info_aggr`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `datum_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'datum_id',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `gmt_modified` datetime(0) NOT NULL COMMENT '修改时间',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfoaggr_datagrouptenantdatum`(`data_id`, `group_id`, `tenant_id`, `datum_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '增加租户字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_beta
-- ----------------------------
DROP TABLE IF EXISTS `config_info_beta`;
CREATE TABLE `config_info_beta`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `beta_ips` varchar(1024) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'betaIps',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfobeta_datagrouptenant`(`data_id`, `group_id`, `tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_beta' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_info_tag
-- ----------------------------
DROP TABLE IF EXISTS `config_info_tag`;
CREATE TABLE `config_info_tag`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tag_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_id',
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'content',
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'md5',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL COMMENT 'source user',
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'source ip',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_configinfotag_datagrouptenanttag`(`data_id`, `group_id`, `tenant_id`, `tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_info_tag' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for config_tags_relation
-- ----------------------------
DROP TABLE IF EXISTS `config_tags_relation`;
CREATE TABLE `config_tags_relation`  (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `tag_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'tag_name',
  `tag_type` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tag_type',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'data_id',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'group_id',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `nid` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`nid`) USING BTREE,
  UNIQUE INDEX `uk_configtagrelation_configidtag`(`id`, `tag_name`, `tag_type`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'config_tag_relation' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for group_capacity
-- ----------------------------
DROP TABLE IF EXISTS `group_capacity`;
CREATE TABLE `group_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数，，0表示使用默认值',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_group_id`(`group_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '集群、各Group容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for his_config_info
-- ----------------------------
DROP TABLE IF EXISTS `his_config_info`;
CREATE TABLE `his_config_info`  (
  `id` bigint(64) UNSIGNED NOT NULL,
  `nid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `group_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `app_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'app_name',
  `content` longtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `md5` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00',
  `src_user` text CHARACTER SET utf8 COLLATE utf8_bin NULL,
  `src_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `op_type` char(10) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT '租户字段',
  PRIMARY KEY (`nid`) USING BTREE,
  INDEX `idx_gmt_create`(`gmt_create`) USING BTREE,
  INDEX `idx_gmt_modified`(`gmt_modified`) USING BTREE,
  INDEX `idx_did`(`data_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '多租户改造' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_config_info
-- ----------------------------
INSERT INTO `his_config_info` VALUES (0, 1, 'common.yaml', 'DEFAULT_GROUP', '', '\r\n#通用配置点\r\ncommon:\r\n  port:\r\n    blog-gateway: 8000\r\n    blog-user: 8100\r\n    blog-article: 8200\r\n    blog-third: 9100\r\n    blog-data: 9200\r\n\r\n\r\n\r\n#mybatis\r\nmybatis-plus:\r\n  mapper-locations: classpath*:/mapper/**/*.xml\r\n  #实体扫描，多个package用逗号或者分号分隔\r\n  typeAliasesPackage: com.ly.blog*.*.entity\r\n  global-config:\r\n    #数据库相关配置\r\n    db-config:\r\n      #主键类型  AUTO:\"数据库ID自增\", INPUT:\"用户输入ID\", ID_WORKER:\"全局唯一ID (数字类型唯一ID)\", UUID:\"全局唯一ID UUID\";\r\n      id-type: AUTO\r\n      logic-delete-value: -1\r\n      logic-not-delete-value: 0\r\n    banner: false\r\n  #原生配置\r\n  configuration:\r\n    map-underscore-to-camel-case: true\r\n    cache-enabled: false\r\n    call-setters-on-nulls: true\r\n    jdbc-type-for-null: \'null\'\r\n\r\n\r\n\r\n#日志级别\r\n# 自定义配置需添加配置文件\r\n#  config: xxxx.xml\r\n#logging:\r\n#  level:\r\n#    com.ly.blog_user: info #无效\r\n\r\nlogging:\r\n  level:\r\n    root: info\r\n\r\n', '15150388d819ccfcb2c52d6b10e6f72a', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 2, 'user.yaml', 'DEFAULT_GROUP', '', '\r\nspring:\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', '635d7121caf41b1e82b11d5f2b8ad637', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 3, 'article.yaml', 'DEFAULT_GROUP', '', '\r\n\r\nspring:\r\n\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', 'b249bae2f547c4d0b34095487e6c969a', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 4, 'third.yaml', 'DEFAULT_GROUP', '', 'spring:\r\n  cloud:\r\n    \r\n    alicloud:\r\n      access-key: LTAI5tBMviENdzK5QrE5YFAG\r\n      secret-key: cPgOwwZsbglMeQJOUQT38qh2jUXgS5\r\n      oss:\r\n        endpoint: oss-cn-chengdu.aliyuncs.com\r\n        bucket-name: blog-yunglee\r\n\r\n  \r\n  servlet:\r\n    multipart:\r\n      max-file-size: 1024MB  #默认1M\r\n      max-request-size: 1024MB #默认10M\r\n\r\n# 配置redis\r\n  redis:\r\n    host: 169.254.244.100', '942390003e7fe542f1b4dfa9b5bcc8f3', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 5, 'source-user.yaml', 'DEFAULT_GROUP', '', '\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        url: jdbc:mysql://169.254.244.100:3306/blog3_user?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        #url: jdbc:mysql://169.254.244.100:3307/blog3_user?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        username: root\r\n        password: 123456\r\n        initial-size: 10\r\n        max-active: 100\r\n        min-idle: 10\r\n        max-wait: 60000\r\n        pool-prepared-statements: true\r\n        max-pool-prepared-statement-per-connection-size: 20\r\n        time-between-eviction-runs-millis: 60000\r\n        min-evictable-idle-time-millis: 300000\r\n        #Oracle需要打开注释\r\n        #validation-query: SELECT 1 FROM DUAL\r\n        test-while-idle: true\r\n        test-on-borrow: false\r\n        test-on-return: false\r\n        stat-view-servlet:\r\n            enabled: true\r\n            url-pattern: /druid/*\r\n            #login-username: admin\r\n            #login-password: admin\r\n        filter:\r\n            stat:\r\n                log-slow-sql: true\r\n                slow-sql-millis: 1000\r\n                merge-sql: false\r\n            wall:\r\n                config:\r\n                    multi-statement-allow: true\r\n\r\n', '455af012c5c4da684283f71c59a16b41', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 6, 'source-article.yaml', 'DEFAULT_GROUP', '', '\r\nspring:\r\n  datasource:\r\n    type: com.alibaba.druid.pool.DruidDataSource\r\n    druid:\r\n        driver-class-name: com.mysql.cj.jdbc.Driver\r\n        url: jdbc:mysql://169.254.244.100:3306/blog3_article?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        #url: jdbc:mysql://169.254.244.100:3307/blog3_article?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai\r\n        username: root\r\n        password: 123456\r\n        initial-size: 10\r\n        max-active: 100\r\n        min-idle: 10\r\n        max-wait: 60000\r\n        pool-prepared-statements: true\r\n        max-pool-prepared-statement-per-connection-size: 20\r\n        time-between-eviction-runs-millis: 60000\r\n        min-evictable-idle-time-millis: 300000\r\n        #Oracle需要打开注释\r\n        #validation-query: SELECT 1 FROM DUAL\r\n        test-while-idle: true\r\n        test-on-borrow: false\r\n        test-on-return: false\r\n        stat-view-servlet:\r\n            enabled: true\r\n            url-pattern: /druid/*\r\n            #login-username: admin\r\n            #login-password: admin\r\n        filter:\r\n            stat:\r\n                log-slow-sql: true\r\n                slow-sql-millis: 1000\r\n                merge-sql: false\r\n            wall:\r\n                config:\r\n                    multi-statement-allow: true\r\n\r\n', 'fe3555ecae5df37f41368bb3426e8b48', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');
INSERT INTO `his_config_info` VALUES (0, 7, 'data.yaml', 'DEFAULT_GROUP', '', 'id: name', 'e546daca3dd13a67d0cbc5800edb482a', '2010-05-05 00:00:00', '2021-06-25 18:54:08', NULL, '169.254.244.1', 'I', '');

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `resource` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `uk_role_permission`(`role`, `resource`, `action`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `uk_username_role`(`username`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('nacos', 'ROLE_ADMIN');

-- ----------------------------
-- Table structure for tenant_capacity
-- ----------------------------
DROP TABLE IF EXISTS `tenant_capacity`;
CREATE TABLE `tenant_capacity`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT 'Tenant ID',
  `quota` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配额，0表示使用默认值',
  `usage` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '使用量',
  `max_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
  `max_aggr_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '聚合子配置最大个数',
  `max_aggr_size` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
  `max_history_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最大变更历史数量',
  `gmt_create` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '创建时间',
  `gmt_modified` datetime(0) NOT NULL DEFAULT '2010-05-05 00:00:00' COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '租户容量信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tenant_info
-- ----------------------------
DROP TABLE IF EXISTS `tenant_info`;
CREATE TABLE `tenant_info`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `kp` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'kp',
  `tenant_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_id',
  `tenant_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT '' COMMENT 'tenant_name',
  `tenant_desc` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'tenant_desc',
  `create_source` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'create_source',
  `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
  `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_tenant_info_kptenantid`(`kp`, `tenant_id`) USING BTREE,
  INDEX `idx_tenant_id`(`tenant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = 'tenant_info' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', 1);

SET FOREIGN_KEY_CHECKS = 1;
