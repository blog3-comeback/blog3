/*
 Navicat Premium Data Transfer

 Source Server         : 169.254.75.100（3307-prod）
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : 169.254.75.100:3307
 Source Schema         : blog3_third

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 28/09/2021 09:13:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for email_record
-- ----------------------------
DROP TABLE IF EXISTS `email_record`;
CREATE TABLE `email_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `to_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送的邮箱，多个逗号分隔',
  `is_success` int(2) NULL DEFAULT NULL COMMENT '是否成功 0成功 1失败',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '发送的内容',
  `type` int(2) NULL DEFAULT NULL COMMENT '邮件类型 0，注册 1，登录...',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of email_record
-- ----------------------------
INSERT INTO `email_record` VALUES (1, '[yunglee995@gmail.com]', 0, '401674', 0, NULL, '2021-09-11 19:18:10', NULL, '2021-09-11 19:18:10');

-- ----------------------------
-- Table structure for email_template
-- ----------------------------
DROP TABLE IF EXISTS `email_template`;
CREATE TABLE `email_template`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` int(10) NULL DEFAULT NULL COMMENT '类型，1、登录；2、注册；3、通知',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '主题',
  `template` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '邮箱模板',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `last_update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `last_update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `type`(`type`) USING BTREE COMMENT '唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of email_template
-- ----------------------------
INSERT INTO `email_template` VALUES (1, 1, 'young@你 | 欢迎您的加入', '<div class=\"content\"><h4><b>你好，注册者</b>：<br>&nbsp; &nbsp; 欢迎加入 <b>young@你</b> 博客系统<br>&nbsp; &nbsp; 你注册的验证码是： <b>#{emailCode}&nbsp;<br></b><b>&nbsp; &nbsp;</b> <u>验证码的有效时间是 30 分钟，请注意过期时间</u></h4><h6><span style=\"font-family: &quot;Times New Roman&quot;;\">yunglee博客系统，一个简单的生活记录平台，留下所闻所想，和大家分享和探讨生活的趣事和烦恼。畅所欲言，无所顾忌。 还支持点小小的网页抓取，有兴趣可以自己试试。</span></h6>\n</div>', '1', '2021-09-15 10:34:58', '1', '2021-09-15 16:31:44');
INSERT INTO `email_template` VALUES (2, 2, 'young@你 | 邮箱登录验证码', '<div class=\"content\"><h4><span style=\"font-weight: bolder;\">你好，</span><b>#{email}</b><span style=\"font-size: 1.5rem;\">：</span></h4><h4>&nbsp; &nbsp; 欢迎回到&nbsp;<span style=\"font-weight: bolder;\">young@你</span>&nbsp;博客系统<br>&nbsp; &nbsp; 你登录的验证码是：&nbsp;<span style=\"font-weight: bolder;\">#{emailCode}&nbsp;<br></span><span style=\"font-weight: bolder;\">&nbsp; &nbsp;</span>&nbsp;<u>验证码的有效时间是 30 分钟，请注意过期时间</u></h4><h6><span style=\"font-family: &quot;Times New Roman&quot;;\">yunglee博客系统，一个简单的生活记录平台，留下所闻所想，和大家分享和探讨生活的趣事和烦恼。畅所欲言，无所顾忌。 还支持点小小的网页抓取，有兴趣可以自己试试。</span></h6>\n</div>', '1', '2021-09-15 10:34:58', '1', '2021-09-15 16:39:47');
INSERT INTO `email_template` VALUES (3, 3, 'young@你 | 你有最新的评价信息，请注意查收', '<div class=\"content\"><p>你有新的信息：</p><p>&nbsp; &nbsp; 用户【 #{userAlias} 】评价了你的文章 《 #{title} 》</p><p>&nbsp; &nbsp;评价时间：#{issueTime}</p><h6><span style=\"font-family: &quot;Times New Roman&quot;;\">yunglee博客系统，一个简单的生活记录平台，留下所闻所想，和大家分享和探讨生活的趣事和烦恼。畅所欲言，无所顾忌。 还支持点小小的网页抓取，有兴趣可以自己试试。</span></h6>\n</div>', '1', '2021-09-15 10:34:58', '1', '2021-09-15 16:45:04');

SET FOREIGN_KEY_CHECKS = 1;
