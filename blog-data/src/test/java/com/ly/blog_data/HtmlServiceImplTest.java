package com.ly.blog_data;

import com.alibaba.fastjson.JSONObject;
import com.ly.blog_data.entity.CatalogueRuleEntity;
import com.ly.blog_data.entity.ContentRuleEntity;
import com.ly.blog_data.service.impl.HtmlServiceImpl;
import com.ly.blog_data.utils.HtmlInfoExplainUtils;
import com.ly.blog_data.utils.HttpJsonDataUtils;

/**
 * @author ly create at 2021/6/22 - 18:05
 **/
public class HtmlServiceImplTest extends BaseTest{

//    @Test
    public void test1(){
        HtmlServiceImpl htmlService = new HtmlServiceImpl();
        String url = "http://news.youth.cn/gn/";
        CatalogueRuleEntity catalogueRuleEntity = new CatalogueRuleEntity();

        catalogueRuleEntity.setAreaPreList("<ul class=\"tj3_1\">");
        catalogueRuleEntity.setAreaPostList("</ul>");

        catalogueRuleEntity.setContentReg("<a href=\"//news.youth.cn","\">","</a>");

        ContentRuleEntity contentRuleEntity = new ContentRuleEntity();
        contentRuleEntity.setAreaPreList("<div class=TRS_Editor>");
        contentRuleEntity.setAreaPostList("</div>");

        htmlService.catchData(url, catalogueRuleEntity, contentRuleEntity);

    }

    //测试目录
//    @Test
    public void testCatalogue(){
        HtmlServiceImpl htmlService = new HtmlServiceImpl();
//        String url = "http://news.youth.cn/gn/";
        String url = "http://www.wenxue88.com/beitaoyandeyongqi/index.html";
        String htmlContent = HtmlInfoExplainUtils.getHtmlContent(url,false);
        System.out.println(htmlContent);
    }

    //测试内容
//    @Test
    public void testContent(){
        HtmlServiceImpl htmlService = new HtmlServiceImpl();
//        String url = "http://news.youth.cn/gn/202106/t20210623_13042861.htm";
        String url = "http://www.wenxue88.com/beitaoyandeyongqi/btydyq_3000.html";
        String htmlContent = HtmlInfoExplainUtils.getHtmlContent(url,false);
        System.out.println(htmlContent);
    }


    //东方财富网测试
//    @Test
    public void testFinancial(){
        HtmlServiceImpl htmlService = new HtmlServiceImpl();
//        String url = "http://news.youth.cn/gn/202106/t20210623_13042861.htm";
//        String url = "http://quote.eastmoney.com/sz002607.html";

        //网友调查数据
        //{"Status":1,"Message":"","Data":{"TapeZ":0.3249,"TapeD":0.6751,"TapeType":0,"Date":"2021-07-02"}}
        String u1 = "http://quote.eastmoney.com/newapi/getstockvote?code=sz002607";

        //jQuery1124016364125361377457_1625213839362
        // ({"rc":0,"rt":12,"svr":180606313,"lt":1,"full":1,
        // "data":{"code":"002607","market":0,"decimal":2,"prePrice":20.39,
        // "details":["14:56:33,19.66,74,15,1","14:56:36,19.67,7,4,2","14:56:39,19.67,1,1,2",
        // "14:56:42,19.65,68,26,1","14:56:45,19.65,71,43,1",
        // "14:56:48,19.66,13,1,2","14:56:51,19.65,44,12,1",
        // "14:56:54,19.65,33,3,1","14:56:57,19.65,31,8,1",
        // "14:57:00,19.66,16,7,2","15:00:59,19.68,2539,274,1"]}});
        String u2 = "http://push2.eastmoney.com/api/qt/stock/details/get?ut=fa5fd1943c7b386f172d6893dbfba10b&fields1=f1,f2,f3,f4&fields2=f51,f52,f53,f54,f55&pos=-11&secid=0.002607&cb=jQuery1124016364125361377457_1625213839362&_=1625213839363";



        String url = "http://quote.eastmoney.com/sz002607.html";
        JSONObject r = HttpJsonDataUtils.getJSONData(url);
        System.out.println(r);
    }

}
