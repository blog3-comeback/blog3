package com.ly.blog_data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author ly create at 2021/6/21 - 17:24
 **/

@EnableScheduling
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.ly.blog_data.feign"})
@SpringBootApplication(scanBasePackages = {"com.ly"})
public class DataApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataApplication.class,args);
    }

}
