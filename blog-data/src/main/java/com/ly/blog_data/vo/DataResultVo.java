package com.ly.blog_data.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

/**
 * 任务结果输出对象
 *
 * @author ly
 * create at 2021/11/5 - 18:09
 **/
@Data
@Builder
public class DataResultVo {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    private Integer id;
    /**
     * 任务id
     */
    private Integer taskId;
    /**
     * 执行的url
     */
    private String exeUrl;
    /**
     * 目录名
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 执行结果；0失败，1、成功
     */
    private Integer state;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String lastUpdateBy;
    /**
     * 更新时间
     */
    private Date lastUpdateTime;


    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 任务名称
     */
    private Integer taskType;
    /**
     * 是否推送，0，未推送；1、已推送
     */
    private Integer isPush;

}
