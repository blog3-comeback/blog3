package com.ly.blog_data.schedule;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ly.blog_data.entity.DataTaskEntity;
import com.ly.blog_data.service.DataTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据任务定时执行
 * @author ly create at 2021/6/23 - 22:35
 **/
@Slf4j
@Service
public class DataTaskSchedule {

    @Autowired
    private DataTaskService dataTaskService;

//    @Scheduled(cron="0/5 * * * * ?")
    @Scheduled(cron = "0 0/10 * * * ? ")
    public void execTask(){
        //获取新闻任务
        List<DataTaskEntity> dataTaskEntities = dataTaskService.list(new QueryWrapper<DataTaskEntity>().eq("type", 0));

        dataTaskEntities.forEach(item ->{
            log.info("【定时执行任务】 ---- 开始 -- 任务号 --- {}",item.getName());
            dataTaskService.exec(item.getId());
            log.info("【定时执行任务】 ---- 结束 -- 任务号 --- {}",item.getName());
        });
    }

}
