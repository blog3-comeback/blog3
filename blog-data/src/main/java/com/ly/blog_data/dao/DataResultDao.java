package com.ly.blog_data.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ly.blog_data.entity.DataResultEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
@Mapper
public interface DataResultDao extends BaseMapper<DataResultEntity> {
	
}
