package com.ly.blog_data.feign;

import com.ly.blog_common.entity.ChapterEntity;
import com.ly.blog_common.entity.InfoEntity;
import com.ly.blog_common.utils.R;
import com.ly.blog_data.feign.config.FeignConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 文章接口
 * @author ly create at 2021/6/26 - 13:44
 **/
@Service
@FeignClient(value = "blog-article", configuration = FeignConfig.class)
public interface ArticleService {

    @RequestMapping("/article/chapter/push/result")
    Integer articlePushResult(@RequestBody Integer id);

    @RequestMapping("/article/info/save")
    public R articleSave(@RequestBody InfoEntity info);


    @RequestMapping("/article/chapter/save")
    public R chapterSave(@RequestBody ChapterEntity chapter);


}
