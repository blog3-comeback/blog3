package com.ly.blog_data.feign;

import com.ly.blog_common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 第三方接口
 * @author ly create at 2021/6/24 - 9:48
 **/
@Service
@FeignClient(value = "blog-third")
public interface ThirdService {

    @GetMapping("/third/file/netFileUpload")
    public R netFileUpload(@RequestParam(value = "netUrl") String netUrl) throws Exception;


}
