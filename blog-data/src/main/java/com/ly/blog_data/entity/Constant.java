package com.ly.blog_data.entity;

/**
 * 常量类
 * @author ly create at 2021/6/22 - 13:48
 **/
public class Constant {

    public static final String NEXT_LINE = "\n";

    public static final String DEFAULT_CHARSET = "UTF-8";


    public static final String REG_ALL_ = "([\\s\\S]*)"; //最大匹配

    public static final String REG_MIX_ = "(.*?)"; //行 最小匹配

    public static final String REG_ALL_MIX_ = "([\\s\\S]*?)"; //整体 最小匹配
}
