package com.ly.blog_data.entity;

import lombok.Data;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 内容解析规则对象定义
 * @author ly create at 2021/6/22 - 13:37
 **/
@Data
public class ContentRuleEntity {

    //内容区间锁定
    private List<String> areaPreList; //前置区间条件 中间模糊匹配

    private List<String> areaPostList; //后置区间条件

    private Map<String,String> replaceCondition = new HashMap<>();


    public void setAreaPreList(String ... areaPre){
        this.areaPreList = Arrays.asList(areaPre);
    }

    public void setAreaPostList(String ... areaPost){
        this.areaPostList = Arrays.asList(areaPost);
    }

}
