package com.ly.blog_data.entity;

import lombok.Data;

/**
 * 记录目录数据
 * @author ly create at 2021/6/22 - 17:04
 **/
@Data
public class CatalogueEntity {

    private String url;

    private String catalogueName;

    private String content;

    @Override
    public String toString(){
        StringBuffer stringBuffer = new StringBuffer();
        return stringBuffer.append("url:").append(this.url)
            .append("catalogueName:").append(this.catalogueName)
            .append("content:").append(this.content)
            .toString();
    }
}
