package com.ly.blog_data.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * 
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
@Data
@TableName("data_task")
public class DataTaskEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@TableId
	private Integer id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 类型：0，新闻（目录即文章名） 1，小说（文章目录）
	 */
	private Integer type;
	/**
	 * 目录url
	 */
	private String catalogueUrl;
	/**
	 * 目录规则
	 */
	private String catalogueRule;
	/**
	 * 内容规则
	 */
	private String contentRule;
    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 更新人
     */
    private String lastUpdateBy;
    /**
     * 更新时间
     */
    private Date lastUpdateTime;

}
