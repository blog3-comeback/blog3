package com.ly.blog_data.entity;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * 目录解析规则对象定义
 * @author ly create at 2021/6/22 - 13:37
 **/
@Data
public class CatalogueRuleEntity {

    //目录区间锁定
    private List<String> areaPreList; //前置区间条件 中间模糊匹配
    private List<String> areaPostList; //后置区间条件

    //数据获取两端值，目录名 - url
    private String dataPre;
    private String dataCenter;
    private String dataPost;

    public void setAreaPreList(String ... areaPre){
        this.areaPreList = Arrays.asList(areaPre);
    }

    public void setAreaPostList(String ... areaPost){
        this.areaPostList = Arrays.asList(areaPost);
    }

    public void setContentReg(String dataPre,String dataCenter,String dataPost) {
        this.dataPre = dataPre;
        this.dataCenter = dataCenter;
        this.dataPost = dataPost;
    }
}
