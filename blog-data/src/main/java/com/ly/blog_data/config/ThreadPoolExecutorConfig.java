package com.ly.blog_data.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 自定线程池
 *
 * @author ly
 * create at 2021/9/19 - 14:11
 **/
@Configuration
public class ThreadPoolExecutorConfig {

    @Bean
    public ExecutorService singleThreadExecutor(){
        return Executors.newSingleThreadExecutor();
    }

}
