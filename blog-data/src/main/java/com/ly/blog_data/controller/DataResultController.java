package com.ly.blog_data.controller;

import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_data.entity.DataResultEntity;
import com.ly.blog_data.service.DataResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;




/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
@RestController
@RequestMapping("data/dataresult")
public class DataResultController {
    @Autowired
    private DataResultService dataResultService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("data:dataresult:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dataResultService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("data:dataresult:info")
    public R info(@PathVariable("id") Integer id){
		DataResultEntity dataResult = dataResultService.getById(id);

        return R.ok().put("dataResult", dataResult);
    }


    /**
     * 推送
     * @param id 结果id
     * @param pushType 全部还是当前 0,1 - 新闻类型为单一，小说类型为全部获取或者当前
     * @return
     */
    @RequestMapping("/push")
    // @RequiresPermissions("data:dataresult:info")
    public R push(@RequestParam(value = "id") Integer id,
                  @RequestParam(value = "pushType") Integer pushType){
		dataResultService.push(id,pushType);

        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("data:dataresult:save")
    public R save(@RequestBody DataResultEntity dataResult){
		dataResultService.save(dataResult);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("data:dataresult:update")
    public R update(@RequestBody DataResultEntity dataResult){
		dataResultService.updateById(dataResult);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("data:dataresult:delete")
    public R delete(@RequestBody Integer[] ids){
		dataResultService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
