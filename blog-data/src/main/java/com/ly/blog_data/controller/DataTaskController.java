package com.ly.blog_data.controller;

import com.ly.blog_common.constant.DataConstant;
import com.ly.blog_common.security.SecurityUtils;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_common.utils.R;
import com.ly.blog_data.entity.DataTaskEntity;
import com.ly.blog_data.service.DataTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutorService;


/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
@RestController
@RequestMapping("data/datatask")
public class DataTaskController {

    @Autowired
    private DataTaskService dataTaskService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private ExecutorService singleThreadExecutor;

    /**
     * 列表
     */
    @RequestMapping("/list")
    // @RequiresPermissions("data:datatask:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = dataTaskService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    // @RequiresPermissions("data:datatask:info")
    public R info(@PathVariable("id") Integer id){

		DataTaskEntity dataTask = dataTaskService.getById(id);

        return R.ok().put("dataTask", dataTask);
    }

    /**
     * 执行任务
     */
    @RequestMapping("/exec/percentage")
    // @RequiresPermissions("data:datatask:info")
    public R execPercentage(){
        return R.ok().put("percentage",redisTemplate.opsForValue().get(DataConstant.TASK_PERCENTAGE));
    }

    @RequestMapping("/exec/{id}")
    // @RequiresPermissions("data:datatask:info")
    public R exec(@PathVariable("id") Integer id){
        singleThreadExecutor.submit(()->{
            boolean result = dataTaskService.exec(id);
        });
        return R.ok();
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    // @RequiresPermissions("data:datatask:save")
    public R save(@RequestBody DataTaskEntity dataTask){
        dataTask.setCreateBy(SecurityUtils.getUserId());
        dataTask.setCreateTime(new Date());
        dataTask.setLastUpdateBy(SecurityUtils.getUserId());
        dataTask.setLastUpdateTime(new Date());
		dataTaskService.save(dataTask);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    // @RequiresPermissions("data:datatask:update")
    public R update(@RequestBody DataTaskEntity dataTask){
        dataTask.setLastUpdateBy(SecurityUtils.getUserId());
        dataTask.setLastUpdateTime(new Date());
		dataTaskService.updateById(dataTask);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    // @RequiresPermissions("data:datatask:delete")
    public R delete(@RequestBody Integer[] ids){
		dataTaskService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

    /**
     * 获取url对应结构数据
     */
    @RequestMapping("/parse")
    public R parseHtml(@RequestBody String url){
        return R.ok().put("data",dataTaskService.parseHtml(url));
    }

    @RequestMapping("/check")
    public R check(@RequestBody DataTaskEntity dataTask){
        return R.ok().put("data",dataTaskService.check(dataTask));
    }

}
