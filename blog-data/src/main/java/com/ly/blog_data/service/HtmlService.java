package com.ly.blog_data.service;

import com.ly.blog_data.entity.CatalogueEntity;
import com.ly.blog_data.entity.CatalogueRuleEntity;
import com.ly.blog_data.entity.ContentRuleEntity;
import com.ly.blog_data.parse.MsgList;

import java.util.List;

/**
 * 通过html捕获数据
 * @author ly create at 2021/6/22 - 13:33
 **/
public interface HtmlService {

    /**
     * 捕获数据 输入目录的url 解析出每个数据单元信息 解析具体数据
     * @param catalogueURL
     * @param catalogueRuleEntity
     * @param contentRuleEntity
     * @return
     */
    List<CatalogueEntity> catchData(String catalogueURL, CatalogueRuleEntity catalogueRuleEntity, ContentRuleEntity contentRuleEntity);


    MsgList<String> checkData(String catalogueUrl, CatalogueRuleEntity catalogueRuleEntity, ContentRuleEntity contentRuleEntity);
}
