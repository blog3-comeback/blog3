package com.ly.blog_data.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_data.entity.DataTaskEntity;
import com.ly.blog_data.parse.MsgList;

import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
public interface DataTaskService extends IService<DataTaskEntity> {

    MsgList<String> check(DataTaskEntity dataTask);

    PageUtils queryPage(Map<String, Object> params);

    boolean exec(Integer id);

    String parseHtml(String url);
}

