package com.ly.blog_data.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ly.blog_common.utils.PageUtils;
import com.ly.blog_data.entity.DataResultEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 
 *
 * @author liyang
 *
 * @date 2021-06-23 09:28:40
 */
public interface DataResultService extends IService<DataResultEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void push(Integer id, Integer pushType);
}

