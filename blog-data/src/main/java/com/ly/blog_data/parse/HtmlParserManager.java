package com.ly.blog_data.parse;

import com.ly.blog_data.entity.CatalogueEntity;
import com.ly.blog_data.entity.CatalogueRuleEntity;
import com.ly.blog_data.entity.ContentRuleEntity;
import com.ly.blog_data.feign.ThirdService;

import java.util.List;

/**
 * 解析Html内容对外点
 * @author ly create at 2021/6/22 - 14:01
 **/
public class HtmlParserManager {

    public static List<CatalogueEntity> explainCatalogue(String catalogueUrlHtmlContent, CatalogueRuleEntity catalogueRuleEntity, String domain) {
        return new HtmlCatalogueExplain(catalogueRuleEntity,domain).parserHtml(catalogueUrlHtmlContent);
    }

    /**
     * 测试规则适配程度
     */
    public static List<CatalogueEntity> explainCatalogueCheck(String catalogueUrlHtmlContent, CatalogueRuleEntity catalogueRuleEntity, String domain, MsgList<String> checkResult) {
        return new HtmlCatalogueExplain(catalogueRuleEntity,domain).parserHtmlCheck(catalogueUrlHtmlContent,checkResult);
    }


    public static String explainContent(String contentUrlHtmlContent, ContentRuleEntity contentRuleEntity, String contentUrl, ThirdService thirdService) {
        return new HtmlContentExplain(contentRuleEntity,contentUrl,thirdService).parserHtml(contentUrlHtmlContent);
    }

    public static String explainContentCheck(String contentUrlHtmlContent, ContentRuleEntity contentRuleEntity, String contentUrl, ThirdService thirdService, MsgList<String> checkResult) {
        return new HtmlContentExplain(contentRuleEntity,contentUrl,thirdService).parserHtmlCheck(contentUrlHtmlContent,checkResult);
    }
}
