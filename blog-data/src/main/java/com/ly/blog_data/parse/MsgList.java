package com.ly.blog_data.parse;

import java.util.ArrayList;

/**
 * 累加信息的自定义List
 *
 * @author ly
 * create at 2021/9/18 - 9:50
 **/
public class MsgList<T> extends ArrayList<T> {

    /**
     * 自定义消息累加
     * @param o
     * @return
     */
    public MsgList addd(T o) {
        super.add(o);
        return this;
    }

}
