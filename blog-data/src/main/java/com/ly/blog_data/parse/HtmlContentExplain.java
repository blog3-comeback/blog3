package com.ly.blog_data.parse;

import com.ly.blog_common.utils.R;
import com.ly.blog_data.entity.Constant;
import com.ly.blog_data.entity.ContentRuleEntity;
import com.ly.blog_data.feign.ThirdService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 内容Html解析
 * @author ly create at 2021/6/22 - 19:41
 **/
@Slf4j
public class HtmlContentExplain {


    private ContentRuleEntity contentRuleEntity;
    private String contentUrl;
    private ThirdService thirdService;

    HtmlContentExplain(ContentRuleEntity contentRuleEntity, String contentUrl, ThirdService thirdService){
        this.contentRuleEntity = contentRuleEntity;
        this.contentUrl = contentUrl;
        this.thirdService = thirdService;
    }


    //解析内容最小区间
    private String areaRule2Reg(){
        List<String> areaPreList = this.contentRuleEntity.getAreaPreList();
        List<String> areaPostList = this.contentRuleEntity.getAreaPostList();
        StringBuilder reg = new StringBuilder();
        reg.append(areaPreList.stream().reduce("",(all,now)-> {
            if(!"".equals(all)){
                return all + Constant.REG_ALL_MIX_ + now;
            }
            return now;
        }));
        reg.append(Constant.REG_ALL_MIX_);
        reg.append(areaPostList.stream().reduce("",(all,now)-> {
            if(!"".equals(all)){
                return all + Constant.REG_ALL_MIX_ + now;
            }
            return now;
        }));

        return reg.toString();
    }


    public String parserHtml(String contentUrlHtmlContent) {
        log.info("【获取内容】 -- 开始 - ");
        String htmlContent = null;
        Pattern pattern = Pattern.compile(areaRule2Reg());
        Matcher matcher = pattern.matcher(contentUrlHtmlContent);
        if(matcher.find()){
            htmlContent = matcher.group(this.contentRuleEntity.getAreaPreList().size());
            log.info("【获取内容】 -- 已匹配内容区间 -- 内容{} ",htmlContent);
            //替换规则
            for(Map.Entry<String,String> entry:this.contentRuleEntity.getReplaceCondition().entrySet()){
                htmlContent = htmlContent.replaceAll(entry.getKey(),entry.getValue());
            }
            //图片转换存储本地
            htmlContent = getImgSaveLocal(htmlContent);
        }else{
            log.error("【获取内容】 -- 无匹配到内容 -- 内容{} ",contentUrlHtmlContent);
        }
        log.info("【获取内容】 -- 结束 - ");
        return htmlContent;
    }



    public String parserHtmlCheck(String contentUrlHtmlContent, MsgList<String> checkResult) {
        log.info("【获取内容】 -- 开始 - ");
        checkResult.add("检测内容规则 - 开始 -----------\n");
        String htmlContent = null;
        Pattern pattern = Pattern.compile(areaRule2Reg());
        Matcher matcher = pattern.matcher(contentUrlHtmlContent);
        if(matcher.find()){
            checkResult.add("匹配内容区间规则 匹配成功 ---------\n");
            htmlContent = matcher.group(this.contentRuleEntity.getAreaPreList().size());
            log.info("【获取内容】 -- 已匹配内容区间 -- 内容{} ",htmlContent);
            checkResult.addd("匹配内容区间规则 匹配内容为 ---------").addd(htmlContent).addd("\n");
            //替换规则
            for(Map.Entry<String,String> entry:this.contentRuleEntity.getReplaceCondition().entrySet()){
                htmlContent = htmlContent.replaceAll(entry.getKey(),entry.getValue());
            }
            //图片转换存储本地
            htmlContent = getImgSaveLocal(htmlContent);
        }else{
            log.error("【获取内容】 -- 无匹配到内容 -- html内容{} ",contentUrlHtmlContent);
            checkResult.addd("匹配内容区间规则 匹配失败 ---------").addd(contentUrlHtmlContent).addd("\n");
        }
        log.info("【获取内容】 -- 结束 - ");
        checkResult.add("检测内容规则 - 结束 -----------\n");
        return htmlContent;
    }

    //获取网络图片存储本地
    public String getImgSaveLocal(String htmlContent) {

        final String reg = "<img src=\"" + Constant.REG_ALL_MIX_ + "\"";
        final String local = "./";

        Pattern pattern = Pattern.compile(reg);
        Matcher matcher = pattern.matcher(htmlContent);
        while (matcher.find()){
            String imgSrc = matcher.group(1);
            if(imgSrc.contains(local)){
                String imgSrcTemp = this.contentUrl.substring(0,this.contentUrl.lastIndexOf("/")+1) + imgSrc.replace(local,"");
                imgSrcTemp = saveImg(imgSrcTemp);
                htmlContent = htmlContent.replace(imgSrc,imgSrcTemp);
            }
        }

        return htmlContent;

    }

    /**
     * 原计划采用前端的form表单传输，发现实现很困难 - 变相处理它 - 在文件服务器去处理文件，返回路径
     * @param imgSrcTemp
     * @return
     */
    private String saveImg(String imgSrcTemp) {
        try {
            R r = thirdService.netFileUpload(imgSrcTemp);
            return String.valueOf(r.get("filePath"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imgSrcTemp;
    }


}
